/*
 * ----------------------------------------------
 * Projet ou Module : oauth-resource-server
 * Nom de la classe : ResourceServerApplicationStarter.java
 * Date de création : 15 nov. 2020
 * Heure de création : 00:54:14
 * Package : fr.vincent.tuto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * Le Starter du Back-End de l'application de gestion d'une compagnie.
 * 
 * @author Vincent Otchoun
 */
@SpringBootApplication
public class ResourceServerApplicationStarter
{
    /**
     * @param args
     */
    public static void main(String... args)
    {
        //
        new SpringApplicationBuilder(ResourceServerApplicationStarter.class).run(args);
    }
}
