/*
 * ----------------------------------------------
 * Projet ou Module : oauth-resource-server
 * Nom de la classe : package-info.java
 * Date de création : 15 nov. 2020
 * Heure de création : 00:52:59
 * Package : fr.vincent.tuto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Package de base de l'application de gestion de compagnie.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto;
