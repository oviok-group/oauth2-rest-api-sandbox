# oauth2-rest-api-sandbox

## 1- Présentation générale

### 1-1 Type d'architecture  
La défintion/conception des briques/composants applicatifs est basée sur une **_architecture micro-service_**. Les éléments de spécifications sont également founis
dans la documentation de chaque micro-service.

### 1-2 Objectif principal 
L'objectif principal est de  :
- Sécuriser une application **Spring REST API** avec **OAuth2** et **Spring Security**
- Gérer la persistance des données qui seront stockées dans une base de données relationnelles.
- Founir un client Angular pour interagir avec les micro-services

### 1-3 Les axes 
Les axes directeurs de l'implémentation des use case pour la satisfaction des besoins exprimés :
- Configurer Spring Security pour intégrer la couche de sécurité dans l'application
- Configurer la base de données pour la persistance et gestion des données 
- Créer un serveur de ressources pour protéger les données ou ressources (le Back-End métier)
- Créer un serveur d’autorisations pour l'authentification des clients
- Obtenir un jeton d’accès (access token) et un jeton d'actualisation (refresh token)
- Obtenir une ressource sécurisée à l’aide d’un jeton d’accès.

### 1-4 Stack Technique Global 
En plus de l'IDE Eclipse STS utilisé, voici une liste non exhaustive des technos qui sont embarquées pour l'implémention des besoins exprimés.

![](https://img.shields.io/badge/Java_11-✓-blue.svg)
![](https://img.shields.io/badge/Maven_3-✓-blue.svg)
![](https://img.shields.io/badge/Spring_Boot_2-✓-blue.svg)
![](https://img.shields.io/badge/Spring_Security_5-✓-blue.svg)
![](https://img.shields.io/badge/OAuh2-✓-blue.svg)
![](https://img.shields.io/badge/Jpa_2-✓-blue.svg)
![](https://img.shields.io/badge/Hibernate_5-✓-blue.svg)
![](https://img.shields.io/badge/PostgreSQL_9+-✓-blue.svg)
![](https://img.shields.io/badge/Docker-✓-blue.svg)
![](https://img.shields.io/badge/Swagger_OAS_3-✓-blue.svg)
![](https://img.shields.io/badge/Angular-✓-blue.svg)
![](https://img.shields.io/badge/Java_Mail_Sender-✓-blue.svg)

### 1-5 Le Build
L'état du résultat final des opérations de compliation, construction des livrables de l'application est :

![](https://img.shields.io/badge/build-success-brightgreen.svg)

## 2 -Brève présentation de OAuth2 
La spécification OAuth 2.0 définit un protocole de délégation qui définit les flux d'autiorisation entre les clients et un ou plusieurs services HTTP ou réseau d'applications et d'API Web (les applications bureau, les téléphones mobiles, les appareils de salon) afin d'accéder aux ressources protégées.
**_Oauth 2.0 est le protocole standard pour l’autorisation_**. Il est utilisé dans une grande variété d'applications, notamment en fournissant des mécanismes d'authentification des utilisateurs. Il définit :
- des rôles : ils sont au nombre de 4 (dont 3 côté serveur)
	- côté serveur ce sont : **_Resource owner_**, **_Resource server_**, **_Authorization server_**
	- côté client : **_Client_** 
- des types d'octroi ou de subventions (flux d'autorisation)

### 2-1 Les rôles définis par OAuth 2 
Le tableau ci-dessous donne une brève description des rôles définis et fournit par la spécification OAuth 2.0. 

|Rôle|Description
|---|---
|Resource owner|_Entité capable d'accorder ou de contrôler les accès à des ressources protégées (par exemple l'utilisateur final)_ 
|Client|_Application demandant l’accès à un serveur de ressources (site web, application Javascript ou application mobile,… )_
|Resource server|_Serveur hébergeant des données/informations protégées, qui fournit réellement les ressources_
|Authorization server|_Le serveur émettant des jetons d'accès au client après avoir authentifié avec succès le propriétaire de la ressource et obtenu l'autorisation_

Le diagramme ci-dessus illustre le schéma global de flux de rôles OAuth2
![Diagramme de composants de flux de Rôles](./docs/images/flux_roles.png "Flux de Rôles OAuth2")

### 2-2 Les types d'Octroi founis par OAuth2  
Il définit également 4 types de Subventions ou flux d'autorisation. Le tableau ci-dessous permet de fournir quelques détails.

|Type de d'octroi|Description
|---|---
|Authorization Code|_Il est utilisé par les clients confidentiels et publics pour échanger un code d'autorisation contre un jeton d'accès. C'est la fonctionnalité permettant de se connecter à une application à l’aide d'un compte Facebook ou Google par exemple_
|Client credentials|_Il est utilisé par les clients pour obtenir un jeton d'accès en dehors du contexte d'un utilisateur. Le client peut demander un jeton d’accès en utilisant uniquement ses informations d’identification ( ou un autre moyen d’authentification pris en charge )_
|Implicit|_C'est un flux de code d’autorisation simplifié optimisé pour les clients implémentés dans un navigateur utilisant un langage de scripting tel que JavaScript. Dans le flux implicite, au lieu d’émettre au client un code d’autorisation, le client reçoit directement un jeton d’accès._
|Password|_Il est destiné aux clients basés sur un agent utilisateur (applications web à page unique, par exemple) qui ne peuvent pas garder un client confidentiel. Ensuite, au lieu du serveur d’autorisation retournant un code d’autorisation qui est échangé contre un jeton d’accès, dans le cas d’octroi d’autorisation, il renvoie un jeton d’accès._

**NB** : Dans le cadre de cette réalisation, le type d'octroi qui sera implémenté est : **Password Grant**.

## 3 -Expression de besoins et exigences
Le souhait ou le besoin principal exprimé est de **_sécuriser les accès aux ressources et aux données d'une compagnie_** (société ou entreprise). Les données/informations manipulées concernent les éléments ou objets cités ci-dessous :
- le _parc automobile_, les _bureaux_, les _départements_ de la société
- les _employés_ de la société et leur _adresse_
- les informations d'identification des utilisateurs et/ou client de l'application

### 3-1 Les exigences
Les exigences identifées sont principalement de 2 ordres :
- **les exigences fonctionnelles** : elles concernent les besoins à satisfaire par l'application. Le tableau ci-dessous expose les principaux processus relatant les exigences fonctionnelles. Ce sont principalement

|Processus|Description
|---|---
|Gestion de l'authentification|_Vérifier l'identité de l'utilisateur, produire et fournir les jetons d'accès_
|Gestion de l'autorisation|_Créer et définir les rôles, protéger les ressources ou services de l'application_
|Gestion de la persistance des données|_Créer, rechercher, mettre à jour, supprimer, administrer les données_
|Gestion asynchrone de notifications par mails|_Créer, envoyer de mails de notifications des utilisateurs_
|Gestion de l'administration des données et ressources|_administrer les données, administrer les services et ressources_

- **les exigences non fonctionnelles** : beaucoup plus d'ordre technique, elles concourent également au bon fonctionnement de l'application. Il s'agit principalement de :
	- la gestion des propriétés
	- la gestion des accès à la base de données	
	- la gestion des logs 
	- la gestion des erreurs et/ou des exceptions 

### 3-2 Les cas d'utilisation
D'un point de vu macroscopique, le schéma global des cas d'utilisation est fourni par le diagramme ci-dessous:
![DUC](./docs/images/use_case_global_v2.png "Diagramme des Use case")

## 4 -Architecture - Fonctionnement Global
Comme indiqué un peu plus haut, les élements qui seront founis ici dans le cadre de notre application sont de type : **Resource Owner Password Credentials Grant**. Dans le soucis d'apporter encore de
la clarté dans mes spécifications techniques de l'application, les éléments suivants ci-dessous sont présentés par la suite dans cette section:
- le diagramme d'architecture applicatif et technique
- le diagramme de séquences du fonctionnement global de l'application

### 4-1 Architecture Globale (Applicative et Technique) 
Le schéma fournit une vision globale des flux d'informations entre l'application et les autres acteurs du système.

![DAAT](./docs/images/architecture_technique_globale.png "Diagrammme Architecture Applicatif et Technique")

### 4-2 Le Fonctionnement Global
Une vue macroscopique du fonctionnement global de l'application est fournie par le dagramme de séquences ci-dessous.

![DS](./docs/images/fonctionnement_global.png "Diagramme de séquences du fonctionnement global")

## 5 -Le schéma global de modèle de données
Ce schéma comprend principalement 2 grands volets :
- le modèle de données métier
- le modèle de données de gestion de Credentials (information d'authentification et autorisation)

### 5-1 Le diagramme de classes des objets métier
C'est le modèle de données embarqué par l'application **_oauth-resource-server_** pour la gestion de la logique métier.

![DC](./docs/images/model_donnees_metier.png "Diagramme de Classes des objets métier")

### 5-2 Le Modèle Conceptuel des Données des objets métier
Le **_MCD_** correspondant au schéma de données des objets metier est le suivant

![MCD](./docs/images/model_conceptuel_donnees_metier.png "MCD des objets métier")

### 5-3 Les schémas de modèles de données pour la gestion des Credentials
Ces modèles sont embarqués par l'application **_oauth-authorization-server_**. Les schémas des tables en base de données sont complètés par ceux des scripts OAuth2 fournit par 
la spécification pour la gestion des données d'identification des clients.

#### A - Le schéma du modèle de données des utilisateurs
Le diagramme de classes ci-dessous présente les informations du modèle de données de base qui sera embarquée par l'application pour gérer l'identification des utilisateurs. Ce modèle peut être amené à être enrichi
lors de la réalisation (par exemple la granularité des rôles des utilisateurs).
  
![MCD](./docs/images/credentials_model_donnees_metier_v2.png "Credentials modèle de données")

#### B - Le schéma du modèle de données des tables OAUTH
Le diagramme ci-dessous présente les informations des tables OAuth2 pour la gestion des identifications des clients.
![MCD](./docs/images/model_donnees_tables_oauth2.png "MCD Tables OAuth2")

Le tableau ci-dessous donne une brève description de chaque table.

|Table|Description
|---|---
|oauth_client_details|_Elle stocke des détails sur les applications client OAuth2. Chaque application cliente Web Flow doit avoir un enregistrement dans cette table_
|oauth_client_token|_Elle stocke les jetons OAuth2 à récupérer par les applications clientes_
|oauth_access_token|_Elle stocke les jetons d'accès OAuth2_
|oauth_refresh_token|_Elle stocke les jetons d'actualisation/renouvellement OAuth2_
|oauth_code|_Elle stocke les données pour l'octroi du code d'autorisation OAuth2_
