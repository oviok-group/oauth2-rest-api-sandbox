/*
 * ----------------------------------------------
 * Projet ou Module : oauth-authorization-server
 * Nom de la classe : AuthorizationServerAppWebXml.java
 * Date de création : 14 nov. 2020
 * Heure de création : 23:29:15
 * Package : fr.vincent.tuto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * Ajout du profile web pour faciliter le déploiement et l'exécution dans un container externe.
 * 
 * @author Vincent Otchoun
 */
public class AuthorizationServerAppWebXml extends SpringBootServletInitializer
{

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder applicationBuilder)
    {
        return applicationBuilder.sources(AuthorizationServerApplicationStarter.class);
    }

}
