/*
 * ----------------------------------------------
 * Projet ou Module : oauth-authorization-server
 * Nom de la classe : AuthorizationServerBaseConfig.java
 * Date de création : 15 nov. 2020
 * Heure de création : 00:14:27
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Configuration de base du serveur d'autorisation.
 * 
 * @author Vincent Otchoun
 */
@Configuration
@ComponentScan(basePackages = "fr.vincent.tuto")
@PropertySource(value = { "classpath:authorization-server-application.properties" }, ignoreResourceNotFound = false)
public class AuthorizationServerBaseConfig
{
    //
}
