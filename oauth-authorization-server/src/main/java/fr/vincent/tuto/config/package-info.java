/*
 * ----------------------------------------------
 * Projet ou Module : oauth-authorization-server
 * Nom de la classe : package-info.java
 * Date de création : 15 nov. 2020
 * Heure de création : 00:13:23
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les beans de configuration du module applicatif.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.config;
