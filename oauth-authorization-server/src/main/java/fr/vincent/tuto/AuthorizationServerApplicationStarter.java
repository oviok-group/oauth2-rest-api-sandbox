/*
 * ----------------------------------------------
 * Projet ou Module : oauth-authorization-server
 * Nom de la classe : AuthorizationServerApplicationStarter.java
 * Date de création : 14 nov. 2020
 * Heure de création : 22:47:17
 * Package : fr.vincent.tuto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * Le Starter du serveur d'autorisation pour confirmer ou valider l'identité de l'utilisateur ou du client.
 * 
 * @author Vincent Otchoun
 */
@SpringBootApplication
public class AuthorizationServerApplicationStarter
{

    /**
     * @param args
     */
    public static void main(String... args)
    {
        //
        new SpringApplicationBuilder(AuthorizationServerApplicationStarter.class).run(args);
    }
}
