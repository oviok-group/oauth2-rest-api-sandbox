# oauth-common-app

Module applicatif commun qui permet de mutualiser les codes des services, configurations, ... qui sont utilisés dans les autres composants applicatifs.

![](https://img.shields.io/badge/build-success-brightgreen.svg)

## Eléments de mutualisation
Le module comporte ou fournit les éléments suivants :
- Le modèle de données :
	- objet de fourniture des paramètres de cryptographie pour les utilitaires embarqués.
	- énumération ppour la définition des rôles dans les flux d'authentification.
Les configurations :
	- Configuration de base du module applicatif.
	- Configuration pour la création de beans communs : 
		- BCryptPasswordEncoder : pour le crhiffrement des mot de passe 
		- ObjectMapper : pour les traitements des flux JSON
		- ModelMapper : pour la transformation des objets. 
		- ...
- Les services :
	- le service de gestion des propriétés d'accès à la base de données : 
		- Initialiser les composants assurant la connexion à la base de données (**_DataSource avec Hikari_**)
		- Initialiser le gestionnaire de pool de connexion à la base de données avec **_ Hikari_**
		- Initialiser les composants **_JPA/Hibernate_** d'abstraction des accès aux informations en base de données 
	- le service de gestion des autres propriétés applicatives
		- Générer la documentation Swagger des API
		- Elements de gestion de l'authentification avec JWT
		- Assurer la gestion et  envoi de mail
		- Gestion de l'asynchronisme
		- Alimenter les composants de cryptographie.
	- le service de production et d'envoi de mails.		
- les utilitaires : 
	- Génration aléatoire de chaîne de caractères
	- Génération de schéma XSD à partir de l'objet 
	- Cryptographie avec algorithmes de Chiffrement/Cryptage, Décrytage symétrique AES (Advanced Encryption Standard) et asymétrique RSA (Rivest-Shamir-Adelman) (chaîne de caractères, fichiers, ...)
	- Cryptographie avec Algorithmes de chiffrement asymétriques (ou à clef publique) RSA.
	- Authentification de Spring Security et JWT (Json Web Tokens).
	- Conversion de dates.
