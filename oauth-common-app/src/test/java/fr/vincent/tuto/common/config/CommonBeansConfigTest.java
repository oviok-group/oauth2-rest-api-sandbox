/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : CommonBeansConfigTest.java
 * Date de création : 25 nov. 2020
 * Heure de création : 08:10:05
 * Package : fr.vincent.tuto.common.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.config;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.vincent.tuto.common.model.Article;

/**
 * Classe des tests unitaires des objets de type {@link CommonBeansConfig}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:common-app-db-test.properties",
        "classpath:common-app-application-test.properties", "classpath:common-app-messages-test.properties" })
@ContextConfiguration(name = "commonBeansConfigTest", classes = { CommonAppBaseConfig.class,
        CommonBeansConfig.class, JavaMailSenderImpl.class })
@ActiveProfiles("test")
@SpringBootTest
public class CommonBeansConfigTest
{
    //
    private static final String MOT_DE_PASSE = "Mot de passe de Test";
    private static final String ENCODED_PASSE = "$2a$12$wZV/cRtRE72j1lg89isL9OKxoqB6k0E8v3.NTXSXpDsxNraU5h5sq";

    private static final String ARTICLE_JSON = "{\"refArticle\":-9223372036854775808,\"titleArticle\":\"Titre de l'article\",\"categorieArticle\":\"Categorie de l'article\",\"dateCreationArticle\":[2020,11,25,8,48,1,421211000]}";
    private static final String ARTICLE_JSON_PRETTY_PRINT =    "{\r\n"
    + "  \"refArticle\" : -9223372036854775808,\r\n"
    + "  \"titleArticle\" : \"Titre de l'article\",\r\n"
    + "  \"categorieArticle\" : \"Categorie de l'article\",\r\n"
    + "  \"dateCreationArticle\" : [ 2020, 11, 25, 8, 59, 45, 139508500 ]\r\n"
    + "}";

    private Article article;

    @Autowired
    private CommonBeansConfig commonBeansConfig;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.article = new Article();
        this.article.setCategorieArticle("Categorie de l'article");
        this.article.setTitleArticle("Titre de l'article");
        this.article.setRefArticle(Long.MIN_VALUE);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.commonBeansConfig = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.config.CommonBeansConfig#bCryptPasswordEncoder()}.
     */
    @Test
    public void testBCryptPasswordEncoder()
    {
        final PasswordEncoder passwordEncoder = this.commonBeansConfig.passwordEncoder();

        assertThat(passwordEncoder).isNotNull();
        assertThat(passwordEncoder.encode(MOT_DE_PASSE)).isNotEmpty();
        assertThat(passwordEncoder.encode(MOT_DE_PASSE)).contains("$12$");
    }

    @Test
    public void testBCryptPasswordEncoderWithEmpty()
    {
        final PasswordEncoder passwordEncoder = this.commonBeansConfig.passwordEncoder();

        assertThat(passwordEncoder).isNotNull();
        assertThat(passwordEncoder.encode(StringUtils.EMPTY)).isNotEmpty();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBCryptPasswordEncoderShouldCatchException()
    {
        final PasswordEncoder passwordEncoder = this.commonBeansConfig.passwordEncoder();

        assertThat(passwordEncoder).isNotNull();
        assertThat(passwordEncoder.encode(null)).isNotEmpty();
    }

    @Test
    public void testBCryptPasswordEncoderVerifTrue()
    {
        final PasswordEncoder passwordEncoder = this.commonBeansConfig.passwordEncoder();

        assertThat(passwordEncoder).isNotNull();
        assertThat(passwordEncoder.matches(MOT_DE_PASSE, ENCODED_PASSE)).isTrue();
    }

    @Test
    public void testBCryptPasswordEncoderVerifFalse()
    {
        final PasswordEncoder passwordEncoder = this.commonBeansConfig.passwordEncoder();

        assertThat(passwordEncoder).isNotNull();
        assertThat(passwordEncoder.encode(MOT_DE_PASSE)).isNotEmpty();
        assertThat(passwordEncoder.matches(MOT_DE_PASSE, passwordEncoder.encode(StringUtils.EMPTY))).isFalse();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.config.CommonBeansConfig#jackson2ObjectMapperBuilder()}.
     */
    @Test
    public void testJackson2ObjectMapperBuilder()
    {
        assertThat(this.commonBeansConfig.jackson2ObjectMapperBuilder()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.config.CommonBeansConfig#objectMapper()}.
     */
    @Test
    public void testObjectMapper()
    {
        assertThat(this.commonBeansConfig.objectMapper()).isNotNull();
    }

    @Test
    public void testObjectMapperToJSON() throws JsonProcessingException
    {
        final ObjectMapper mapper = this.commonBeansConfig.objectMapper();
        final String jsonArticle = mapper.writeValueAsString(this.article);

        assertThat(jsonArticle).isNotEmpty();
        assertThat(jsonArticle).containsSequence("Titre de l'article");
        assertThat(jsonArticle).containsSequence("Categorie de l'article");
    }

    @Test
    public void testObjectMapperToJSONPrettyPrint() throws JsonProcessingException
    {
        final ObjectMapper mapper = this.commonBeansConfig.objectMapper();
        final String jsonArticle = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this.article);

        assertThat(jsonArticle).isNotEmpty();
        assertThat(jsonArticle).containsSequence("Titre de l'article");
        assertThat(jsonArticle).containsSequence("Categorie de l'article");
    }

    @Test
    public void testObjectMapperToJSONWithNull() throws JsonProcessingException
    {
        final ObjectMapper mapper = this.commonBeansConfig.objectMapper();
        final String jsonArticle = mapper.writeValueAsString(null);

        assertThat(jsonArticle).isNotNull();
    }

    @Test
    public void testObjectMapperFromJSON() throws JsonMappingException, JsonProcessingException
    {
        final ObjectMapper mapper = this.commonBeansConfig.objectMapper();
        final Article article = mapper.readValue(ARTICLE_JSON, Article.class);

        assertThat(article).isNotNull();
        assertThat(article).isExactlyInstanceOf(Article.class);
        assertThat(article.toString()).isNotNull();
    }

    @Test
    public void testObjectMapperFromJSONPrettyPrint() throws JsonMappingException, JsonProcessingException
    {
    final ObjectMapper mapper = this.commonBeansConfig.objectMapper();
    final Article article = mapper.readValue(ARTICLE_JSON_PRETTY_PRINT, Article.class);

    assertThat(article).isNotNull();
    assertThat(article).isExactlyInstanceOf(Article.class);
    assertThat(article.toString()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.config.CommonBeansConfig#modelMapper()}.
     */
    @Test
    public void testModelMapper()
    {
        assertThat(this.commonBeansConfig.modelMapper()).isNotNull();
    }

    @Test
    public void testIsNotNullConfig()
    {
        assertThat(this.commonBeansConfig).isNotNull();
    }

}
