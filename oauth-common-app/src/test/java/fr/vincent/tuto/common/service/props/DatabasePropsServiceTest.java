/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : DatabasePropsServiceTest.java
 * Date de création : 18 nov. 2020
 * Heure de création : 23:16:41
 * Package : fr.vincent.tuto.common.service.props
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.service.props;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.common.config.CommonAppBaseConfig;
import fr.vincent.tuto.common.service.props.DatabasePropsService.DataSourceProps;
import fr.vincent.tuto.common.service.props.DatabasePropsService.FlywayProps;
import fr.vincent.tuto.common.service.props.DatabasePropsService.HikariProps;
import fr.vincent.tuto.common.service.props.DatabasePropsService.JpaHibernateProps;

/**
 * Classe des Tests Unitaires des objets de type {@link DatabasePropsService}
 * 
 * @author Vincent Otchoun
 */
@SpringBootApplication
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:common-app-db-test.properties",
        "classpath:common-app-application-test.properties", "classpath:common-app-messages-test.properties" })
@ContextConfiguration(name = "databasePropsServiceTest", classes = { CommonAppBaseConfig.class,
        DatabasePropsService.class, JavaMailSenderImpl.class })
@SpringBootTest
@ActiveProfiles("test")
public class DatabasePropsServiceTest
{
    //
    @Autowired
    private DatabasePropsService props;

    /**
     * Test method for {@link fr.vincent.tuto.common.service.props.DatabasePropsService#getFlywayProps()}.
     */
    @Test
    public void testGetFlywayProps()
    {
        final FlywayProps flyway = this.props.getFlywayProps();

        assertThat(flyway).isNotNull();


        assertThat(flyway.getEnabled()).isTrue();
        assertThat(flyway.getGroup()).isTrue();
        assertThat(flyway.getBaselineOnMigrate()).isTrue();
        assertThat(flyway.getSqlMigrationPrefix()).isEqualToIgnoringCase("V");
        assertThat(flyway.getSqlMigrationSeparator()).isEqualToIgnoringCase("__");
        assertThat(flyway.getSqlMigrationSuffixes()).isEqualToIgnoringCase(".sql");

        assertThat(flyway.getLocations()).contains("filesystem:./docs/");
        assertThat(flyway.getUrl()).contains("jdbc:h2:mem:security_permission");
        assertThat(flyway.getUser()).isEqualToIgnoringCase("sa");
        assertThat(flyway.getPassword()).isEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.service.props.DatabasePropsService#getDataSourceProps()}.
     */
    @Test
    public void testGetDataSourceProps()
    {
        final DataSourceProps dataSource = this.props.getDataSourceProps();

        assertThat(dataSource).isNotNull();

        assertThat(dataSource.getType()).isEqualToIgnoringCase("com.zaxxer.hikari.HikariDataSource");
        assertThat(dataSource.getDatasourceClassName()).isEqualToIgnoringCase("org.h2.jdbcx.JdbcDataSource");
        assertThat(dataSource.getDriverClassName()).isEqualToIgnoringCase("org.h2.Driver");
        assertThat(dataSource.getJdbcUrl()).contains("jdbc:h2:mem:");
        assertThat(dataSource.getUserName()).isEqualToIgnoringCase("sa");
        assertThat(dataSource.getPassword()).isEmpty();
        assertThat(dataSource.getTestWhileIdle()).isTrue();
        assertThat(dataSource.getPoolPreparedStatements()).isTrue();
        assertThat(dataSource.getValidationQuery()).isEqualToIgnoringCase("select 1");
        assertThat(dataSource.getMaxOpenPreparedStatements()).isPositive();
        assertThat(dataSource.getMaxOpenPreparedStatements()).isEqualTo(250);
        assertThat(dataSource.getPersistenceUnitName()).isEqualTo("jwt-auth-sandbox-pu-test");
        assertThat(dataSource.getPackageToScan()).contains("fr.vincent.");
        assertThat(dataSource.getGenerateDdl()).isFalse();
        assertThat(dataSource.getPlatform()).isEqualTo("H2");

        //
        assertThat(dataSource.getCachePrepareStatements()).isEqualTo("cachePrepStmts");
        assertThat(dataSource.getPrepareStatementCacheSize()).isEqualTo("prepStmtCacheSize");
        assertThat(dataSource.getPrepareStatementCacheSqlLimit()).isEqualTo("prepStmtCacheSqlLimit");
        assertThat(dataSource.getUseServerPrepareStatements()).isEqualTo("useServerPrepStmts");

        //
        assertThat(dataSource.getInitializationMode()).isEqualTo("NEVER");
        assertThat(dataSource.getInitSchema()).contains("/schema/schema-h2.sql");
        assertThat(dataSource.getInitData()).contains("/data/data-h2.sql");
        assertThat(dataSource.getInitContinueOnError()).isFalse();
        assertThat(dataSource.toString()).isNotEmpty();
    }

    @Test
    public void testGetDataSourcePropsShouldReturnNull()
    {
        //
        final DataSourceProps dataSource = this.props.getDataSourceProps();

        dataSource.setType(null);
        dataSource.setDriverClassName(null);
        dataSource.setJdbcUrl(null);
        dataSource.setDatasourceClassName(null);
        dataSource.setUserName(null);
        dataSource.setPassword(null);
        dataSource.setTestWhileIdle(null);
        dataSource.setValidationQuery(null);
        dataSource.setPoolPreparedStatements(null);
        dataSource.setMaxOpenPreparedStatements(null);
        dataSource.setPersistenceUnitName(null);
        dataSource.setPackageToScan(null);
        dataSource.setPlatform(null);

        //
        dataSource.setCachePrepareStatements(null);
        dataSource.setPrepareStatementCacheSize(null);
        dataSource.setPrepareStatementCacheSqlLimit(null);
        dataSource.setUseServerPrepareStatements(null);

        //
        dataSource.setInitializationMode(null);
        dataSource.setInitSchema(null);
        dataSource.setInitData(null);
        dataSource.setInitContinueOnError(null);

        assertThat(dataSource).isNotNull();
        assertThat(dataSource.getType()).isNull();
        assertThat(dataSource.getDriverClassName()).isNull();
        assertThat(dataSource.getJdbcUrl()).isNull();
        assertThat(dataSource.getDatasourceClassName()).isNull();
        assertThat(dataSource.getUserName()).isNull();
        assertThat(dataSource.getPassword()).isNull();
        assertThat(dataSource.getTestWhileIdle()).isNull();
        assertThat(dataSource.getValidationQuery()).isNull();
        assertThat(dataSource.getPoolPreparedStatements()).isNull();
        assertThat(dataSource.getMaxOpenPreparedStatements()).isNull();
        assertThat(dataSource.getPersistenceUnitName()).isNull();
        assertThat(dataSource.getPersistenceUnitName()).isNull();
        assertThat(dataSource.getPackageToScan()).isNull();
        assertThat(dataSource.getPlatform()).isNull();

        assertThat(dataSource.getCachePrepareStatements()).isNull();
        assertThat(dataSource.getPrepareStatementCacheSize()).isNull();
        assertThat(dataSource.getPrepareStatementCacheSqlLimit()).isNull();
        assertThat(dataSource.getUseServerPrepareStatements()).isNull();

        //
        assertThat(dataSource.getInitializationMode()).isNull();
        assertThat(dataSource.getInitSchema()).isNull();
        assertThat(dataSource.getInitData()).isNull();
        assertThat(dataSource.getInitContinueOnError()).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.service.props.DatabasePropsService#getJpaHibernateProps()}.
     */
    @Test
    public void testGetJpaHibernateProps()
    {
        final JpaHibernateProps jpaHibernate = this.props.getJpaHibernateProps();

        //
        assertThat(jpaHibernate).isNotNull();
        assertThat(jpaHibernate.getDatabaseName()).isEqualToIgnoringCase("H2");
        assertThat(jpaHibernate.getDialect()).contains("H2Dialect");
        assertThat(jpaHibernate.getShowSql()).isTrue();
        assertThat(jpaHibernate.getFormatSql()).isTrue();
        assertThat(jpaHibernate.getUseSql()).isTrue();
        assertThat(jpaHibernate.getUseSqlComments()).isTrue();
        assertThat(jpaHibernate.getUseReflectionOptimizer()).isFalse();
        assertThat(jpaHibernate.getBytecodeUseReflectionOptimizer()).isFalse();
        assertThat(jpaHibernate.getEnableLazy()).isTrue();
        assertThat(jpaHibernate.getNamingStrategy()).contains("ImprovedNamingStrategy");
        assertThat(jpaHibernate.getUseSecondLevelCache()).isFalse();
        assertThat(jpaHibernate.getGenerateStatistics()).isFalse();
        assertThat(jpaHibernate.getBytecodeUseReflectionOptimizer()).isFalse();
        assertThat(jpaHibernate.getDdlAuto()).contains("create");

        assertThat(jpaHibernate.getHbm2ddlImportFiles()).isNotNull();
        // assertThat(StringUtils.split(jpaHibernate.getHbm2ddlImportFiles(), ",").length).isPositive();

        assertThat(jpaHibernate.getGenerateDdl()).isNotNull();
        assertThat(jpaHibernate.getGenerateDdl()).isTrue();

        assertThat(jpaHibernate.toString()).isNotEmpty();
    }

    @Test
    public void testGetJpaHibernatePropsShouldReturnNull()
    {
        final JpaHibernateProps jpaHibernate = this.props.getJpaHibernateProps();

        //
        jpaHibernate.setDatabaseName(null);
        jpaHibernate.setDialect(null);
        jpaHibernate.setShowSql(null);
        jpaHibernate.setFormatSql(null);
        jpaHibernate.setUseSql(null);
        jpaHibernate.setUseSqlComments(null);
        jpaHibernate.setBytecodeUseReflectionOptimizer(null);
        jpaHibernate.setNamingStrategy(null);
        jpaHibernate.setUseSecondLevelCache(null);
        jpaHibernate.setGenerateStatistics(null);
        jpaHibernate.setBytecodeUseReflectionOptimizer(null);
        jpaHibernate.setDdlAuto(null);
        jpaHibernate.setEnableLazy(null);

        //
        assertThat(jpaHibernate).isNotNull();
        assertThat(jpaHibernate.getDatabaseName()).isNull();
        assertThat(jpaHibernate.getDialect()).isNull();
        assertThat(jpaHibernate.getShowSql()).isNull();
        assertThat(jpaHibernate.getFormatSql()).isNull();
        assertThat(jpaHibernate.getUseSql()).isNull();
        assertThat(jpaHibernate.getUseSqlComments()).isNull();
        assertThat(jpaHibernate.getBytecodeUseReflectionOptimizer()).isNull();
        assertThat(jpaHibernate.getNamingStrategy()).isNull();
        assertThat(jpaHibernate.getUseSecondLevelCache()).isNull();
        assertThat(jpaHibernate.getGenerateStatistics()).isNull();
        assertThat(jpaHibernate.getBytecodeUseReflectionOptimizer()).isNull();
        assertThat(jpaHibernate.getDdlAuto()).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.service.props.DatabasePropsService#getHikariProps()}.
     */
    @Test
    public void testGetHikariProps()
    {
        final HikariProps hikari = this.props.getHikariProps();

        //
        assertThat(hikari).isNotNull();
        assertThat(hikari.getMinimumIdle()).isPositive();
        assertThat(hikari.getMaximumPoolSize()).isPositive();
        assertThat(hikari.getIdleTimeout()).isPositive();
        assertThat(hikari.getPoolName()).isNotNull();
        assertThat(hikari.getPoolName()).isEqualToIgnoringCase("votJPAHikariCPTest");
        assertThat(hikari.getMaxLifetime()).isPositive();
        assertThat(hikari.getConnectionTimeout()).isPositive();
        assertThat(hikari.toString()).isNotEmpty();

        assertThat(hikari.getCachePrepareStatements()).isTrue();
        assertThat(hikari.getPrepareStatementCacheSize()).isEqualTo(250);
        assertThat(hikari.getPrepareStatementCacheSqlLimit()).isEqualTo(2048);
        assertThat(hikari.getUseServerPrepareStatements()).isTrue();
    }

    /*
     * Test à null complet sur le dernier bloc, fait planter le test non null.
     */
    @Test
    public void testGetHikariPartialNull()
    {
        final HikariProps hikari = this.props.getHikariProps();

        hikari.setMinimumIdle(2);
        hikari.setMaximumPoolSize(2);
        hikari.setIdleTimeout(2l);
        hikari.setPoolName("");
        hikari.setMaxLifetime(2l);
        hikari.setConnectionTimeout(2l);

        hikari.setCachePrepareStatements(null);
        hikari.setPrepareStatementCacheSize(null);
        hikari.setPrepareStatementCacheSqlLimit(null);
        hikari.setUseServerPrepareStatements(null);

        //
        assertThat(hikari).isNotNull();
        assertThat(hikari.getMinimumIdle()).isNotNull();
        assertThat(hikari.getMaximumPoolSize()).isNotNull();
        assertThat(hikari.getIdleTimeout()).isNotNull();
        assertThat(hikari.getPoolName()).isNotNull();
        assertThat(hikari.getMaxLifetime()).isNotNull();
        assertThat(hikari.getConnectionTimeout()).isNotNull();

        assertThat(hikari.getCachePrepareStatements()).isNull();
        assertThat(hikari.getPrepareStatementCacheSize()).isNull();
        assertThat(hikari.getPrepareStatementCacheSqlLimit()).isNull();
        assertThat(hikari.getUseServerPrepareStatements()).isNull();
    }
}
