/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : LogStartConfigTest.java
 * Date de création : 30 nov. 2020
 * Heure de création : 09:18:40
 * Package : fr.vincent.tuto.common.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.config;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.common.CommonAppTestStarter;

/**
 * Classe des Tests Unitaires des objets de type {@link LogStartConfig}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:common-app-db-test.properties",
        "classpath:common-app-application-test.properties", "classpath:common-app-messages-test.properties" })
@ContextConfiguration(name = "appCommonLogStartConfigProdTest", classes = { CommonAppBaseConfig.class,
        CommonBeansConfig.class, JavaMailSenderImpl.class, LogStartConfig.class })
@ActiveProfiles(profiles = { "test", "dev", "prod" })
@SpringBootTest
public class LogStartConfigProdTest
{
    //
    @Autowired
    private ConfigurableEnvironment environment;

    @Autowired
    private LogStartConfig commonLogStartConfig;

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.commonLogStartConfig = null;
        this.environment = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.config.LogStartConfig#logStartUp(org.springframework.core.env.Environment)}.
     */
    @Test
    public void testLogStartUp()
    {
        LogStartConfig.logStartUp(this.environment);
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.config.LogStartConfig#afterPropertiesSet()}.
     * 
     * @throws Exception
     */
    @Test
    public void testAfterPropertiesSet() throws Exception
    {
        this.commonLogStartConfig.afterPropertiesSet();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.config.LogStartConfig#addDefaultProfile(org.springframework.boot.SpringApplication)}.
     */
    @Test
    public void testAddDefaultProfile()
    {
        final SpringApplication application = new SpringApplication(CommonAppTestStarter.class);
        LogStartConfig.addDefaultProfile(application);
    }

    @Test(expected = NullPointerException.class)
    public void testAddDefaultProfileShouldThrowException()
    {
        LogStartConfig.addDefaultProfile(null);
    }

    @Test
    public void testINotNullBeans()
    {
        assertThat(this.environment).isNotNull();
        assertThat(this.commonLogStartConfig).isNotNull();
    }

}
