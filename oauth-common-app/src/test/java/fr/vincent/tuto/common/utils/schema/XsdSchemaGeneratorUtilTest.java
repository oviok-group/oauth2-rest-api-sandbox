/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : XsdSchemaGeneratorUtilTest.java
 * Date de création : 19 nov. 2020
 * Heure de création : 19:36:38
 * Package : fr.vincent.tuto.common.utils
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.utils.schema;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.common.exception.CustomAppException;
import fr.vincent.tuto.common.model.Article;

/**
 * Classe des Tests Unitaires des objets de type {@link XsdSchemaGeneratorUtil}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:common-app-db-test.properties", "classpath:common-app-application-test.properties",
        "classpath:common-app-messages-test.properties" })
@ContextConfiguration(name = "xsdSchemaGeneratorUtilTest", classes = { XsdSchemaGeneratorUtil.class, JavaMailSenderImpl.class })
@ActiveProfiles("test")
public class XsdSchemaGeneratorUtilTest
{

    private static final String SCHEMA_FILE_NAME = "article.xsd";

    @Value("${vot.xsd.file.test.gen.path}")
    private String filePath;

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.schema.XsdSchemaGeneratorUtil#xsdSchemaGeneration(java.lang.Class, java.lang.String)}.
     */
    @Test
    public void testXsdSchemaGeneration()
    {
        var schemaLocation = this.filePath + SCHEMA_FILE_NAME;
        assertThat(schemaLocation).isNotNull();

        XsdSchemaGeneratorUtil.xsdSchemaGeneration(Article.class, schemaLocation);
    }

    @Test(expected = CustomAppException.class)
    public void testXsdSchemaGenerationShouldCatchException()
    {
        XsdSchemaGeneratorUtil.xsdSchemaGeneration(null, null);
    }

    @Test
    public void testIsNotNullResource()
    {
        assertThat(this.filePath).isNotNull();
    }
}
