/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : CryptoProviderUtilTest.java
 * Date de création : 22 nov. 2020
 * Heure de création : 10:12:28
 * Package : fr.vincent.tuto.common.utils.crypto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.utils.crypto;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.common.config.CommonAppBaseConfig;
import fr.vincent.tuto.common.exception.CustomAppException;
import fr.vincent.tuto.common.model.mo.CryptoModel;

/**
 * Classe des Tests Unitaires des objets de type {@link CryptoProviderUtil}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:common-app-db-test.properties",
        "classpath:common-app-application-test.properties", "classpath:common-app-messages-test.properties" })
@ContextConfiguration(name = "cryptoProviderUtilTest", classes = { CommonAppBaseConfig.class,
        CryptoProviderUtil.class, JavaMailSenderImpl.class })
@ActiveProfiles("test")
public class CryptoProviderUtilTest
{
    //
    private CryptoModel cryptoModel;
    private static final String VALUE_TO_ENCRYPT = "JsonContent {\"refArticle\":-9223372036854775808,\"titleArticle\":\"Article de TestSignNotOk\",\"categorieArticle\":\"Catégorie de TestSignNotOk\",\"dateCreationArticle\":{\"date\":{\"year\":2018,\"month\":8,\"day\":15},\"time\":{\"hour\":7,\"minute\":26,\"second\":54,\"nano\":466000000}}}";
    private static final String ENCYTED_DATA = "i9n7rvdHPX27wQysbMrIEtzAyafL6KFjlJrTLTgRoBt6QaBzZWJ4Ma2IZL/bP4YO6Fd0J624JW8scgcbIMnPUyin/v71wuqZ5p53ag8ZAB6bdJE96MrPpPNE/9yLRZ3RZl2PMWFNSMzHVOUHkPVS9NwQtSip3YGDO20X8Mkm+8+AmYxc4kZbWGHV3mg93LMXbvTY95wRPNHKGBk9EtfiDTUIIFtFzb9TcaPYLmLaQ3P2bkGM6w9y7DkayaV7WV1P6j4tDg50txlDjyzsKirNIV3BjxFdJeNfWV5OxtJt4a1JjAJrtkIhttT05Yk6hGcwSs5tuXJohcySN17FS5lOddWXxjQFK7pnZxhYwF13HEw=";

    @Value("${vot.xsd.file.test.gen.path}")
    private String filePath;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.cryptoModel = new CryptoModel()//
        .algorithm("AES")//
        .instancePad("AES/ECB/PKCS5Padding")//
        .secureRandomInstance("SHA1PRNG")//
        .bytesLength(16)//
        .keyValue("2RJ87sLaFDdhxzhP")//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.TRUE)//
        .privateKey(null)//
        .publicKey(null)//
        .keystorePassword(null)//
        .keyPassword(null)//
        .aliasKeystore(null)//
        .signatureInstance(null);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.cryptoModel = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.crypto.CryptoProviderUtil#encrypt(java.lang.String, fr.vincent.tuto.common.model.mo.CryptoModel)}.
     */
    @Test
    public void testEncrypt()
    {
        final var encrypt = CryptoProviderUtil.encrypt(VALUE_TO_ENCRYPT, this.cryptoModel);

        // System.err.println(">Encryt data : \n" + encrypt);

        assertThat(encrypt).isNotNull();
        assertThat(encrypt.length()).isPositive();
        assertThat(encrypt).isExactlyInstanceOf(String.class);
    }

    @Test(expected = NullPointerException.class)
    public void testEncryptShouldCatchNPE()
    {
        final var encryptedData = CryptoProviderUtil.encrypt(null, null);

        assertThat(encryptedData).isNull();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEncryptWithNullAlgorithm()
    {
        var model = new CryptoModel()//
        .algorithm(null)//
        .instancePad("AES/ECB/PKCS5Padding")//
        .secureRandomInstance("SHA1PRNG")//
        .bytesLength(16)//
        .keyValue("2RJ87sLaFDdhxzhP")//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.TRUE);

        final var encryptedData = CryptoProviderUtil.encrypt(VALUE_TO_ENCRYPT, model);

        assertThat(encryptedData).isNull();
    }

    @Test(expected = CustomAppException.class)
    public void testEncryptWithNullInstancePad()
    {
        var model = new CryptoModel()//
        .algorithm("AES")//
        .instancePad(null)//
        .secureRandomInstance("SHA1PRNG")//
        .bytesLength(16)//
        .keyValue("2RJ87sLaFDdhxzhP")//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.TRUE);

        final var encryptedData = CryptoProviderUtil.encrypt(VALUE_TO_ENCRYPT, model);

        assertThat(encryptedData).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.crypto.CryptoProviderUtil#encryptSecureRandom(java.lang.String, fr.vincent.tuto.common.model.mo.CryptoModel)}.
     */
    @Test
    public void testEncryptSecureRandom()
    {
        final var secureEncryptedData = CryptoProviderUtil.encryptSecureRandom(VALUE_TO_ENCRYPT, this.cryptoModel);

        assertThat(secureEncryptedData).isNotNull();
        assertThat(secureEncryptedData.length()).isPositive();
        assertThat(secureEncryptedData).isExactlyInstanceOf(String.class);
    }

    @Test(expected = NullPointerException.class)
    public void testEncryptSecureRandomShouldCatchNPE()
    {
        final String secureEncryptedData = CryptoProviderUtil.encryptSecureRandom(null, null);

        assertThat(secureEncryptedData).isNull();
    }

    @Test(expected = CustomAppException.class)
    public void testEncryptSecureRandomShouldCatchException()
    {
        var model = new CryptoModel()//
        .algorithm("AES")//
        .instancePad(null)//
        .secureRandomInstance("SHA1PRNG")//
        .bytesLength(16)//
        .keyValue("2RJ87sLaFDdhxzhP")//
        .encoding("UTF-8").//
        aesSwitchFlag(Boolean.TRUE);

        final var secureEncryptedData = CryptoProviderUtil.encryptSecureRandom(VALUE_TO_ENCRYPT, model);

        assertThat(secureEncryptedData).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.crypto.CryptoProviderUtil#decrypt(java.lang.String, fr.vincent.tuto.common.model.mo.CryptoModel)}.
     */
    @Test
    public void testDecrypt()
    {
        final var decryptedData = CryptoProviderUtil.decrypt(ENCYTED_DATA, this.cryptoModel);

        assertThat(decryptedData).isNotNull();
        assertThat(decryptedData.length()).isPositive();
        assertThat(decryptedData).containsSequence("JsonContent");
    }

    @Test(expected = CustomAppException.class)
    public void testDecryptShoudlCatchOtherException()
    {
        var model = new CryptoModel()//
        .algorithm("AES")//
        .instancePad(null)//
        .secureRandomInstance("SHA1PRNG")//
        .bytesLength(16)//
        .keyValue("2RJ87sLaFDdhxzhP")//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.TRUE);

        final String decryptedData = CryptoProviderUtil.decrypt(ENCYTED_DATA, model);

        assertThat(decryptedData).isNull();
    }

    @Test(expected = NullPointerException.class)
    public void testDecryptShouldCatchNPE()
    {
        final String decryptedData = CryptoProviderUtil.decrypt(null, null);

        assertThat(decryptedData).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.crypto.CryptoProviderUtil#writeToFile(java.lang.String, java.lang.String)}.
     */
    @Test
    public void testWriteToFile()
    {
        final var destinataion = this.filePath + "Article_Util.txt";
        
        assertThat(destinataion).isNotNull();
        CryptoProviderUtil.writeToFile(ENCYTED_DATA, destinataion);
    }

    @Test(expected = CustomAppException.class)
    public void testWriteToFileShouldCatchOtherException()
    {
        CryptoProviderUtil.writeToFile(ENCYTED_DATA, StringUtils.EMPTY);
    }

    @Test(expected = NullPointerException.class)
    public void testWriteToFileShouldCathNPE()
    {
        CryptoProviderUtil.writeToFile(null, null);
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.crypto.CryptoProviderUtil#encodeFileToBase64(java.lang.String)}.
     */
    @Test
    public void testEncodeFileToBase64()
    {
        final var fileLocation = "./src/test/resources/xsd/article.xsd";
        final var encodedFile = CryptoProviderUtil.encodeFileToBase64(fileLocation);

        assertThat(encodedFile).isNotNull();
        assertThat(encodedFile.length()).isNotNegative();
        assertThat(encodedFile).endsWith("=");
    }

    @Test(expected = CustomAppException.class)
    public void testEncodeFileToBase64ShouldCatchOtherException()
    {
        final var encodedFile = CryptoProviderUtil.encodeFileToBase64(StringUtils.EMPTY);
        assertThat(encodedFile).isNull();
    }

    @Test(expected = NullPointerException.class)
    public void testEncodeFileToBase64ShouldCatchNPE()
    {
        final var encodedFile = CryptoProviderUtil.encodeFileToBase64(null);
        assertThat(encodedFile).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.crypto.CryptoProviderUtil#createSecureRandomSecretKey(java.lang.String, fr.vincent.tuto.common.model.mo.CryptoModel)}.
     */
    @Test
    public void testCreateSecureRandomSecretKey()
    {
        var cleACrypter = "ClefTestCryptageDev";
        final String randomKey = CryptoProviderUtil.createSecureRandomSecretKey(cleACrypter, this.cryptoModel);

        assertThat(randomKey).endsWith("==");
    }

    @Test(expected = CustomAppException.class)
    public void testCreateSecureRandomSecretKeySouldCatchOtherException()
    {
        var model = new CryptoModel()//
        .algorithm("")//
        .instancePad(null)//
        .secureRandomInstance("SHA1PRNG")//
        .bytesLength(16)//
        .keyValue("2RJ87sLaFDdhxzhP")//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.TRUE);

        var cleACrypter = "ClefTestCryptageDev";
        final var randomKey = CryptoProviderUtil.createSecureRandomSecretKey(cleACrypter, model);
        assertThat(randomKey).isNull();
    }

    @Test(expected = NullPointerException.class)
    public void testCreateSecureRandomSecretKeyShouldCatchNPE()
    {
        final var randomKey = CryptoProviderUtil.createSecureRandomSecretKey(null, null);
        assertThat(randomKey).isNull();
    }

    @Test
    public void testAESSModel()
    {
        var model = new CryptoModel();
        model.setAlgorithm("AES");
        model.setInstancePad("SHA1PRNG");
        model.setBytesLength(32);
        model.setKeyValue("2RJ87sLaFDdhxzhP");
        model.setEncoding("UTF-8");//
        model.aesSwitchFlag(Boolean.TRUE);
        model.setSecureRandomInstance(null);
        model.setAesSwitchFlag(null);
        model.setPrivateKey(null);
        model.setPublicKey(null);
        model.setKeystorePassword(null);
        model.setKeyPassword(null);
        model.setAliasKeystore(null);
        model.setSignatureInstance(null);
        
        var model2 = new CryptoModel();//
        
        model2.setAlgorithm("AES");
        model2.setInstancePad("SHA2PRNG");
        model2.setBytesLength(32);
        model2.setKeyValue("2RJ87sLaFDdhxzhP");
        model2.setEncoding("UTF-8");//
        model2 .aesSwitchFlag(Boolean.TRUE);
        model2.setSecureRandomInstance(null);
        model2.setAesSwitchFlag(null);
        model2.setPrivateKey(null);
        model2.setPublicKey(null);
        model2.setKeystorePassword(null);
        model2.setKeyPassword(null);
        model2.setAliasKeystore(null);
        model2.setSignatureInstance(null);

        assertThat(model.getBytesLength()).isPositive();

        assertThat(model.toString()).isNotNull();
        assertThat(model.hashCode()).isNotPositive();
        assertThat(model).isNotSameAs(model2);
    }
}
