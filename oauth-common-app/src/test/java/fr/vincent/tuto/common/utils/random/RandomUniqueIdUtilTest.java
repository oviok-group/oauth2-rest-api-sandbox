/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : RandomUniqueIdUtilTest.java
 * Date de création : 19 nov. 2020
 * Heure de création : 18:03:09
 * Package : fr.vincent.tuto.common.utils
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.utils.random;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;

import org.junit.Test;

import fr.vincent.tuto.common.constants.AppConstants;

/**
 * Classe des Tests Unitaires de l'utiltaire {@link RandomUniqueIdUtil}
 * 
 * @author Vincent Otchoun
 */
public class RandomUniqueIdUtilTest
{

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.random.RandomUniqueIdUtil#generatePassword()}.
     */
    @Test
    public void testGeneratePassword()
    {
        final String password = RandomUniqueIdUtil.generatePassword();

        assertThat(password).isNotEmpty();
        assertThat(password.length()).isPositive();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.random.RandomUniqueIdUtil#generateActivationKey()}.
     */
    @Test
    public void testGenerateActivationKey()
    {
        final String activationKey = RandomUniqueIdUtil.generateActivationKey();

        assertThat(activationKey).isNotEmpty();
        assertThat(activationKey.length()).isPositive();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.random.RandomUniqueIdUtil#generateResetKey()}.
     */
    @Test
    public void testGenerateResetKey()
    {
        final String resetKey = RandomUniqueIdUtil.generateResetKey();

        assertThat(resetKey).isNotEmpty();
        assertThat(resetKey.length()).isPositive();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.random.RandomUniqueIdUtil#generateUniqueId()}.
     */
    @Test
    public void testGenerateUniqueId()
    {
        final String id = RandomUniqueIdUtil.generateUniqueId();

        // System.err.println(">>>>>>>> Identifiant Unique = \n" + id);
        assertThat(id).isNotEmpty();
        assertThat(id.length()).isPositive();
        assertThat(AppConstants.UUID_PATTERN.matcher(id).matches()).isTrue();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.random.RandomUniqueIdUtil#generateUUID(byte[])}.
     */
    @Test
    public void testGenerateUUID()
    {
        String id = "2df7980a-2f28-423b-9624-e7f4dda77288";
        final UUID uuid = RandomUniqueIdUtil.generateUUID(id.getBytes());

        assertThat(uuid).isNotNull();
        assertThat(uuid.toString()).isNotNull();
        assertThat(uuid.toString()).isNotEmpty();
    }
}
