package fr.vincent.tuto.common;
/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : CommonAppTestStarter.java
 * Date de création : 30 nov. 2020
 * Heure de création : 09:27:30
 * Package : 
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * @author Vincent Otchoun
 *
 */
@SpringBootApplication
public class CommonAppTestStarter
{
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        //
        new SpringApplicationBuilder(CommonAppTestStarter.class).run(args);

    }

}
