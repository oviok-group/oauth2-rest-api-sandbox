/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : Article.java
 * Date de création : 19 nov. 2020
 * Heure de création : 19:48:29
 * Package : fr.vincent.tuto.common
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.model;

import java.io.Serializable;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import fr.vincent.tuto.common.constants.AppConstants;

/**
 * Objet de test.
 * 
 * @author Vincent Otchoun
 */
@JsonPropertyOrder({ "refArticle", "titleArticle", "categorieArticle", "dateCreationArticle" })
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Article implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -2051693896232984026L;

    @JsonProperty(value = "refArticle", access = JsonProperty.Access.AUTO)
    @JsonPropertyDescription("La référence de l'article")
    private Long refArticle;

    @JsonProperty(value = "titleArticle")
    @JsonPropertyDescription("Le titre de l'article")
    private String titleArticle;

    @JsonProperty(value = "categorieArticle")
    @JsonPropertyDescription("La catégorie de l'article")
    private String categorieArticle;

    @JsonProperty(value = "dateCreationArticle")
    @JsonPropertyDescription("La date de creation de l'article")
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = AppConstants.CET_DATE_FORMAT_WITHOUT_TIMEZONE_TEXT, locale = AppConstants.FR_LOCALE, timezone = AppConstants.CET_TIMEZONE)
    private LocalDateTime dateCreationArticle;

    /**
     * Constructuer
     */
    public Article()
    {
        this.dateCreationArticle = LocalDateTime.now(Clock.system(ZoneId.systemDefault()));
        //LocalDateTime.now(ZoneId.of(AppConstants.CET_TIMEZONE));
        //ZonedDateTime.now().toLocalDateTime();
        // 
        // 
        // LocalDateTime.now(ZoneId.systemDefault());
    }

    public Long getRefArticle()
    {
        return this.refArticle;
    }

    public String getTitleArticle()
    {
        return this.titleArticle;
    }

    public String getCategorieArticle()
    {
        return this.categorieArticle;
    }

    public LocalDateTime getDateCreationArticle()
    {
        return this.dateCreationArticle;
    }

    public void setRefArticle(final Long pRefArticle)
    {
        this.refArticle = pRefArticle;
    }

    public void setTitleArticle(final String pTitleArticle)
    {
        this.titleArticle = pTitleArticle;
    }

    public void setCategorieArticle(final String pCategorieArticle)
    {
        this.categorieArticle = pCategorieArticle;
    }

    public void setDateCreationArticle(final LocalDateTime pDateCreationArticle)
    {
        this.dateCreationArticle = pDateCreationArticle;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(java.lang.Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Article article = (Article) o;
        return Objects.equals(this.refArticle, article.refArticle)//
        && Objects.equals(this.titleArticle, article.titleArticle)//
        && Objects.equals(this.categorieArticle, article.categorieArticle)//
        && Objects.equals(this.dateCreationArticle, article.dateCreationArticle);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(this.refArticle, this.titleArticle, this.categorieArticle, this.dateCreationArticle);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
