/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : CommonAppTestsUtil.java
 * Date de création : 24 nov. 2020
 * Heure de création : 22:48:17
 * Package : fr.vincent.tuto.common
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common;

import java.security.Key;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.google.common.collect.Maps;

import fr.vincent.tuto.common.enumeration.BaseRolesEnum;
import fr.vincent.tuto.common.utils.auth.AuthProviderUtil;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

/**
 * Utilitaire commun pour les tests dans le module applicatif.
 * 
 * @author Vincent Otchoun
 */
public final class CommonAppTestsUtil
{
    //
    public static final String USER_ONE_USERNAME = "admin1";
    public static final String USER_ONE_EMAIL = "admin1.test@live.fr";
    public static final String USER_TWO_USERNAME = "admin2";
    public static final String USER_TWO_EMAIL = "admin2.test@live.fr";
    public static final String USER_THREE_USERNAME = "client1";
    public static final String USER_THREE_EMAIL = "client1.test@live.fr";

    //
    public static final String AUTH_TOKEN_PREFIX = "Bearer ";
    public static final String BASIC_AUTH_PREFIX = "Basic ";
    public static final String TOKEN_PREFIX = "Bearer";
    public static final String AUTHORIZATION_HEADER = "Authorization";

    public static final String PAYLOAD_DATA = "payload";
    public static final Long ONE_MINUTE = 60000L;
    public static final String ANONYMOUS_ROLE = "anonymous1";
    public static final String NOT_EXIST_ANONYMOUS_ROLE = "anonymous";

    public static final String REAL_BASE64_TEST_TOKEN = "87ZxNTF3hqIxFpKB0n06EeH5femEJ/vf7f/C2Ie7BOkSTV/GynzjR4WdOGKM5DZRi+ZMiSAy0XgDFlOS/nyTHw==";
    public static final String BAD_BASE64_TEST_TOKEN = "kuse9ecUZO7Blnpij6fPknsHufalY0J50go9YV5KzhapJvIsQ/L1Qyq9h1IYLqQzpOsOv3Oh1t/1BGbcsL3FKw==";
    public static final String AUTHORITIES_KEY = "auth";

    public static final String ADMIN = "admin";
    public static final String TOKEN = "token";
    public static final String ANONYMOUS = "anonymous";
    public static final String CLIENT = "client";
    public static final String AUTH_TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhbm9ueW1vdXMiLCJleHAiOjE2MDM4NzgyMzF9.-V9sx5fnyHT7mY1B0e1vp1MY-OVEGW_DvOKOhD6wtB-HEZOa6Je3z1huJyJchD-iIRtaeaZKaL8jGAanoqrlMQ";

    /**
     * Constructeur
     */
    private CommonAppTestsUtil()
    {
    }

    /**
     * Création de jeton d'authentification non valide.
     * 
     * @param testKey la clé de signature u jeton d'authentification.
     * @return le jeton d'authentification.
     */
    public static String createUnsupportedToken(final Key testKey)
    {
        return Jwts.builder()//
        .setPayload(PAYLOAD_DATA)//
        .signWith(testKey, SignatureAlgorithm.HS512)//
        .compact();
    }

    /**
     * Construire le jeton d'authentification de l'utilisateur avec une clé encodée Base64.
     * 
     * @param pBase64SecretKey le mot de passe encodé Base64.
     * @param pUser            l'utilisateur courant.
     * @return le jeton d'authentification.
     */
    public static String createTestToken(final String pBase64SecretKey, final String pUser)
    {
        final Key otherKey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(pBase64SecretKey));
        final Map<String, Object> header = Maps.newHashMap();
        header.put(Header.TYPE, Header.JWT_TYPE);

        return Jwts.builder()//
        .setId(UUID.randomUUID().toString())//
        .setIssuedAt(new Date())//
        .setSubject(pUser)//
        .setHeader(header)
        // .signWith(otherKey)//
        .signWith(otherKey, SignatureAlgorithm.HS512)//
        .setExpiration(new Date(new Date().getTime() + ONE_MINUTE))//
        .compact();
    }

    /**
     * Construire le jeton de la demande d'authentification.
     * 
     * @return le jeton de la demande d'authentification.
     */
    public static Authentication createAuthentication()
    {
        return new UsernamePasswordAuthenticationToken(ANONYMOUS_ROLE, "anonymous", Collections.singletonList(
        new SimpleGrantedAuthority(BaseRolesEnum.ANONYMOUS.getAuthority())));
    }

    /**
     * Construire le jeton de la demande d'authentification par recherche en abse de données des informations
     * d'authentification de l'utilisateur.
     *
     * @param pUserDetailsService
     * @return
     */
    public static Authentication createDataBaseAuthentication(final UserDetailsService pUserDetailsService)
    {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        // authorities.add(new SimpleGrantedAuthority(BaseRolesEnum.ANONYMOUS.getAuthority()));
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.ANONYMOUS.getAuthority()));

        final UserDetails springSecurityUser = pUserDetailsService.loadUserByUsername(BaseRolesEnum.ANONYMOUS
        .getAuthority());
        // return new UsernamePasswordAuthenticationToken(springSecurityUser, "anonymous", springSecurityUser
        // .getAuthorities());
        return AuthProviderUtil.createAuthentication((User) springSecurityUser, authorities);
    }



}
