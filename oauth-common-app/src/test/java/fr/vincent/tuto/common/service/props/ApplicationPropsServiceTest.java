/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : ApplicationPropsServiceTest.java
 * Date de création : 19 nov. 2020
 * Heure de création : 13:38:43
 * Package : fr.vincent.tuto.common.service.props
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.service.props;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.common.config.CommonAppBaseConfig;
import fr.vincent.tuto.common.service.props.ApplicationPropsService.AsyncProps;
import fr.vincent.tuto.common.service.props.ApplicationPropsService.CorsProps;
import fr.vincent.tuto.common.service.props.ApplicationPropsService.CryptoProps;
import fr.vincent.tuto.common.service.props.ApplicationPropsService.EhcacheProps;
import fr.vincent.tuto.common.service.props.ApplicationPropsService.JwtProps;
import fr.vincent.tuto.common.service.props.ApplicationPropsService.MailProps;
import fr.vincent.tuto.common.service.props.ApplicationPropsService.MessageProps;
import fr.vincent.tuto.common.service.props.ApplicationPropsService.OauthProps;
import fr.vincent.tuto.common.service.props.ApplicationPropsService.SwaggerProps;

/**
 * Classe des Tests Unitaires des objets de type {@link ApplicationPropsService}
 * 
 * @author Vincent Otchoun
 */
@SpringBootApplication
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:common-app-db-test.properties",
        "classpath:common-app-application-test.properties", "classpath:common-app-messages-test.properties" })
@ContextConfiguration(name = "applicationPropsServiceTest", classes = { CommonAppBaseConfig.class,
        ApplicationPropsService.class, JavaMailSenderImpl.class })
@SpringBootTest
@ActiveProfiles("test")
public class ApplicationPropsServiceTest
{
    //
    @Autowired
    private ApplicationPropsService propsService;

    /**
     * Test method for {@link fr.vincent.tuto.common.service.props.ApplicationPropsService#getSwaggerProps()}.
     */
    @Test
    public void testGetSwaggerProps()
    {
        final SwaggerProps swagger = this.propsService.getSwaggerProps();

        assertThat(swagger).isNotNull();

        assertThat(swagger.getTitle().trim()).isEqualTo("API Authentification avec JWT");
        assertThat(swagger.getDescription().trim()).contains(">Gestion des droits :");
        assertThat(swagger.getVersion().trim()).isEqualTo("3.0");
        assertThat(swagger.getTermeOfServiceUrl().trim()).isEqualTo("1.0.0");
        assertThat(swagger.getContactName().trim()).containsIgnoringCase("vincent");
        assertThat(swagger.getContactUrl().trim()).contains("tuto.vincent.fr");
        assertThat(swagger.getContactEmail().trim()).contains("bleckyss31");
        assertThat(swagger.getLicence().trim()).isEqualTo("Apache 2.0");
        assertThat(swagger.getLicenceUrl().trim()).contains("http://www.apache.org/");
        assertThat(swagger.getDefaultIncludePattern().trim()).isEqualTo("1.0.0");
        assertThat(swagger.getHost().trim()).isEqualTo("host1");
        assertThat(swagger.getProtocols().size()).isEqualTo(3);
        assertThat(swagger.getProtocols().get(0).trim()).isEqualTo("protocol1");
        assertThat(swagger.getUseDefaultResponseMessages()).isFalse();
        assertThat(swagger.getCodeGeneration()).isFalse();
        assertThat(swagger.getAppBasePackage().trim()).isEqualTo("fr.vincent.tuto.controller");
        assertThat(swagger.getContexteRacine()).isEmpty();
        assertThat(swagger.getNomMachineDev().trim()).isEqualToIgnoringCase("OVIOK");
        assertThat(swagger.getContexteRacineDefault().trim()).isEqualTo("/");

        assertThat(swagger.getUrlPaths()).isNotEmpty();
        assertThat(Arrays.asList(StringUtils.split(swagger.getUrlPaths(), ",")).size()).isPositive();
        assertThat(Arrays.asList(StringUtils.split(swagger.getUrlPaths(), ","))).contains("/v3/api-docs**");

        assertThat(swagger.toString()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.service.props.ApplicationPropsService#getJwtProps()}.
     */
    @Test
    public void testGetJwtProps()
    {
        final JwtProps jwt = this.propsService.getJwtProps();

        assertThat(jwt.getTokenSecret()).isNotNull();
        assertThat(jwt.getBase64TokenSecret()).isNotNull();
        assertThat(jwt.getTokenValidity()).isPositive();
        assertThat(jwt.getTokenValidityForRememberMe()).isPositive();
        assertThat(jwt.getRefreshTokenValidity()).isEqualTo(86400);

        assertThat(jwt.getAuthorizationHeader()).isEqualTo("Authorization");
        assertThat(jwt.getAuthoritiesKey()).isEqualTo("auth");
        assertThat(jwt.getBearerToken()).isEqualTo("Bearer ");

        assertThat(jwt.toString()).isNotEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.service.props.ApplicationPropsService#getOauthProps()}.
     */
    @Test
    public void testGetOauthProps()
    {
        final OauthProps oauthProps = this.propsService.getOauthProps();

        assertThat(oauthProps.getClientId()).isEqualTo("client");
        assertThat(oauthProps.getClientSecret()).isEqualTo("secret");
        assertThat(oauthProps.getAccessTokenValidity()).isEqualTo(86400);
        assertThat(oauthProps.getRefreshTokenValidity()).isEqualTo(86400);
        assertThat(StringUtils.split(oauthProps.getAuthorizedGrantTypes(), ",")).contains("password");
        assertThat(oauthProps.getPrivateKey()).isEmpty();
        assertThat(oauthProps.getPublicKey()).isEmpty();
        assertThat(oauthProps.getAccessTokenUri()).isEqualTo("http://localhost:8080/api/oauth/token");
        assertThat(oauthProps.getUserAuthorizationUri()).isEqualTo("http://localhost:8080/api/oauth/authorize");
        assertThat(oauthProps.getResourceUri()).isEqualTo("//localhost:8080/api/oauth/authorize");

        assertThat(oauthProps.toString()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.service.props.ApplicationPropsService#getCorsProps()}.
     */
    @Test
    public void testGetCorsProps()
    {
        final CorsProps cors = this.propsService.getCorsProps();

        assertThat(cors).isNotNull();
        assertThat(cors.getOrigin()).isEqualTo("*");
        assertThat(cors.getCredentials()).isTrue();
        assertThat(cors.getMethods()).isNotNull();
        assertThat(Arrays.asList(StringUtils.split(cors.getMethods(), ",")).size()).isEqualTo(7);
        assertThat(cors.getMaxAge()).isPositive();
        assertThat(cors.getHeaders()).isNotNull();
        assertThat(Arrays.asList(StringUtils.split(cors.getHeaders(), ","))).contains("Authorization");

        assertThat(cors.toString()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.service.props.ApplicationPropsService#getMailProps()}.
     */
    @Test
    public void testGetMailProps()
    {
        //
        final MailProps mail = this.propsService.getMailProps();

        assertThat(mail).isNotNull();
        assertThat(mail.getHost()).isNotEqualTo("localhost");
        assertThat(mail.getPort()).isNotEqualTo(25);
        assertThat(mail.getUsername()).isNotEqualTo("test");
        assertThat(mail.getPassword()).isNotEqualTo("test-mail_");
        assertThat(mail.getProtocol()).isEqualToIgnoringCase("SMTP");
        assertThat(mail.getTls()).isTrue();
        assertThat(mail.getAuth()).isTrue();
        assertThat(mail.getFrom()).isNotEqualTo("@localhost");
        
        assertThat(mail.getQuitWait()).isFalse();

        assertThat(mail.toString()).isNotEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.service.props.ApplicationPropsService#getAsyncProps()}.
     */
    @Test
    public void testGetAsyncProps()
    {
        final AsyncProps async = this.propsService.getAsyncProps();

        assertThat(async).isNotNull();
        assertThat(async.getCorePoolSize()).isNotNull();
        assertThat(async.getMaxPoolSize()).isNotNull();
        assertThat(async.getQueueCapacity()).isNotNull();

        assertThat(async.toString()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.service.props.ApplicationPropsService#getCryptoProps()}.
     */
    @Test
    public void testGetCryptoProps()
    {
        final CryptoProps cryptoProps = this.propsService.getCryptoProps();

        assertThat(cryptoProps).isNotNull();
        assertThat(cryptoProps.getAlgorithm()).isEqualToIgnoringCase("AES");
        assertThat(cryptoProps.getInstancePad()).isEqualToIgnoringCase("AES/ECB/PKCS5Padding");
        assertThat(cryptoProps.getSecureRandomInstance()).isEqualToIgnoringCase("SHA1PRNG");

        assertThat(cryptoProps.getBytesLength()).isEqualTo(16);
        assertThat(cryptoProps.getKeyValue()).isEqualToIgnoringCase("2RJ87sLaFDdhxzhP");
        assertThat(cryptoProps.getEncoding()).isNotNull();
        assertThat(cryptoProps.getEncoding()).isEqualTo("UTF-8");
        // assertThat(Arrays.asList(StringUtils.split(aesProps.getEncoding(), ",")).size()).isGreaterThan(0);

        // RSA
        assertThat(cryptoProps.getPrivateKey()).isEmpty();
        assertThat(cryptoProps.getPublicKey()).isEmpty();
        assertThat(cryptoProps.getKeystorePassword()).contains("recette");
        assertThat(cryptoProps.getKeyPassword()).contains("recette");
        assertThat(cryptoProps.getAliasKeystore()).contains("recette");
        assertThat(cryptoProps.getSignatureInstance()).isEqualTo("SHA256withRSA");

        assertThat(cryptoProps.getKeystoreFileLocation()).isNotNull();
        assertThat(cryptoProps.getKeystoreFileLocation()).contains("app-recette-keystore.jks");
        assertThat(cryptoProps.getPublicKeyFileLocation()).isNotNull();
        assertThat(cryptoProps.getPublicKeyFileLocation()).contains("app_recette_publicKey.pub.txt");
        
        // new 2021 - 02- 27
        assertThat(cryptoProps.getKeyStoreType()).containsIgnoringCase("jks");
        assertThat(cryptoProps.getTrustStorePath()).contains("app-recette.p12");
        assertThat(cryptoProps.getTrustStorePassword()).contains("recette");
        assertThat(cryptoProps.getTrustStoreType()).containsIgnoringCase("pkcs12");
        assertThat(cryptoProps.getHttpsProtocols()).containsIgnoringCase("tlsv");

        assertThat(cryptoProps.toString()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.service.props.ApplicationPropsService#getEhcacheProps()}.
     */
    @Test
    public void testGetEhcacheProps()
    {
        final EhcacheProps ehcacheProps = this.propsService.getEhcacheProps();

        assertThat(ehcacheProps.getMaxBytesLocalHeap()).isNotEmpty();
        assertThat(ehcacheProps.getTimeToLiveSeconds()).isNotNull();
        assertThat(ehcacheProps.getMaxEntries()).isNotNull();

        assertThat(ehcacheProps.toString()).isNotNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.service.props.ApplicationPropsService#getMessageProps()}.
     */
    @Test
    public void testGetMessageProps()
    {
        final MessageProps messageProps = this.propsService.getMessageProps();

        assertThat(messageProps.getAppNameKey()).isNotEmpty();
        assertThat(messageProps.getSecurityKey()).isNotNull();
        assertThat(messageProps.getServerPortKey()).isNotNull();
        assertThat(messageProps.getContextPathKey()).isNotNull();
        assertThat(messageProps.getDefaultProfileKey()).isNotNull();

        assertThat(messageProps.getProtocol()).isNotNull();
        assertThat(messageProps.getAppSecureProtocol()).isNotNull();
        assertThat(messageProps.getDefaultContextPath()).isNotNull();
        assertThat(messageProps.getHostnameErrorMessage()).isNotNull();
        assertThat(messageProps.getStartLogMessage()).isNotNull();
        assertThat(messageProps.getHostname()).isNotNull();

        assertThat(messageProps.getProdDevErrorMessage()).isNotNull();
        assertThat(messageProps.getCloudDevErrorMessage()).isNotNull();

        assertThat(messageProps.toString()).isNotNull();
    }


}
