/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : KeyPairProviderUtilTest.java
 * Date de création : 23 nov. 2020
 * Heure de création : 14:16:35
 * Package : fr.vincent.tuto.common.utils.crypto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.utils.crypto;

import static org.assertj.core.api.Assertions.assertThat;

import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.common.config.CommonAppBaseConfig;
import fr.vincent.tuto.common.constants.AppConstants;
import fr.vincent.tuto.common.exception.CustomAppException;
import fr.vincent.tuto.common.model.mo.CryptoModel;

/**
 * Classe des Tests Unitaires des objets de type {@link KeyPairProviderUtil}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:common-app-db-test.properties", "classpath:common-app-application-test.properties",
        "classpath:common-app-messages-test.properties" })
@ContextConfiguration(name = "keyPairProviderUtilTest", classes = { CommonAppBaseConfig.class, KeyPairProviderUtil.class, JavaMailSenderImpl.class })
@ActiveProfiles("test")
public class KeyPairProviderUtilTest
{
    //
    private static final String TEXT_CHIFFREMENT = "L'univers et tout est la réponse à la vie.";
    private static final String SIGNATURE_TEXT = "Texte à signer avec algorithme RSA.";

    //
    private CryptoModel cryptoModel;
    private String keystorePath;
    private String privateKeyPath;
    private String publicKeyPath;

    @Value("${vot.crypto.file.test.in.path}")
    private String inFilePath;
    @Value("${vot.crypto.file.test.out.path}")
    private String outFilesPath;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.cryptoModel = new CryptoModel()//
        .algorithm("RSA")//
        .instancePad(null)//
        .secureRandomInstance(null)//
        .bytesLength(null)//
        .keyValue(null)//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.FALSE)//
        .privateKey(null)//
        .publicKey(null)//
        .keystorePassword("app-recette")//
        .keyPassword("app-recette")//
        .aliasKeystore("app-recette")//
        .signatureInstance("SHA384RSA");

        this.keystorePath = this.inFilePath + "app-recette-keystore.jks";
        this.privateKeyPath = this.inFilePath + "app_recette_privateKey.pkcs8.txt";
        this.publicKeyPath = this.inFilePath + "app_recette_publicKey.pub.txt";

    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.cryptoModel = null;
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#generateKeyPair(java.lang.String)}.
     */
    @Test
    public void testGenerateKeyPair()
    {
        final var keyPair = KeyPairProviderUtil.generateKeyPair(this.cryptoModel.getAlgorithm());

        assertThat(keyPair).isNotNull();
        assertThat(keyPair.getPrivate()).isNotNull();
        assertThat(keyPair.getPrivate().getAlgorithm()).isEqualTo(AppConstants.RSA_ALGORITHM_KEY);
        assertThat(keyPair.getPrivate().getEncoded()).isNotNull();

        assertThat(keyPair.getPublic()).isNotNull();
        assertThat(keyPair.getPublic().getAlgorithm()).isEqualTo(AppConstants.RSA_ALGORITHM_KEY);
        assertThat(keyPair.getPublic().getEncoded()).isNotNull();
    }

    @Test(expected = NullPointerException.class)
    public void testGenerateKeyPairShouldCatchNPE()
    {
        final var keyPair = KeyPairProviderUtil.generateKeyPair(null);

        assertThat(keyPair).isNull();
    }

    @Test(expected = CustomAppException.class)
    public void testGenerateKeyPairShouldCatchOtherException()
    {
        final var keyPair = KeyPairProviderUtil.generateKeyPair("");

        assertThat(keyPair).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#getKeyPairFromKeystore(java.lang.String, fr.vincent.tuto.common.model.mo.CryptoModel)}.
     */
    @Test
    public void testGetKeyPairFromKeystore()
    {
        final var keyPair = KeyPairProviderUtil.getKeyPairFromKeystore(this.keystorePath, this.cryptoModel);

        //
        assertThat(keyPair).isNotNull();
        assertThat(keyPair.getPrivate()).isNotNull();
        assertThat(keyPair.getPrivate().getAlgorithm()).isEqualTo(AppConstants.RSA_ALGORITHM_KEY);
        assertThat(keyPair.getPrivate().getEncoded()).isNotNull();

        assertThat(keyPair.getPublic()).isNotNull();
        assertThat(keyPair.getPublic().getAlgorithm()).isEqualTo(AppConstants.RSA_ALGORITHM_KEY);
        assertThat(keyPair.getPublic().getEncoded()).isNotNull();
    }

    @Test(expected = NullPointerException.class)
    public void testGetKeyPairFromKeystoreShouldCatchNPE()
    {
        final var keyPair = KeyPairProviderUtil.getKeyPairFromKeystore(null, null);

        assertThat(keyPair).isNull();
    }

    @Test(expected = CustomAppException.class)
    public void testGetKeyPairFromKeystoreShouldCatchOtherException()
    {
        var cryptoModel = new CryptoModel()//
        .algorithm("RSA")//
        .instancePad(null)//
        .secureRandomInstance(null)//
        .bytesLength(null)//
        .keyValue(null)//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.FALSE)//
        .privateKey(null)//
        .publicKey(null)//
        .keystorePassword("app-recette-test")//
        .keyPassword("app-recette-test")//
        .aliasKeystore("app-recette")//
        .signatureInstance("SHA384RSA");

        final var keyPair = KeyPairProviderUtil.getKeyPairFromKeystore(this.keystorePath, cryptoModel);

        assertThat(keyPair).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#getKeyFromKeystore(java.lang.String, fr.vincent.tuto.common.model.mo.CryptoModel)}.
     */
    @Test
    public void testGetKeyFromKeystore()
    {
        final var key = KeyPairProviderUtil.getKeyFromKeystore(this.keystorePath, this.cryptoModel);

        //
        assertThat(key).isNotNull();
        assertThat(key.getAlgorithm()).isEqualTo(AppConstants.RSA_ALGORITHM_KEY);
        assertThat(key.getEncoded()).isNotNull();
    }

    @Test(expected = NullPointerException.class)
    public void testGetKeyFromKeystoreShouldCatchNPE()
    {
        final var key = KeyPairProviderUtil.getKeyFromKeystore(null, null);

        //
        assertThat(key).isNull();
    }

    @Test(expected = CustomAppException.class)
    public void testGetKeyFromKeystoreShouldCatchOtherException()
    {
        var cryptoModel = new CryptoModel()//
        .algorithm("RSA")//
        .instancePad(null)//
        .secureRandomInstance(null)//
        .bytesLength(null)//
        .keyValue(null)//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.FALSE)//
        .privateKey(null)//
        .publicKey(null)//
        .keystorePassword("app-recette-test")//
        .keyPassword("app-recette-test")//
        .aliasKeystore("app-recette")//
        .signatureInstance("SHA384RSA");

        final var key = KeyPairProviderUtil.getKeyFromKeystore(this.keystorePath, cryptoModel);

        //
        assertThat(key).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#getCertFromKeystore(java.lang.String, fr.vincent.tuto.common.model.mo.CryptoModel)}.
     */
    @Test
    public void testGetCertFromKeystore()
    {
        final var cert = KeyPairProviderUtil.getCertFromKeystore(this.keystorePath, this.cryptoModel);

        final var certOutPath = this.outFilesPath + "keystore-cert-out.txt";
        KeyPairProviderUtil.writeToFile(cert, certOutPath);

        assertThat(cert).isNotNull();
    }

    @Test(expected = NullPointerException.class)
    public void testGetCertFromKeystoreShouldCatchNPE()
    {
        final var cert = KeyPairProviderUtil.getCertFromKeystore(null, null);

        assertThat(cert).isNull();
    }

    @Test(expected = CustomAppException.class)
    public void testGetCertFromKeystoreShouldCatchOtherException()
    {
        var cryptoModel = new CryptoModel()//
        .algorithm("RSA")//
        .instancePad(null)//
        .secureRandomInstance(null)//
        .bytesLength(null)//
        .keyValue(null)//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.FALSE)//
        .privateKey(null)//
        .publicKey(null)//
        .keystorePassword("app-recette-test")//
        .keyPassword("app-recette-test")//
        .aliasKeystore("app-recette")//
        .signatureInstance("SHA384RSA");

        final var cert = KeyPairProviderUtil.getCertFromKeystore(this.keystorePath, cryptoModel);

        assertThat(cert).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#getKeyFromFile(java.lang.String)}.
     */
    @Test
    public void testGetKeyFromFile()
    {
        final var keyStr = KeyPairProviderUtil.getKeyFromFile(this.privateKeyPath);

        assertThat(keyStr.toString()).containsSequence(AppConstants.PRIVATE_KEY_BEGIN);
        assertThat(keyStr).containsSequence(AppConstants.PRIVATE_KEY_END);
    }

    @Test
    public void testGetKeyFromFileWithPublicKey()
    {
        final var keyStr = KeyPairProviderUtil.getKeyFromFile(this.publicKeyPath);

        assertThat(keyStr.toString()).containsSequence(AppConstants.PUBLIC_KEY_BEGIN);
        assertThat(keyStr).containsSequence(AppConstants.PUBLIC_KEY_END);
    }

    @Test(expected = NullPointerException.class)
    public void testGetKeyFromFileShouldCatchNPE()
    {
        final var keyStr = KeyPairProviderUtil.getKeyFromFile(null);
        assertThat(keyStr).isNull();
    }

    @Test(expected = CustomAppException.class)
    public void testGetKeyFromFileShouldCatchException()
    {
        final var keyStr = KeyPairProviderUtil.getKeyFromFile("");
        assertThat(keyStr).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#getPrivateKey(java.security.KeyPair)}.
     */
    @Test
    public void testGetPrivateKey()
    {
        final var keyPair = KeyPairProviderUtil.generateKeyPair(this.cryptoModel.getAlgorithm());
        final PrivateKey privateKey = KeyPairProviderUtil.getPrivateKey(keyPair);

        assertThat(privateKey).isNotNull();
        assertThat(privateKey.getAlgorithm()).isNotEmpty();
        assertThat(privateKey.getAlgorithm()).isEqualTo(AppConstants.RSA_ALGORITHM_KEY);
    }

    @Test(expected = NullPointerException.class)
    public void testGetPrivateKeyShouldCatchNPE()
    {
        final var privateKey = KeyPairProviderUtil.getPrivateKey(null);
        assertThat(privateKey).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#rsaPrivateKey(java.security.KeyPair)}.
     */
    @Test
    public void testRsaPrivateKey()
    {
        final var keyPair = KeyPairProviderUtil.getKeyPairFromKeystore(this.keystorePath, this.cryptoModel);
        var privateKey = (RSAPrivateKey) keyPair.getPrivate();
        final var privateKeyStr = KeyPairProviderUtil.rsaPrivateKey(privateKey);

        assertThat(privateKey).isNotNull();
        assertThat(privateKeyStr).isNotEmpty();
    }

    @Test(expected = NullPointerException.class)
    public void testRsaPrivateKeyShouldCatchNPE()
    {
        final var privateKey = KeyPairProviderUtil.rsaPrivateKey(null);
        assertThat(privateKey).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#getPrivateKeyFromString(java.lang.String)}.
     */

    @Test
    public void testGetPrivateKeyFromStringWithKeystoreData()
    {
        final var keyFilePath = this.outFilesPath + "keystore-private-key-out.txt";
        final var keyStr = KeyPairProviderUtil.getKeyFromFile(keyFilePath);

        final var privateKey = KeyPairProviderUtil.getPrivateKeyFromString(keyStr);
        assertThat(privateKey).isNotNull();
    }

    @Test
    public void testGetPrivateKeyFromString()
    {
        final String keyFilePath = this.outFilesPath + "keystore-private-key-out.txt";
        final String keystoreStr = KeyPairProviderUtil.getKeyFromFile(keyFilePath);
        final String keyStr = KeyPairProviderUtil.getKeyFromFile(this.privateKeyPath);

        final RSAPrivateKey privateKey = KeyPairProviderUtil.getPrivateKeyFromString(keyStr);
        assertThat(privateKey).isNotNull();
        assertThat(keystoreStr).isNotNull();
        assertThat(keyStr).isNotNull();
        assertThat(privateKey).isEqualTo(KeyPairProviderUtil.getPrivateKeyFromString(keystoreStr));
    }

    @Test(expected = NullPointerException.class)
    public void testGetPrivateKeyFromStringWithNullParam()
    {
        final var privateKey = KeyPairProviderUtil.getPrivateKeyFromString(null);
        assertThat(privateKey).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#getPrivateKeyFromFile(java.lang.String)}.
     */
    @Test
    public void testGetPrivateKeyFromFile()
    {
        final var keyFilePath = this.outFilesPath + "keystore-private-key-out.txt";
        final var keystoreStr = KeyPairProviderUtil.getKeyFromFile(keyFilePath);
        final var keyStr = KeyPairProviderUtil.getKeyFromFile(this.privateKeyPath);

        final var privateKey = KeyPairProviderUtil.getPrivateKeyFromFile(this.privateKeyPath);
        assertThat(privateKey).isNotNull();
        assertThat(keystoreStr).isNotNull();
        assertThat(keyStr).isNotNull();
        assertThat(keystoreStr).isEqualTo(KeyPairProviderUtil.rsaPrivateKey(privateKey));
    }

    // @Test(expected = NullPointerException.class)
    // public void testGetPrivateKeyFromFile_ShouldCatchNPE()
    // {
    // final RSAPrivateKey privateKey = KeyPairProviderUtil.getPrivateKeyFromFile(null);
    //
    // assertThat(privateKey).isNull();
    // }

    // @Test(expected = CustomAppException.class)
    // public void testGetPrivateKeyFromFileShouldCatchOther()
    // {
    // final RSAPrivateKey privateKey = KeyPairProviderUtil.getPrivateKeyFromFile("");
    // assertThat(privateKey).isNull();
    // }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#getPublicKey(java.security.KeyPair)}.
     */
    @Test
    public void testGetPublicKey()
    {

        final var keyPair = KeyPairProviderUtil.generateKeyPair(this.cryptoModel.getAlgorithm());
        final var publicKey = KeyPairProviderUtil.getPublicKey(keyPair);

        assertThat(publicKey).isNotNull();
        assertThat(publicKey.getAlgorithm()).isNotEmpty();
        assertThat(publicKey.getAlgorithm()).isEqualTo(AppConstants.RSA_ALGORITHM_KEY);
    }

    @Test(expected = NullPointerException.class)
    public void testGetPublicKeyShouldCatchNPE()
    {
        final var privateKey = KeyPairProviderUtil.getPrivateKeyFromFile(null);
        assertThat(privateKey).isNull();
    }

    @Test(expected = CustomAppException.class)
    public void testGetPublicKeyShouldCatchOther()
    {
        final var privateKey = KeyPairProviderUtil.getPrivateKeyFromFile("");
        assertThat(privateKey).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#rsaPublicKey(java.security.KeyPair)}.
     */
    @Test
    public void testRsaPublicKey()
    {
        final var keyPair = KeyPairProviderUtil.getKeyPairFromKeystore(this.keystorePath, this.cryptoModel);
        var rsaPublicKey = (RSAPublicKey) keyPair.getPublic();
        final var publicKeyStr = KeyPairProviderUtil.rsaPublicKey(rsaPublicKey);

        assertThat(publicKeyStr).isNotNull();
        assertThat(publicKeyStr.toString()).isNotEmpty();
    }

    @Test(expected = NullPointerException.class)
    public void testRsaPublicKeyShouldCatchNPE()
    {
        final var publicKey = KeyPairProviderUtil.rsaPublicKey(null);
        assertThat(publicKey).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#getPublicKeyFromString(java.lang.String)}.
     */
    @Test
    public void testGetPublicKeyFromString()
    {
        final var keyFilePath = this.outFilesPath + "keystore-public-key-out.txt";
        final var keystoreStr = KeyPairProviderUtil.getKeyFromFile(keyFilePath);
        final var keyStr = KeyPairProviderUtil.getKeyFromFile(this.publicKeyPath);

        final var rsaPublicKey = KeyPairProviderUtil.getPublicKeyFromString(keyStr);
        assertThat(rsaPublicKey).isNotNull();
        assertThat(keystoreStr).isNotNull();
        assertThat(keyStr).isNotNull();
        assertThat(rsaPublicKey).isEqualTo(KeyPairProviderUtil.getPublicKeyFromString(keystoreStr));
    }

    @Test
    public void testGetPublicKeyFromStringWithKeystoreData()
    {
        final var keyFilePath = this.outFilesPath + "keystore-public-key-out.txt";
        final var keyStr = KeyPairProviderUtil.getKeyFromFile(keyFilePath);

        final var publicKey = KeyPairProviderUtil.getPublicKeyFromString(keyStr);
        assertThat(publicKey).isNotNull();
    }

    @Test(expected = NullPointerException.class)
    public void testGetPublicKeyFromStringShouldCatchNPE()
    {
        final var rsaPublicKey = KeyPairProviderUtil.getPublicKeyFromString(null);
        assertThat(rsaPublicKey).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#getPublicKeyFromFile(java.lang.String)}.
     */
    @Test
    public void testGetPublicKeyFromFile()
    {
        final var keyFilePath = this.outFilesPath + "keystore-public-key-out.txt";
        final var keystoreStr = KeyPairProviderUtil.getKeyFromFile(keyFilePath);
        final var keyStr = KeyPairProviderUtil.getKeyFromFile(this.publicKeyPath);

        final var rsaPublicKey = KeyPairProviderUtil.getPublicKeyFromFile(this.publicKeyPath);
        assertThat(rsaPublicKey).isNotNull();
        assertThat(keystoreStr).isNotNull();
        assertThat(keyStr).isNotNull();
        assertThat(keystoreStr).isEqualTo(KeyPairProviderUtil.rsaPublicKey(rsaPublicKey));
    }

    @Test(expected = NullPointerException.class)
    public void testGetPublicKeyFromFileShouldCatchNPE()
    {
        final var rsaPublicKey = KeyPairProviderUtil.getPublicKeyFromFile(null);
        assertThat(rsaPublicKey).isNull();
    }

    @Test(expected = CustomAppException.class)
    public void testGetPublicKeyFromFileShouldCatchOther()
    {
        final var rsaPublicKey = KeyPairProviderUtil.getPublicKeyFromFile("");
        assertThat(rsaPublicKey).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#writeToFile(java.lang.String, java.lang.String)}.
     */
    @Test
    public void testWriteToFile()
    {
        final var cert = KeyPairProviderUtil.getCertFromKeystore(this.keystorePath, this.cryptoModel);

        final var certOutPath = this.outFilesPath + "keystore-cert-out.txt";
        KeyPairProviderUtil.writeToFile(cert, certOutPath);

        assertThat(cert).isNotNull();
    }

    @Test(expected = NullPointerException.class)
    public void testWriteToFileShouldCatchNPE()
    {
        final var cert = KeyPairProviderUtil.getCertFromKeystore(this.keystorePath, this.cryptoModel);
        KeyPairProviderUtil.writeToFile(cert, null);

        assertThat(cert).isNotNull();
    }

    @Test(expected = CustomAppException.class)
    public void testWriteToFileShouldCatchOtherException()
    {
        final var cert = KeyPairProviderUtil.getCertFromKeystore(this.keystorePath, this.cryptoModel);
        KeyPairProviderUtil.writeToFile(cert, "");

        assertThat(cert).isNotNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#encrypt(java.lang.String, fr.vincent.tuto.common.model.mo.CryptoModel)}.
     */
    @Test
    public void testEncrypt()
    {
        final var rsaPublicKey = KeyPairProviderUtil.getPublicKeyFromFile(this.publicKeyPath);
        final var publicKey = KeyPairProviderUtil.rsaPublicKey(rsaPublicKey);

        var cryptoModel = new CryptoModel()//
        .algorithm("RSA")//
        .instancePad(null)//
        .secureRandomInstance(null)//
        .bytesLength(null)//
        .keyValue(null)//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.FALSE)//
        .privateKey(null)//
        .publicKey(publicKey)//
        .keystorePassword("app-recette-test")//
        .keyPassword("app-recette-test")//
        .aliasKeystore("app-recette")//
        .signatureInstance("SHA384RSA");

        final var encryptData = KeyPairProviderUtil.encrypt(TEXT_CHIFFREMENT, cryptoModel);

        assertThat(encryptData).isNotNull();
        assertThat(encryptData.toString()).isNotEmpty();
    }

    @Test(expected = NullPointerException.class)
    public void testEncryptShouldCatchNPE()
    {
        final var encryptData = KeyPairProviderUtil.encrypt(null, null);

        assertThat(encryptData).isNull();
    }

    @Test(expected = CustomAppException.class)
    public void testEncryptShouldCatchOtherEXception()
    {
        final var rsaPublicKey = KeyPairProviderUtil.getPublicKeyFromFile(this.publicKeyPath);
        final var publicKey = KeyPairProviderUtil.rsaPublicKey(rsaPublicKey);

        var cryptoModel = new CryptoModel()//
        .algorithm("")//
        .instancePad(null)//
        .secureRandomInstance(null)//
        .bytesLength(null)//
        .keyValue(null)//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.FALSE)//
        .privateKey(null)//
        .publicKey(publicKey)//
        .keystorePassword("app-recette-test")//
        .keyPassword("app-recette-test")//
        .aliasKeystore("app-recette")//
        .signatureInstance("SHA384RSA");

        final var encryptData = KeyPairProviderUtil.encrypt(TEXT_CHIFFREMENT, cryptoModel);

        assertThat(encryptData).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#decrypt(java.lang.String, fr.vincent.tuto.common.model.mo.CryptoModel)}.
     */
    @Test
    public void testDecrypt()
    {
        final var rsaPublicKey = KeyPairProviderUtil.getPublicKeyFromFile(this.publicKeyPath);
        final var publicKey = KeyPairProviderUtil.rsaPublicKey(rsaPublicKey);

        final var rsaPrivateKey = KeyPairProviderUtil.getPrivateKeyFromFile(this.privateKeyPath);
        final var privateKey = KeyPairProviderUtil.rsaPrivateKey(rsaPrivateKey);

        var cryptoModel = new CryptoModel()//
        .algorithm("RSA")//
        .instancePad(null)//
        .secureRandomInstance(null)//
        .bytesLength(null)//
        .keyValue(null)//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.FALSE)//
        .privateKey(privateKey)//
        .publicKey(publicKey)//
        .keystorePassword("app-recette-test")//
        .keyPassword("app-recette-test")//
        .aliasKeystore("app-recette")//
        .signatureInstance("SHA384RSA");

        final var encryptData = KeyPairProviderUtil.encrypt(TEXT_CHIFFREMENT, cryptoModel);
        final var decryptData = KeyPairProviderUtil.decrypt(encryptData, cryptoModel);

        assertThat(decryptData).isNotEmpty();
        assertThat(decryptData.toString()).isEqualToIgnoringCase(TEXT_CHIFFREMENT);
    }

    @Test(expected = NullPointerException.class)
    public void testDecryptShouldCatchNPE()
    {
        final var decryptData = KeyPairProviderUtil.decrypt(null, null);

        assertThat(decryptData).isNull();
    }

    @Test(expected = CustomAppException.class)
    public void testDecryptShouldCatchOtherEXception()
    {
        final var rsaPublicKey = KeyPairProviderUtil.getPublicKeyFromFile(this.publicKeyPath);
        final var publicKey = KeyPairProviderUtil.rsaPublicKey(rsaPublicKey);

        final var rsaPrivateKey = KeyPairProviderUtil.getPrivateKeyFromFile(this.privateKeyPath);
        final var privateKey = KeyPairProviderUtil.rsaPrivateKey(rsaPrivateKey);

        var cryptoModel = new CryptoModel()//
        .algorithm("")//
        .instancePad(null)//
        .secureRandomInstance(null)//
        .bytesLength(null)//
        .keyValue(null)//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.FALSE)//
        .privateKey(privateKey)//
        .publicKey(publicKey)//
        .keystorePassword("app-recette-test")//
        .keyPassword("app-recette-test")//
        .aliasKeystore("app-recette")//
        .signatureInstance("SHA384RSA");

        final var encryptData = KeyPairProviderUtil.encrypt(TEXT_CHIFFREMENT, cryptoModel);
        final var decryptData = KeyPairProviderUtil.decrypt(encryptData, cryptoModel);

        assertThat(decryptData).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#sign(java.lang.String, fr.vincent.tuto.common.model.mo.CryptoModel)}.
     */
    @Test
    public void testSign()
    {
        final var rsaPrivateKey = KeyPairProviderUtil.getPrivateKeyFromFile(this.privateKeyPath);
        final var privateKey = KeyPairProviderUtil.rsaPrivateKey(rsaPrivateKey);

        var cryptoModel = new CryptoModel()//
        .algorithm("")//
        .instancePad(null)//
        .secureRandomInstance(null)//
        .bytesLength(null)//
        .keyValue(null)//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.FALSE)//
        .privateKey(privateKey)//
        .publicKey(null)//
        .keystorePassword("app-recette-test")//
        .keyPassword("app-recette-test")//
        .aliasKeystore("app-recette")//
        .signatureInstance("SHA256withRSA");

        final var signData = KeyPairProviderUtil.sign(SIGNATURE_TEXT, cryptoModel);

        assertThat(signData).isNotNull();
    }

    @Test(expected = NullPointerException.class)
    public void testSignShouldCatchNPE()
    {

        final var signData = KeyPairProviderUtil.sign(SIGNATURE_TEXT, null);

        assertThat(signData).isNull();
    }

    @Test(expected = CustomAppException.class)
    public void testSignShouldCatchOtherException()
    {
        final var rsaPrivateKey = KeyPairProviderUtil.getPrivateKeyFromFile(this.privateKeyPath);
        final var privateKey = KeyPairProviderUtil.rsaPrivateKey(rsaPrivateKey);

        var cryptoModel = new CryptoModel()//
        .algorithm("")//
        .instancePad(null)//
        .secureRandomInstance(null)//
        .bytesLength(null)//
        .keyValue(null)//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.FALSE)//
        .privateKey(privateKey)//
        .publicKey(null)//
        .keystorePassword("app-recette-test")//
        .keyPassword("app-recette-test")//
        .aliasKeystore("app-recette")//
        .signatureInstance("");

        final var signData = KeyPairProviderUtil.sign(SIGNATURE_TEXT, cryptoModel);

        assertThat(signData).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtil#verify(java.lang.String, java.lang.String, fr.vincent.tuto.common.model.mo.CryptoModel)}.
     */
    @Test
    public void testVerify()
    {
        final var rsaPublicKey = KeyPairProviderUtil.getPublicKeyFromFile(this.publicKeyPath);
        final var publicKey = KeyPairProviderUtil.rsaPublicKey(rsaPublicKey);

        final var rsaPrivateKey = KeyPairProviderUtil.getPrivateKeyFromFile(this.privateKeyPath);
        final var privateKey = KeyPairProviderUtil.rsaPrivateKey(rsaPrivateKey);

        var cryptoModel = new CryptoModel()//
        .algorithm("RSA")//
        .instancePad(null)//
        .secureRandomInstance(null)//
        .bytesLength(null)//
        .keyValue(null)//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.FALSE)//
        .privateKey(privateKey)//
        .publicKey(publicKey)//
        .keystorePassword("app-recette-test")//
        .keyPassword("app-recette-test")//
        .aliasKeystore("app-recette")//
        .signatureInstance("SHA256withRSA");

        final var signData = KeyPairProviderUtil.sign(SIGNATURE_TEXT, cryptoModel);
        final var verifData = KeyPairProviderUtil.verify(SIGNATURE_TEXT, signData, cryptoModel);

        assertThat(verifData).isNotNull();
        assertThat(verifData.booleanValue()).isTrue();
    }

    @Test(expected = NullPointerException.class)
    public void testVerifyShouldCatchNPE()
    {
        final var signData = KeyPairProviderUtil.verify(null, null, null);

        assertThat(signData).isNull();
    }

    @Test(expected = CustomAppException.class)
    public void testVerifyShouldCatchOtherException()
    {
        final var rsaPublicKey = KeyPairProviderUtil.getPublicKeyFromFile(this.publicKeyPath);
        final var publicKey = KeyPairProviderUtil.rsaPublicKey(rsaPublicKey);

        final var rsaPrivateKey = KeyPairProviderUtil.getPrivateKeyFromFile(this.privateKeyPath);
        final var privateKey = KeyPairProviderUtil.rsaPrivateKey(rsaPrivateKey);

        var cryptoModel = new CryptoModel()//
        .algorithm("")//
        .instancePad(null)//
        .secureRandomInstance(null)//
        .bytesLength(null)//
        .keyValue(null)//
        .encoding("UTF-8")//
        .aesSwitchFlag(Boolean.FALSE)//
        .privateKey(privateKey)//
        .publicKey(publicKey)//
        .keystorePassword("app-recette-test")//
        .keyPassword("app-recette-test")//
        .aliasKeystore("app-recette")//
        .signatureInstance("");

        final var signData = KeyPairProviderUtil.sign(SIGNATURE_TEXT, cryptoModel);
        final var verifData = KeyPairProviderUtil.verify(SIGNATURE_TEXT, "", cryptoModel);

        assertThat(signData).isNull();
        assertThat(verifData).isNull();
    }

    @Test
    public void testResourceIsNOtNull()
    {
        assertThat(this.inFilePath).isNotNull();
        assertThat(this.outFilesPath).isNotNull();
    }
}
