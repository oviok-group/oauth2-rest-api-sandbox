/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : DatesConverterUtilTest.java
 * Date de création : 27 nov. 2020
 * Heure de création : 05:44:06
 * Package : fr.vincent.tuto.common.utils.date
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.utils.date;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;

import fr.vincent.tuto.common.constants.AppConstants;

/**
 * Classe des tes unitaires des objets de type {@link DatesConverterUtil}
 * 
 * @author Vincent Otchoun
 */
public class DatesConverterUtilTest
{

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#convertToLocalDate(java.util.Date)}.
     */
    @Test
    public void testConvertToLocalDate()
    {
        var date = this.getDate();

        final var localDate = DatesConverterUtil.convertToLocalDate(date);

        assertThat(localDate).isNotNull();
        assertThat(localDate.get(ChronoField.YEAR)).isEqualTo(2020);
        assertThat(localDate.get(ChronoField.MONTH_OF_YEAR)).isEqualTo(11);
        assertThat(localDate.get(ChronoField.DAY_OF_MONTH)).isEqualTo(27);
    }

    @Test
    public void testConvertToLocalDateShoulReturnNull()
    {
        final var localDate = DatesConverterUtil.convertToLocalDate(null);
        assertThat(localDate).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#convertToLocalDateWithInstant(java.util.Date)}.
     */
    @Test
    public void testConvertToLocalDateWithInstant()
    {
        var date = this.getDate();

        final var localDate = DatesConverterUtil.convertToLocalDateWithInstant(date);

        assertThat(localDate).isNotNull();
        assertThat(localDate.get(ChronoField.YEAR)).isEqualTo(2020);
        assertThat(localDate.get(ChronoField.MONTH_OF_YEAR)).isEqualTo(11);
        assertThat(localDate.get(ChronoField.DAY_OF_MONTH)).isEqualTo(27);
    }

    @Test
    public void testConvertToLocalDateWithInstantShoulReturnNull()
    {
        final var localDate = DatesConverterUtil.convertToLocalDateWithInstant(null);
        assertThat(localDate).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#convertToLocalDateWithSqlDate(java.sql.Date)}.
     */
    @Test
    public void testConvertToLocalDateWithSqlDate()
    {
        var date = this.getDate();

        final var localDate = DatesConverterUtil.convertToLocalDateWithSqlDate(date);

        assertThat(localDate).isNotNull();
        assertThat(localDate.get(ChronoField.YEAR)).isEqualTo(2020);
        assertThat(localDate.get(ChronoField.MONTH_OF_YEAR)).isEqualTo(11);
        assertThat(localDate.get(ChronoField.DAY_OF_MONTH)).isEqualTo(27);
    }

    @Test
    public void testConvertToLocalDateWithSqlDateShouldReturnNull()
    {
        final var localDate = DatesConverterUtil.convertToLocalDateWithSqlDate(null);
        assertThat(localDate).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#convertToLocalDateWithMilisecond(java.util.Date)}.
     */
    @Test
    public void testConvertToLocalDateWithMilisecond()
    {
        var date = this.getDate();

        final var localDate = DatesConverterUtil.convertToLocalDateWithMilisecond(date);

        assertThat(localDate).isNotNull();
        assertThat(localDate.get(ChronoField.YEAR)).isEqualTo(2020);
        assertThat(localDate.get(ChronoField.MONTH_OF_YEAR)).isEqualTo(11);
        assertThat(localDate.get(ChronoField.DAY_OF_MONTH)).isEqualTo(27);
    }

    @Test
    public void testConvertToLocalDateWithMilisecondShouldReturnNull()
    {
        final var localDate = DatesConverterUtil.convertToLocalDateWithMilisecond(null);
        assertThat(localDate).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#convertToLocalDateTime(java.util.Date)}.
     */
    @Test
    public void testConvertToLocalDateTime()
    {
        var dateTime = this.getDateTime();

        final var localDateTime = DatesConverterUtil.convertToLocalDateTime(dateTime);

        assertThat(localDateTime).isNotNull();
        assertThat(localDateTime.get(ChronoField.YEAR)).isEqualTo(2020);
        assertThat(localDateTime.get(ChronoField.MONTH_OF_YEAR)).isEqualTo(11);
        assertThat(localDateTime.get(ChronoField.DAY_OF_MONTH)).isEqualTo(27);
        assertThat(localDateTime.get(ChronoField.HOUR_OF_DAY)).isEqualTo(06);
        assertThat(localDateTime.get(ChronoField.MINUTE_OF_HOUR)).isEqualTo(31);
    }

    @Test
    public void testConvertToLocalDateTimeShouldReturnNull()
    {
        final var localDateTime = DatesConverterUtil.convertToLocalDateTime(null);
        assertThat(localDateTime).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#convertToLocalDateTimeWithInstant(java.util.Date)}.
     */
    @Test
    public void testConvertToLocalDateTimeWithInstant()
    {
        var dateTime = this.getDateTime();

        final var localDateTime = DatesConverterUtil.convertToLocalDateTimeWithInstant(dateTime);

        assertThat(localDateTime).isNotNull();
        assertThat(localDateTime.get(ChronoField.YEAR)).isEqualTo(2020);
        assertThat(localDateTime.get(ChronoField.MONTH_OF_YEAR)).isEqualTo(11);
        assertThat(localDateTime.get(ChronoField.DAY_OF_MONTH)).isEqualTo(27);
        assertThat(localDateTime.get(ChronoField.HOUR_OF_DAY)).isEqualTo(06);
        assertThat(localDateTime.get(ChronoField.MINUTE_OF_HOUR)).isEqualTo(31);
    }

    @Test
    public void testConvertToLocalDateTimeWithInstantShouldReturnNull()
    {
        final var localDateTime = DatesConverterUtil.convertToLocalDateTimeWithInstant(null);
        assertThat(localDateTime).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#convertToLocalDateTimeWithSqlTimestamp(java.sql.Date)}.
     */
    @Test
    public void testConvertToLocalDateTimeWithSqlTimestamp()
    {
        var dateTime = this.getDateTime();

        final var localDateTime = DatesConverterUtil.convertToLocalDateTimeWithSqlTimestamp(dateTime);

        assertThat(localDateTime).isNotNull();
        assertThat(localDateTime.get(ChronoField.YEAR)).isEqualTo(2020);
        assertThat(localDateTime.get(ChronoField.MONTH_OF_YEAR)).isEqualTo(11);
        assertThat(localDateTime.get(ChronoField.DAY_OF_MONTH)).isEqualTo(27);
        assertThat(localDateTime.get(ChronoField.HOUR_OF_DAY)).isEqualTo(06);
        assertThat(localDateTime.get(ChronoField.MINUTE_OF_HOUR)).isEqualTo(31);
    }

    @Test
    public void testConvertToLocalDateTimeWithSqlTimestampShouldReturnNull()
    {
        final var localDateTime = DatesConverterUtil.convertToLocalDateTimeWithSqlTimestamp(null);
        assertThat(localDateTime).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#convertToLocalDateTimeWithMilisecond(java.util.Date)}.
     */
    @Test
    public void testConvertToLocalDateTimeWithMilisecond()
    {
        var dateTime = this.getDateTime();

        final var localDateTime = DatesConverterUtil.convertToLocalDateTimeWithMilisecond(dateTime);

        assertThat(localDateTime).isNotNull();
        assertThat(localDateTime.get(ChronoField.YEAR)).isEqualTo(2020);
        assertThat(localDateTime.get(ChronoField.MONTH_OF_YEAR)).isEqualTo(11);
        assertThat(localDateTime.get(ChronoField.DAY_OF_MONTH)).isEqualTo(27);
        assertThat(localDateTime.get(ChronoField.HOUR_OF_DAY)).isEqualTo(06);
        assertThat(localDateTime.get(ChronoField.MINUTE_OF_HOUR)).isEqualTo(31);
    }

    @Test
    public void testConvertToLocalDateTimeWithMilisecondShouldReturnNull()
    {
        final var localDateTime = DatesConverterUtil.convertToLocalDateTimeWithMilisecond(null);
        assertThat(localDateTime).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#getCurrentDate()}.
     */
    @Test
    public void testGetCurrentDate()
    {
        final var currentDate = DatesConverterUtil.getCurrentDate();

        var localDate = LocalDate.now(ZoneId.systemDefault());
        var calendar = DateUtils.toCalendar(currentDate);

        // System.err.println(">>>>>>> DayOfWeek : " + localDate.getDayOfWeek());
        // System.err.println(">>>>>>> DayOfMonth : " + localDate.getDayOfMonth());
        // System.err.println(">>>>>>> DayOfYear : " + localDate.getDayOfYear());
        //
        // System.err.println(">>>>>>> Calendar DayOfMonth : " + calendar.get(Calendar.DAY_OF_MONTH));

        assertThat(currentDate).isNotNull();
        assertThat(localDate).isNotNull();
        assertThat(calendar).isNotNull();

        assertThat(calendar.get(Calendar.YEAR)).isEqualTo(localDate.getYear());
        assertThat(calendar.get(Calendar.MONTH) + 1).isEqualTo(localDate.getMonthValue());
        assertThat(calendar.get(Calendar.DAY_OF_MONTH)).isEqualTo(localDate.getDayOfMonth());
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#getCurrentDateTime()}.
     */
    @Test
    public void testGetCurrentDateWithLocalDateTime()
    {
        final var currentDate = DatesConverterUtil.getCurrentDateTime();

        var localDate = LocalDate.now(ZoneId.systemDefault());
        var calendar = DateUtils.toCalendar(currentDate);

        assertThat(currentDate).isNotNull();
        assertThat(localDate).isNotNull();
        assertThat(calendar).isNotNull();

        assertThat(calendar.get(Calendar.YEAR)).isEqualTo(localDate.getYear());
        assertThat(calendar.get(Calendar.MONTH) + 1).isEqualTo(localDate.getMonthValue());
        assertThat(calendar.get(Calendar.DAY_OF_MONTH)).isEqualTo(localDate.getDayOfMonth());
    }

    @Test
    public void testGetCurrentDateWithLocalDateTimeWith_CET_TimeZoneText()
    {
        final var currentDate = DatesConverterUtil.getCurrentDateTime();
        final var dateStr = DatesConverterUtil.formateDateToString(AppConstants.CET_DATE_FORMAT, AppConstants.CET_TIME_ZONE, currentDate);

        // System.err.println(">>> La date formatée avec CET TimeZone en texte : \n" + dateStr);

        assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getYear());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getMonthValue());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getDayOfMonth());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getHour());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getMinute());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getSecond());
    }

    @Test
    public void testGetCurrentDateWithLocalDateTimeWithout_CET_TimeZoneText()
    {
        final var currentDate = DatesConverterUtil.getCurrentDateTime();
        final var dateStr = DatesConverterUtil.formateDateToString(AppConstants.CET_DATE_FORMAT_MAJ_Z, AppConstants.CET_TIME_ZONE, currentDate);

        // System.err.println(">>> La date formatée sans CET TimeZone en texte : \n" + dateStr);

        assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getYear());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getMonthValue());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getDayOfMonth());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getHour());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getMinute());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getSecond());
    }

    @Test
    public void testGetCurrentDateWithLocalDateTimeWith_UTC_TimeZoneText()
    {
        final var currentDate = DatesConverterUtil.getCurrentDateTime();
        final var dateStr = DatesConverterUtil.formateDateToString(AppConstants.UTC_DATE_FORMAT_MAJ_Z, AppConstants.UTC_TIME_ZONE, currentDate);

        // System.err.println(">>> La date formatée AVEC UTC TimeZone en texte : \n" + dateStr);

        assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getYear());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getMonthValue());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + (LocalDateTime.now().getDayOfMonth() - 1));
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getHour());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getMinute());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getSecond());
    }

    @Test
    public void testGetCurrentDateWithLocalDateTimeWithout_UTC_TimeZoneText()
    {
        final var currentDate = DatesConverterUtil.getCurrentDateTime();
        final var dateStr = DatesConverterUtil.formateDateToString(AppConstants.UTC_DATE_FORMAT_MAJ_Z, AppConstants.UTC_TIME_ZONE, currentDate);

        // System.err.println(">>> La date formatée sans UTC TimeZone en texte : \n" + dateStr);

        assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getYear());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getMonthValue());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + (LocalDateTime.now().getDayOfMonth() - 1));
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getHour());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getMinute());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getSecond());
    }

    @Test
    public void testGetCurrentDateWithLocalDateTimeWith_GMT_TimeZoneText()
    {
        final var currentDate = DatesConverterUtil.getCurrentDateTime();
        final var dateStr = DatesConverterUtil.formateDateToString(AppConstants.GMT_DATE_FORMAT_MAJ_Z, AppConstants.GMT_TIME_ZONE, currentDate);

        // System.err.println(">>> La date formatée AVEC GMT TimeZone en texte : \n" + dateStr);

        assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getYear());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getMonthValue());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + (LocalDateTime.now().getDayOfMonth() - 1));
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getHour());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getMinute());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getSecond());
    }

    @Test
    public void testGetCurrentDateWithLocalDateTimeWithout_GMT_TimeZoneText()
    {
        final var currentDate = DatesConverterUtil.getCurrentDateTime();
        final var dateStr = DatesConverterUtil.formateDateToString(AppConstants.GMT_DATE_FORMAT, AppConstants.GMT_TIME_ZONE, currentDate);

        // System.err.println(">>> La date formatée sans GMT TimeZone en texte : \n" + dateStr);

        assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getYear());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getMonthValue());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + (LocalDateTime.now().getDayOfMonth() - 1));
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getHour());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getMinute());
        // assertThat(dateStr).contains(AppConstants.EMPTY_STR + LocalDateTime.now().getSecond());
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#convertToDateWithSqlDate(java.time.LocalDate)}.
     */
    @Test
    public void testConvertToDateWithSqlDate()
    {
        var localDate = this.getLodalDate();
        final var date = DatesConverterUtil.convertToDateWithSqlDate(localDate);
        var calendar = DateUtils.toCalendar(date);

        assertThat(calendar).isNotNull();
        assertThat(calendar.get(Calendar.YEAR)).isEqualTo(2020);
        assertThat(calendar.get(Calendar.MONTH)).isEqualTo(10);
        assertThat(calendar.get(Calendar.DAY_OF_MONTH)).isEqualTo(27);
    }

    @Test
    public void testConvertToDateWithSqlDateShouldReturnNull()
    {
        final var date = DatesConverterUtil.convertToDateWithSqlDate(null);
        assertThat(date).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#convertToDateWithInstant(java.time.LocalDate)}.
     */
    @Test
    public void testConvertToDateWithInstantLocalDate()
    {
        var localDate = this.getLodalDate();
        final var date = DatesConverterUtil.convertToDateWithInstant(localDate);
        var calendar = DateUtils.toCalendar(date);

        assertThat(calendar).isNotNull();
        assertThat(calendar.get(Calendar.YEAR)).isEqualTo(2020);
        assertThat(calendar.get(Calendar.MONTH)).isEqualTo(10);
        assertThat(calendar.get(Calendar.DAY_OF_MONTH)).isEqualTo(27);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#convertToDateWithInstant(java.time.LocalDateTime)}.
     */
    @Test
    public void testConvertToDateWithInstantLocalDateTime()
    {
        var localDateTime = this.getLocalDateTime();
        final var date = DatesConverterUtil.convertToDateWithInstant(localDateTime);
        var calendar = DateUtils.toCalendar(date);

        assertThat(calendar).isNotNull();
        assertThat(calendar.get(Calendar.YEAR)).isEqualTo(2020);
        assertThat(calendar.get(Calendar.MONTH)).isEqualTo(10);
        assertThat(calendar.get(Calendar.DAY_OF_MONTH)).isEqualTo(27);
        assertThat(calendar.get(Calendar.HOUR_OF_DAY)).isEqualTo(07);
        assertThat(calendar.get(Calendar.MINUTE)).isEqualTo(05);
        assertThat(calendar.get(Calendar.SECOND)).isZero();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#convertToDateTimeWithSqlTimestamp(java.time.LocalDateTime)}.
     */
    @Test
    public void testConvertToDateTimeWithSqlTimestamp()
    {
        var localDateTime = this.getLocalDateTime();
        final var date = DatesConverterUtil.convertToDateTimeWithSqlTimestamp(localDateTime);
        var calendar = DateUtils.toCalendar(date);

        assertThat(calendar).isNotNull();
        assertThat(calendar.get(Calendar.YEAR)).isEqualTo(2020);
        assertThat(calendar.get(Calendar.MONTH)).isEqualTo(10);
        assertThat(calendar.get(Calendar.DAY_OF_MONTH)).isEqualTo(27);
        assertThat(calendar.get(Calendar.HOUR_OF_DAY)).isEqualTo(07);
        assertThat(calendar.get(Calendar.MINUTE)).isEqualTo(05);
        assertThat(calendar.get(Calendar.SECOND)).isZero();
    }

    @Test
    public void testConvertToDateTimeWithSqlTimestampShouldReturnNull()
    {
        final var date = DatesConverterUtil.convertToDateTimeWithSqlTimestamp(null);

        assertThat(date).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#getZonedDateTime(long)}.
     */
    @Test
    public void testGetZonedDateTime()
    {
        var date = this.getDate();
        long millisecond = date.getTime();
        var zonedDateTime = DatesConverterUtil.getZonedDateTime(millisecond);

        assertThat(zonedDateTime).isNotNull();
        assertThat(zonedDateTime.toInstant().toEpochMilli()).isEqualTo(millisecond);
        assertThat(zonedDateTime.toEpochSecond()).isNotEqualTo(millisecond);
    }

    @Test
    public void testGetZonedDateTimeWtihZero()
    {
        var zonedDateTime = DatesConverterUtil.getZonedDateTime(0);

        assertThat(zonedDateTime).isNotNull();
        assertThat(zonedDateTime.toInstant().getEpochSecond()).isZero();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#getInstant(long)}.
     */
    @Test
    public void testGetInstant()
    {
        Date date = this.getDate();
        long millisecond = date.getTime();
        var instant = DatesConverterUtil.getInstant(millisecond);

        assertThat(instant).isNotNull();
        assertThat(instant.toEpochMilli()).isEqualTo(millisecond);
    }

    @Test
    public void testGetInstantWithZero()
    {
        var instant = DatesConverterUtil.getInstant(0);

        assertThat(instant).isNotNull();
        assertThat(instant.toEpochMilli()).isZero();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#getTimestamp()}.
     */
    @Test
    public void testGetTimestamp()
    {
        var timestamp = DatesConverterUtil.getTimestamp();

        assertThat(timestamp).isNotNull();
        assertThat(timestamp.getTime()).isPositive();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#getDateFormat(String,String))}.
     */
    @Test
    public void testGetDateFormatGMT()
    {
        var dateFormat = DatesConverterUtil.getDateFormat(AppConstants.GMT_DATE_FORMAT, AppConstants.GMT_TIME_ZONE);
        var gmtDate = dateFormat.format(DatesConverterUtil.convertToDateWithInstant(LocalDateTime.now()));

        assertThat(dateFormat).isNotNull();
        assertThat(gmtDate).isNotEmpty();
    }

    @Test
    public void testGetDateFormatUTC()
    {
        var dateFormat = DatesConverterUtil.getDateFormat(AppConstants.UTC_DATE_FORMAT, AppConstants.UTC_TIME_ZONE);
        var utcDate = dateFormat.format(DatesConverterUtil.convertToDateWithInstant(LocalDateTime.now()));

        assertThat(dateFormat).isNotNull();
        assertThat(utcDate).isNotEmpty();
    }

    @Test
    public void testGetDateFormatShoulReturnNull()
    {
        var dateFormat = DatesConverterUtil.getDateFormat(null, null);
        assertThat(dateFormat).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#formateDateToString(String, String,
     * Object))}.
     */
    @Test
    public void testFormateDateToString()
    {
        final var gmtStr = DatesConverterUtil.formateDateToString(AppConstants.GMT_DATE_FORMAT, AppConstants.GMT_TIME_ZONE, DatesConverterUtil
        .getTimestamp());

        assertThat(gmtStr).isNotEmpty();
    }

    @Test
    public void testFormateDateToStringWithUTC()
    {
        final var utcStr = DatesConverterUtil.formateDateToString(AppConstants.UTC_DATE_FORMAT, AppConstants.UTC_TIME_ZONE, DatesConverterUtil
        .getTimestamp());

        assertThat(utcStr).isNotEmpty();
    }

    @Test
    public void testFormateDateToStringShoulReturnNull()
    {
        final var dateFormatyed = DatesConverterUtil.formateDateToString(null, null, null);

        assertThat(dateFormatyed).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#getFromDateAsString(String,
     * String))}.
     */
    @Test
    public void testGetFromDateAsString_CET()
    {
        // CET
        final var cetStr = DatesConverterUtil.formateDateToString(AppConstants.CET_DATE_FORMAT_MAJ_Z, AppConstants.CET_TIME_ZONE,
        DatesConverterUtil.getCurrentDateTime());
        assertThat(cetStr).isNotNull();

        var dateTime = DatesConverterUtil.getFromDateAsString(cetStr, AppConstants.CET_DATE_FORMAT_MAJ_Z, AppConstants.CET_TIME_ZONE);
        assertThat(dateTime).isNotNull();

        final var cetStr2 = DatesConverterUtil.formateDateToString(AppConstants.CET_DATE_FORMAT_MAJ_Z, AppConstants.CET_TIME_ZONE, dateTime);
        assertThat(cetStr2).isEqualTo(cetStr);
    }

    @Test
    public void testGetFromDateAsString_UTC()
    {
        // UTC
        final var utcStr = DatesConverterUtil.formateDateToString(AppConstants.UTC_DATE_FORMAT_MAJ_Z, AppConstants.UTC_TIME_ZONE,
        DatesConverterUtil.getCurrentDateTime("UTC"));
        assertThat(utcStr).isNotNull();

        var dateTime = DatesConverterUtil.getFromDateAsString(utcStr, AppConstants.UTC_DATE_FORMAT_MAJ_Z, AppConstants.UTC_TIME_ZONE);
        assertThat(dateTime).isNotNull();

        final var utcStr2 = DatesConverterUtil.formateDateToString(AppConstants.UTC_DATE_FORMAT_MAJ_Z, AppConstants.UTC_TIME_ZONE, dateTime);
        assertThat(utcStr2).isNotNull();
        assertThat(utcStr).isEqualTo(utcStr2);
    }

    @Test
    public void testGetFromDateAsString_NulStrDate()
    {
        var dateTime = DatesConverterUtil.getFromDateAsString(null, AppConstants.CET_DATE_FORMAT_MAJ_Z, AppConstants.CET_TIME_ZONE);
        assertThat(dateTime).isNull();
    }

    // @Test(expected = NullPointerException.class)
    @Test
    public void testGetFromDateAsString_ShouldThrowException()
    {
        // CET
        final var cetStr = DatesConverterUtil.formateDateToString(AppConstants.CET_DATE_FORMAT_MAJ_Z, AppConstants.CET_TIME_ZONE,
        DatesConverterUtil.getCurrentDateTime());
        var dateTime = DatesConverterUtil.getFromDateAsString(cetStr, null, null);
        assertThat(dateTime).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#getXMLGregorianCalendar(java.time.LocalDate)}.
     * 
     * @throws DatatypeConfigurationException
     */
    @Test
    public void testGetXMLGregorianCalendar() throws DatatypeConfigurationException
    {
        var localDate = this.getLodalDate();
        var xmlGregorianCalendar = DatesConverterUtil.getXMLGregorianCalendar(localDate);

        assertThat(xmlGregorianCalendar).isNotNull();
        assertThat(xmlGregorianCalendar.getYear()).isEqualTo(localDate.getYear());
        assertThat(xmlGregorianCalendar.getMonth()).isEqualTo(localDate.getMonthValue());
        assertThat(xmlGregorianCalendar.getDay()).isEqualTo(localDate.getDayOfMonth());
        assertThat(xmlGregorianCalendar.getTimezone()).isEqualTo(DatatypeConstants.FIELD_UNDEFINED);
    }

    @Test
    public void testGetXMLGregorianCalendarShouldReturnNull() throws DatatypeConfigurationException
    {
        var xmlGregorianCalendar = DatesConverterUtil.getXMLGregorianCalendar(null);

        assertThat(xmlGregorianCalendar).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.date.DatesConverterUtil#getLocalDate(javax.xml.datatype.XMLGregorianCalendar)}.
     * 
     * @throws DatatypeConfigurationException
     */
    @Test
    public void testGetLocalDate() throws DatatypeConfigurationException
    {
        var xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-11-27");
        var localDate = DatesConverterUtil.getLocalDate(xmlGregorianCalendar);

        assertThat(localDate).isNotNull();
        assertThat(localDate.getYear()).isEqualTo(xmlGregorianCalendar.getYear());
        assertThat(localDate.getMonthValue()).isEqualTo(xmlGregorianCalendar.getMonth());
        assertThat(localDate.getDayOfMonth()).isEqualTo(xmlGregorianCalendar.getDay());
    }

    @Test
    public void testGetLocalDateSholudReturnNull()
    {
        var localDate = DatesConverterUtil.getLocalDate(null);

        assertThat(localDate).isNull();
    }

    /**
     * @return
     */
    private Date getDate()
    {
        var calendar = Calendar.getInstance();
        calendar.set(2020, 10, 27); // pour 27/11/2020
        var date = calendar.getTime();
        return date;
    }

    /**
     * @return
     */
    private Date getDateTime()
    {
        var calendar = Calendar.getInstance();
        calendar.set(2020, 10, 27, 6, 31);
        var dateTime = calendar.getTime();
        return dateTime;
    }

    /**
     * @return
     */
    private LocalDate getLodalDate()
    {
        var localDate = LocalDate.of(2020, 11, 27); // pour 27/11/2020
        return localDate;
    }

    /**
     * @return
     */
    private LocalDateTime getLocalDateTime()
    {
        var localDateTime = LocalDateTime.of(2020, 11, 27, 7, 05);
        return localDateTime;
    }

    /**
     * @param pZoneFormat
     * @param pTimeZone
     * @return
     */
    @SuppressWarnings("unused")
    private DateFormat getDateFormat(final String pZoneFormat, final String pTimeZone)
    {
        var df = DateFormat.getDateTimeInstance();
        // df = new SimpleDateFormat(AppConstants.UTC_DATE_FORMAT);
        df = new SimpleDateFormat(pZoneFormat);
        df.setTimeZone(TimeZone.getTimeZone(pTimeZone));
        return df;
    }
}
