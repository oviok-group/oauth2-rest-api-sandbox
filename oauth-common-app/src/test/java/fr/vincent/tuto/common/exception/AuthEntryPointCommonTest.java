/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : AuthEntryPointCommonTest.java
 * Date de création : 5 déc. 2020
 * Heure de création : 13:41:52
 * Package : fr.vincent.tuto.common.exception
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.exception;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.common.config.CommonAppBaseConfig;
import fr.vincent.tuto.common.mapper.GenericJSONMapper;
import fr.vincent.tuto.common.model.error.ApiResponseError;
import fr.vincent.tuto.common.service.props.ApplicationPropsService;
import fr.vincent.tuto.common.service.props.DatabasePropsService;

/**
 * Classe d'implementation des Tests Unitaires des objets de type {@link AuthEntryPointCommon}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:common-app-db-test.properties", "classpath:common-app-application-test.properties", "classpath:common-app-messages-test.properties" })
@ContextConfiguration(name = "authEntryPointCommonTest", classes = { CommonAppBaseConfig.class, DatabasePropsService.class, ApplicationPropsService.class, JavaMailSenderImpl.class,
        StringHttpMessageConverter.class, AuthEntryPointCommon.class, GenericJSONMapper.class })
@SpringBootTest
@ActiveProfiles("test")
public class AuthEntryPointCommonTest
{
    private static final String REAL_BASE64_TEST_TOKEN = "87ZxNTF3hqIxFpKB0n06EeH5femEJ/vf7f/C2Ie7BOkSTV/GynzjR4WdOGKM5DZRi+ZMiSAy0XgDFlOS/nyTHw==";
    private static final String AUTH_ERROR_MSG = "Erreur acces non authorise, echec de l'authentification";

    @Autowired
    private AuthEntryPointCommon pointCommon;

    @Autowired
    private GenericJSONMapper genericJSONMapper;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.pointCommon = null;
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.exception.AuthEntryPointCommon#commence(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.AuthenticationException)}.
     * 
     * @throws ServletException
     * @throws IOException
     */
    @Test
    public void testCommence() throws IOException, ServletException
    {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", "Bearer " + REAL_BASE64_TEST_TOKEN);
        request.setRequestURI("/api/test");

        MockHttpServletResponse response = new MockHttpServletResponse();
        response.setWriterAccessAllowed(true);

        AuthenticationException exception = new AuthenticationCredentialsNotFoundException(AUTH_ERROR_MSG);

        this.pointCommon.commence(request, response, exception);

        // System.err.println(">>>> Le contenu de la réponse : \n" + response.getContentAsString());

        assertThat(response.getStatus()).isNotEqualTo(HttpStatus.OK.value());
        assertThat(response.getStatus()).isEqualTo(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Test(expected = CustomAppException.class)
    public void testCommence_GenResponseContent() throws IOException, ServletException
    {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", "Bearer " + REAL_BASE64_TEST_TOKEN);
        request.setRequestURI("/api/test");

        MockHttpServletResponse response = new MockHttpServletResponse();
        response.setWriterAccessAllowed(true);

        AuthenticationException exception = new AuthenticationCredentialsNotFoundException(AUTH_ERROR_MSG);

        this.pointCommon.commence(request, response, exception);

        assertThat(response).isNotNull();
        final String responseContent = response.getContentAsString();

        // Obtenir l'objet JSON contenu dans la réponse
        final ApiResponseError apiResponseError = this.genericJSONMapper.toJSONObject(responseContent, ApiResponseError.class);

        assertThat(apiResponseError).isNull();
    }

    @Test(expected = NullPointerException.class)
    public void testCommence_ShouldThrowException() throws IOException, ServletException
    {
        this.pointCommon.commence(null, null, null);
    }
}
