/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : AuthProviderUtilTest.java
 * Date de création : 24 nov. 2020
 * Heure de création : 22:44:56
 * Package : fr.vincent.tuto.common.utils.auth
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.utils.auth;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.Key;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.google.common.collect.Maps;

import fr.vincent.tuto.common.CommonAppTestsUtil;
import fr.vincent.tuto.common.constants.AppConstants;
import fr.vincent.tuto.common.enumeration.BaseRolesEnum;
import fr.vincent.tuto.common.model.mo.TokenModel;
import fr.vincent.tuto.common.utils.date.DatesConverterUtil;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import io.jsonwebtoken.security.WeakKeyException;

/**
 * Classe des Tests Unitaires des objets de type {@link AuthProviderUtil}
 * 
 * @author Vincent Otchoun
 */
public class AuthProviderUtilTest
{
    //
    private static final String COMMA_DELIMITER = ",";
    private static final String REAL_BASE64_TEST_TOKEN = "87ZxNTF3hqIxFpKB0n06EeH5femEJ/vf7f/C2Ie7BOkSTV/GynzjR4WdOGKM5DZRi+ZMiSAy0XgDFlOS/nyTHw==";
    private static final Long ONE_MINUTE = 60000L;
    private static final Long REMEMBER_ME = 2592000L;
    private static final Long TOKEN_VALIDITY = 86400L;
    private static final Long REFRESH_TOKEN = 86400L;
    private static final String ANONYMOUS_ROLE = "anonymous1";

    // MESSAGES
    private static final String JWT_MSG = "JWT String argument cannot be null or empty.";
    private static final String KEY_MSG = "signing key cannot be null.";

    private Key testKey;
    private byte[] keyBytes;

    private static String createTestToken(final String pBase64SecretKey, final String pUser)
    {
        final Key otherKey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(pBase64SecretKey));
        final Map<String, Object> header = Maps.newHashMap();
        header.put(Header.TYPE, Header.JWT_TYPE);

        return Jwts.builder()//
        .setId(UUID.randomUUID().toString())//
        .setIssuedAt(new Date())//
        .setSubject(pUser)//
        .setHeader(header)
        // .signWith(otherKey)//
        .signWith(otherKey, SignatureAlgorithm.HS512)//
        .setExpiration(new Date(new Date().getTime() + ONE_MINUTE))//
        .compact();
    }

    private String strToken = createTestToken(REAL_BASE64_TEST_TOKEN, ANONYMOUS_ROLE);
    private String strTokenWithEmptyUser = createTestToken(REAL_BASE64_TEST_TOKEN, StringUtils.EMPTY);
    private String strTokenWithNullyUser = createTestToken(REAL_BASE64_TEST_TOKEN, null);

    @Before
    public void setUp() throws Exception
    {
        this.keyBytes = Decoders.BASE64.decode(REAL_BASE64_TEST_TOKEN);
        this.testKey = Keys.hmacShaKeyFor(this.keyBytes);

    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#getSecureUserLogin()}.
     */
    @Test
    public void testGetSecureUserLogin()
    {
        final var context = SecurityContextHolder.createEmptyContext();
        final var authentication = AuthProviderUtil.createAuthentication(CommonAppTestsUtil.ADMIN, CommonAppTestsUtil.ADMIN);
        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final var username = AuthProviderUtil.getSecureUserLogin();

        assertThat(username).isNotNull();
        assertThat(username.get()).isNotNull();
        assertThat(username.get()).isEqualToIgnoringCase(CommonAppTestsUtil.ADMIN);
    }

    @Test
    public void testGetSecureUserLoginWithUserDetails()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        final Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.ANONYMOUS.getAuthority()));
        final var user = AuthProviderUtil.createSpringUser(CommonAppTestsUtil.ANONYMOUS, CommonAppTestsUtil.ANONYMOUS, authorities);
        final var authenticationToken = AuthProviderUtil.createAuthentication(user, authorities);

        context.setAuthentication(authenticationToken);
        SecurityContextHolder.setContext(context);

        final var username = AuthProviderUtil.getSecureUserLogin();

        assertThat(username).isNotNull();
        assertThat(username.get()).isNotNull();
        assertThat(username.get()).isEqualToIgnoringCase(CommonAppTestsUtil.ANONYMOUS);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#getConcatAllUserRoles(org.springframework.security.core.Authentication, String)}.
     */
    @Test
    public void testGetConcatAllUserRoles()
    {
        final Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.ADMIN.getAuthority()));
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.USER.getAuthority()));
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.CLIENT.getAuthority()));
        final var user = AuthProviderUtil.createSpringUser(CommonAppTestsUtil.ADMIN, CommonAppTestsUtil.ADMIN, authorities);
        final var authenticationToken = AuthProviderUtil.createAuthentication(user, authorities);

        final String rolesStr = AuthProviderUtil.getConcatAllUserRoles(authenticationToken, COMMA_DELIMITER);

        assertThat(rolesStr).isNotNull();
        assertThat(rolesStr.length()).isPositive();

        final String[] tabRoles = StringUtils.split(rolesStr, COMMA_DELIMITER);

        assertThat(tabRoles).isNotEmpty();
        assertThat(tabRoles[0]).isEqualTo(BaseRolesEnum.ADMIN.getAuthority());
        assertThat(tabRoles[1]).isEqualTo(BaseRolesEnum.USER.getAuthority());
        assertThat(tabRoles[2]).isEqualTo(BaseRolesEnum.CLIENT.getAuthority());
    }

    @Test
    public void testGetConcatAllUserRoles_With_Null_Delimiter()
    {
        final Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.ADMIN.getAuthority()));
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.USER.getAuthority()));
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.CLIENT.getAuthority()));
        final var user = AuthProviderUtil.createSpringUser(CommonAppTestsUtil.ADMIN, CommonAppTestsUtil.ADMIN, authorities);
        final var authenticationToken = AuthProviderUtil.createAuthentication(user, authorities);

        final var exception = assertThrows(NullPointerException.class, () -> {
            AuthProviderUtil.getConcatAllUserRoles(authenticationToken, null);
        });

        final var expectedMessage = "The delimiter must not be null";
        final var actualMessage = exception.getMessage();

        assertThat(actualMessage.length()).isPositive();
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    public void testGetConcatAllUserRoles_With_Null_Auth()
    {
        final var exception = assertThrows(NullPointerException.class, () -> {
            AuthProviderUtil.getConcatAllUserRoles(null, COMMA_DELIMITER);
        });

        final var actualMessage = exception.getMessage();
        assertThat(actualMessage).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#getTokenClaims(String, java.security.Key)}.
     */
    @Test
    public void testGetTokenClaims()
    {
        final var claims = AuthProviderUtil.getTokenClaims(this.strToken, this.testKey);

        assertThat(claims).isNotNull();
        assertThat(claims.getSubject()).isEqualTo(ANONYMOUS_ROLE);
        assertThat(claims.getId()).isNotEmpty();
        assertThat(claims.getId()).isExactlyInstanceOf(String.class);
        assertThat(claims.getNotBefore()).isNull();
        assertThat(claims.getExpiration()).isAfter(new Date());
        assertThat(claims.getIssuedAt()).isBefore(new Date());
    }

    @Test
    public void testGetTokenClaims_Null_Token()
    {
        final var exception = assertThrows(IllegalArgumentException.class, () -> {
            AuthProviderUtil.getTokenClaims(null, this.testKey);
        });

        final var expectedMessage = JWT_MSG;
        final var actualMessage = exception.getMessage();

        assertThat(actualMessage.length()).isPositive();
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    public void testGetTokenClaims_Null_Key()
    {
        final var exception = assertThrows(IllegalArgumentException.class, () -> {
            AuthProviderUtil.getTokenClaims(this.strToken, null);
        });

        final var expectedMessage = KEY_MSG;
        final var actualMessage = exception.getMessage();

        assertThat(actualMessage.length()).isPositive();
        assertThat(actualMessage).contains(expectedMessage);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#getUsername(String, java.security.Key)}.
     */
    @Test
    public void testGetUsername()
    {
        final var username = AuthProviderUtil.getUsername(this.strToken, this.testKey);

        assertThat(username).isEqualTo(ANONYMOUS_ROLE);
    }

    @Test
    public void testGetUsername_WithEmptyUser()
    {
        final var username = AuthProviderUtil.getUsername(this.strTokenWithEmptyUser, this.testKey);

        assertThat(username).isEmpty();
    }

    @Test
    public void testGetUsername_WithNullUser()
    {
        final String username = AuthProviderUtil.getUsername(this.strTokenWithNullyUser, this.testKey);

        assertThat(username).isNull();
    }

    @Test
    public void testGetUsername_With_Null_Token()
    {
        final var exception = assertThrows(IllegalArgumentException.class, () -> {
            AuthProviderUtil.getUsername(null, this.testKey);
        });

        final var expectedMessage = JWT_MSG;
        final var actualMessage = exception.getMessage();

        assertThat(actualMessage.length()).isPositive();
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    public void testGetUsername_With_Null_Key()
    {
        final var exception = assertThrows(IllegalArgumentException.class, () -> {
            AuthProviderUtil.getUsername(this.strToken, null);
        });

        final var expectedMessage = KEY_MSG;
        final var actualMessage = exception.getMessage();

        assertThat(actualMessage.length()).isPositive();
        assertThat(actualMessage).contains(expectedMessage);
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#getTokenSignature(String, java.security.Key)}.
     */
    @Test
    public void testGetTokenSignature()
    {
        final var signature = AuthProviderUtil.getTokenSignature(this.strToken, this.testKey);

        //
        assertThat(signature).isNotNull();
        assertThat(StringUtils.isAlphanumeric(signature)).isFalse();
        assertThat(StringUtils.isMixedCase(signature)).isTrue();
    }

    @Test
    public void testGetTokenSignature_With_Full_Null()
    {
        final var exception = assertThrows(IllegalArgumentException.class, () -> {
            AuthProviderUtil.getTokenSignature(null, null);
        });

        final var expectedMessage = KEY_MSG;
        final var actualMessage = exception.getMessage();

        assertThat(actualMessage.length()).isPositive();
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    public void testGetTokenSignature_WithEmptyKey()
    {
        final var exception = assertThrows(WeakKeyException.class, () -> {
            createTestToken(StringUtils.EMPTY, ANONYMOUS_ROLE);
        });

        final var expectedMessage = "he specified key byte array is 0 bits";
        final var actualMessage = exception.getMessage();

        assertThat(actualMessage.length()).isPositive();
        assertThat(actualMessage).contains(expectedMessage);
    }

    @Test
    public void testGetTokenSignature_WithEmpyUser()
    {
        final var signature = AuthProviderUtil.getTokenSignature(this.strTokenWithEmptyUser, this.testKey);

        //
        assertThat(signature).isNotNull();
    }

    @Test
    public void testGetTokenSignature_TokenWithEmpty()
    {
        final String tokenWithEmpt = createTestToken(REAL_BASE64_TEST_TOKEN + StringUtils.EMPTY, ANONYMOUS_ROLE);
        final var signature = AuthProviderUtil.getTokenSignature(tokenWithEmpt, this.testKey);

        //
        assertThat(signature).isNotNull();
        assertThat(StringUtils.isAlphanumeric(signature)).isFalse();
        assertThat(StringUtils.isMixedCase(signature)).isTrue();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#getTokenHeader(String, java.security.Key)}.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void testGetTokenHeader()
    {
        final JwsHeader header = AuthProviderUtil.getTokenHeader(this.strToken, this.testKey);

        assertThat(header).isNotNull();
        assertThat(header.getType()).isEqualTo(Header.JWT_TYPE);
        assertThat(header.getAlgorithm()).isEqualTo(SignatureAlgorithm.HS512.name());
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#getTokenValidity(fr.vincent.tuto.common.model.mo.TokenModel))}.
     */
    @Test
    public void tesGetTokenValidity()
    {
        final TokenModel tokenModel = new TokenModel()//
        .rememberMe(false)//
        .rememberMeTokenValidity(REMEMBER_ME)//
        .tokenValidity(TOKEN_VALIDITY);

        final Date date = AuthProviderUtil.getTokenValidity(tokenModel);

        assertThat(date).isNotNull();
        assertThat(DatesConverterUtil.formateDateToString(AppConstants.CET_DATE_FORMAT_MAJ_Z, AppConstants.CET_TIMEZONE, date)).contains("1970-01-19 T");
    }

    @Test
    public void tesGetTokenValidity_With_RememberMe()
    {
        final TokenModel tokenModel = new TokenModel()//
        .rememberMe(true)//
        .rememberMeTokenValidity(REMEMBER_ME)//
        .tokenValidity(null);

        final Date date = AuthProviderUtil.getTokenValidity(tokenModel);

        assertThat(date).isNotNull();
        assertThat(DatesConverterUtil.formateDateToString(AppConstants.CET_DATE_FORMAT_MAJ_Z, AppConstants.CET_TIMEZONE, date)).contains("1970-01-19 T");
    }

    @Test
    public void tesGetTokenValidity_With_RememberMe_False_And_TokenValidity()
    {
        final TokenModel tokenModel = new TokenModel()//
        .rememberMe(false)//
        .rememberMeTokenValidity(REMEMBER_ME)//
        .tokenValidity(TOKEN_VALIDITY);
        
        final Date date = AuthProviderUtil.getTokenValidity(tokenModel);

        assertThat(date).isNotNull();
        assertThat(DatesConverterUtil.formateDateToString(AppConstants.CET_DATE_FORMAT_MAJ_Z, AppConstants.CET_TIMEZONE, date)).contains("1970-01-19 T");
    }

    @Test
    public void tesGetTokenValidity_With_RememberMe_False()
    {
        final TokenModel tokenModel = new TokenModel()//
        .rememberMe(false)//
        .rememberMeTokenValidity(ONE_MINUTE)//
        .tokenValidity(null);

        final Date date = AuthProviderUtil.getTokenValidity(tokenModel);

        assertThat(date).isNotNull();
        assertThat(DatesConverterUtil.formateDateToString(AppConstants.CET_DATE_FORMAT_MAJ_Z, AppConstants.CET_TIMEZONE, date)).contains("1970-01-19 T");
    }

    @Test
    public void tesGetTokenValidity_WithRefresToken()
    {
        final TokenModel tokenModel = new TokenModel()//
        .refreshToken(true)//
        .refreshTokenValidity(REFRESH_TOKEN)//
        .rememberMeTokenValidity(ONE_MINUTE)//
        .tokenValidity(null);

        final Date date = AuthProviderUtil.getTokenValidity(tokenModel);

        assertThat(date).isNotNull();
        assertThat(DatesConverterUtil.formateDateToString(AppConstants.CET_DATE_FORMAT_MAJ_Z, AppConstants.CET_TIMEZONE, date)).contains("1970-01-19 T");
    }

    @Test
    public void tesGetTokenValidity_With_Null()
    {
        //
        final Date date = AuthProviderUtil.getTokenValidity(null);

        assertThat(date).isNull();
    }

    @Test
    public void tesGetTokenValidity_ShouldThrowNPE()
    {
        final TokenModel tokenModel = new TokenModel()//
        .rememberMe(false)//
        .rememberMeTokenValidity(null)//
        .tokenValidity(ONE_MINUTE);

        final var exception = assertThrows(NullPointerException.class, () -> {
            AuthProviderUtil.getTokenValidity(tokenModel);
        });

        final var actualMessage = exception.getMessage();

        assertThat(actualMessage).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#getCurrentUserJWTToken()}.
     */
    @Test
    public void testGetUserJWTToken()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        final Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.ADMIN.getAuthority()));
        final var user = AuthProviderUtil.createSpringUser(CommonAppTestsUtil.ADMIN, CommonAppTestsUtil.TOKEN, authorities);
        final var authentication = AuthProviderUtil.createAuthentication(user);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final var jwt = AuthProviderUtil.getCurrentUserJWTToken();

        assertThat(jwt).isNotNull();
        assertThat(jwt.get()).isNotNull();
        assertThat(jwt.get()).isEqualToIgnoringCase(CommonAppTestsUtil.TOKEN);
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#isAuthenticatedUser()}.
     */
    @Test
    public void testIsAuthenticatedUser()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        final Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.ADMIN.getAuthority()));
        final var user = AuthProviderUtil.createSpringUser(CommonAppTestsUtil.ADMIN, CommonAppTestsUtil.TOKEN, authorities);
        final var authentication = AuthProviderUtil.createAuthentication(user);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final var isAuthenticated = AuthProviderUtil.isAuthenticatedUser();

        assertThat(isAuthenticated).isTrue();
    }

    @Test
    public void testIsAuthenticatedUserShouldReturnFalse()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        final Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.ANONYMOUS.getAuthority()));
        final var user = AuthProviderUtil.createSpringUser(CommonAppTestsUtil.ANONYMOUS, CommonAppTestsUtil.ANONYMOUS, authorities);
        final var authentication = AuthProviderUtil.createAuthentication(user);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final var isAuthenticated = AuthProviderUtil.isAuthenticatedUser();

        assertThat(isAuthenticated).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsAuthenticatedUserShouldReturn_WithNullGranted()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        final Collection<GrantedAuthority> authorities = null;
        final var user = AuthProviderUtil.createSpringUser(CommonAppTestsUtil.ANONYMOUS, CommonAppTestsUtil.ANONYMOUS, authorities);
        final var authentication = AuthProviderUtil.createAuthentication(user);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final var isAuthenticated = AuthProviderUtil.isAuthenticatedUser();

        assertThat(isAuthenticated).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsAuthenticatedUserShouldReturn_WithAddNullGranted()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        final Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(null);
        final var user = AuthProviderUtil.createSpringUser(CommonAppTestsUtil.ANONYMOUS, CommonAppTestsUtil.ANONYMOUS, authorities);
        final var authentication = AuthProviderUtil.createAuthentication(user);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final var isAuthenticated = AuthProviderUtil.isAuthenticatedUser();

        assertThat(isAuthenticated).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsAuthenticatedUserShouldReturn_ShouldThrowException()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        context.setAuthentication(null);
        SecurityContextHolder.setContext(null);

        final var isAuthenticated = AuthProviderUtil.isAuthenticatedUser();

        assertThat(isAuthenticated).isFalse();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#isAuthenticated()}.
     */
    @Test
    public void testIsAuthenticated()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        final Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.ADMIN.getAuthority()));
        final var user = AuthProviderUtil.createSpringUser(CommonAppTestsUtil.ADMIN, CommonAppTestsUtil.TOKEN, authorities);
        final var authentication = AuthProviderUtil.createAuthentication(user);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final var isAuthenticated = AuthProviderUtil.isAuthenticated();

        assertThat(isAuthenticated).isTrue();
    }

    @Test
    public void testIsAuthenticated_Anonymous_ShouldReturnFalse()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.ANONYMOUS.getAuthority()));
        final var authentication = AuthProviderUtil.createAnonymousAuthentication(CommonAppTestsUtil.ANONYMOUS, CommonAppTestsUtil.ANONYMOUS, authorities);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final var isAuthenticated = AuthProviderUtil.isAuthenticated();

        assertThat(isAuthenticated).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsAuthenticated_WithNullGranted()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        Collection<GrantedAuthority> authorities = null;
        final var authentication = AuthProviderUtil.createAnonymousAuthentication(CommonAppTestsUtil.ANONYMOUS, CommonAppTestsUtil.ANONYMOUS, authorities);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final var isAuthenticated = AuthProviderUtil.isAuthenticated();

        assertThat(isAuthenticated).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsAuthenticated_WithAddNullGranted()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(null);
        final var authentication = AuthProviderUtil.createAnonymousAuthentication(CommonAppTestsUtil.ANONYMOUS, CommonAppTestsUtil.ANONYMOUS, authorities);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final var isAuthenticated = AuthProviderUtil.isAuthenticated();

        assertThat(isAuthenticated).isFalse();

    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsAuthenticated_ShouldThrowException()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        context.setAuthentication(null);
        SecurityContextHolder.setContext(null);

        final var isAuthenticated = AuthProviderUtil.isAuthenticatedUser();

        assertThat(isAuthenticated).isFalse();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#isAuthenticatedUser(java.lang.String)}.
     */
    @Test
    public void testIsAuthenticatedUserString()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.ADMIN.getAuthority()));
        final var user = AuthProviderUtil.createSpringUser(CommonAppTestsUtil.ADMIN, CommonAppTestsUtil.TOKEN, authorities);
        final var authentication = AuthProviderUtil.createAuthentication(user);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final var isAuthenticated = AuthProviderUtil.isAuthenticatedUser(BaseRolesEnum.ANONYMOUS.getAuthority());
        assertThat(isAuthenticated).isTrue();
    }

    @Test
    public void testIsAuthenticatedUserString_ShouldReturnFalse()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.ANONYMOUS.getAuthority()));
        final var user = AuthProviderUtil.createSpringUser(CommonAppTestsUtil.ANONYMOUS, CommonAppTestsUtil.ANONYMOUS, authorities);
        final var authentication = AuthProviderUtil.createAuthentication(user);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final var isAuthenticated = AuthProviderUtil.isAuthenticatedUser(BaseRolesEnum.ANONYMOUS.getAuthority());
        assertThat(isAuthenticated).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsAuthenticatedUserString_WithNullGranted()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        Collection<GrantedAuthority> authorities = null;
        final var user = AuthProviderUtil.createSpringUser(CommonAppTestsUtil.ANONYMOUS, CommonAppTestsUtil.ANONYMOUS, authorities);
        final var authentication = AuthProviderUtil.createAuthentication(user);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        final var isAuthenticated = AuthProviderUtil.isAuthenticatedUser(BaseRolesEnum.ANONYMOUS.getAuthority());
        assertThat(isAuthenticated).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsAuthenticatedUserString_WithNull()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        context.setAuthentication(null);
        SecurityContextHolder.setContext(null);

        final var isAuthenticated = AuthProviderUtil.isAuthenticatedUser(BaseRolesEnum.ANONYMOUS.getAuthority());
        assertThat(isAuthenticated).isFalse();
    }

    @Test(expected = NullPointerException.class)
    public void testIsAuthenticatedUserString_ShouldThrowException()
    {
        final var isAuthenticated = AuthProviderUtil.isAuthenticatedUser(null);
        assertThat(isAuthenticated).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#isUserInRole(java.lang.String)}.
     */
    @Test
    public void testIsUserInRole()
    {
        final var context = SecurityContextHolder.createEmptyContext();

        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(AuthProviderUtil.createRole(BaseRolesEnum.CLIENT.getAuthority()));
        final var user = AuthProviderUtil.createSpringUser(CommonAppTestsUtil.CLIENT, CommonAppTestsUtil.CLIENT, authorities);
        final var authentication = AuthProviderUtil.createAuthentication(user);

        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        assertThat(AuthProviderUtil.isUserInRole(BaseRolesEnum.CLIENT.getAuthority())).isTrue();
        assertThat(AuthProviderUtil.isUserInRole(BaseRolesEnum.ADMIN.getAuthority())).isFalse();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#isAuthorizationHeader(java.lang.String, java.lang.String)}.
     */
    @Test
    public void testIsAuthorizationHeader()
    {
        final var requestBearerToken = CommonAppTestsUtil.AUTH_TOKEN_PREFIX + CommonAppTestsUtil.AUTH_TOKEN;
        final var prefixToken = CommonAppTestsUtil.AUTH_TOKEN_PREFIX;

        final Boolean isValidHeader = AuthProviderUtil.isAuthorizationHeader(requestBearerToken, prefixToken);

        assertThat(isValidHeader).isTrue();
    }

    @Test
    public void testIsAuthorizationHeaderShouldreturnFalseWithUpperCase()
    {
        final var requestBearerToken = "BEARER " + CommonAppTestsUtil.AUTH_TOKEN;
        final var prefixToken = CommonAppTestsUtil.AUTH_TOKEN_PREFIX;

        final Boolean isValidHeader = AuthProviderUtil.isAuthorizationHeader(requestBearerToken, prefixToken);

        assertThat(isValidHeader).isFalse();
    }

    @Test
    public void testIsAuthorizationHeaderShouldreturnFalseWithoutPrefixe()
    {
        final var requestBearerToken = CommonAppTestsUtil.AUTH_TOKEN;
        final var prefixToken = CommonAppTestsUtil.AUTH_TOKEN_PREFIX;

        final var isValidHeader = AuthProviderUtil.isAuthorizationHeader(requestBearerToken, prefixToken);

        assertThat(isValidHeader).isFalse();
    }

    @Test
    public void testIsAuthorizationHeaderShouldreturnFalseWithoutSpace()
    {
        final var requestBearerToken = CommonAppTestsUtil.TOKEN_PREFIX + CommonAppTestsUtil.AUTH_TOKEN;
        final var prefixToken = CommonAppTestsUtil.AUTH_TOKEN_PREFIX;

        final var isValidHeader = AuthProviderUtil.isAuthorizationHeader(requestBearerToken, prefixToken);

        assertThat(isValidHeader).isFalse();
    }

    @Test
    public void testIsAuthorizationHeaderShouldreturnFalseWithoutEmpty()
    {
        final var prefixToken = CommonAppTestsUtil.AUTH_TOKEN_PREFIX;

        final var isValidHeader = AuthProviderUtil.isAuthorizationHeader(StringUtils.EMPTY, prefixToken);

        assertThat(isValidHeader).isFalse();
    }

    @Test
    public void testIsAuthorizationHeaderShouldreturnFalseWithoutNull()
    {
        final var prefixToken = CommonAppTestsUtil.AUTH_TOKEN_PREFIX;

        final var isValidHeader = AuthProviderUtil.isAuthorizationHeader(null, prefixToken);

        assertThat(isValidHeader).isFalse();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#extractJWTToken(javax.servlet.http.HttpServletRequest, java.lang.String, java.lang.String)}.
     */
    @Test
    public void testExtractJWTToken()
    {
        final var bearerToken = CommonAppTestsUtil.AUTH_TOKEN_PREFIX + CommonAppTestsUtil.AUTH_TOKEN;
        final var request = new MockHttpServletRequest();
        request.addHeader(CommonAppTestsUtil.AUTHORIZATION_HEADER, bearerToken);

        final var authBearer = AuthProviderUtil.extractJWTToken(request, CommonAppTestsUtil.AUTHORIZATION_HEADER, CommonAppTestsUtil.AUTH_TOKEN_PREFIX);

        assertThat(authBearer.get()).contains(CommonAppTestsUtil.AUTH_TOKEN);
    }

    @Test
    public void testExtractJWTTokenShouldReturnEmptyWithoutSpace()
    {
        final var bearerToken = CommonAppTestsUtil.TOKEN_PREFIX + CommonAppTestsUtil.AUTH_TOKEN;
        final var request = new MockHttpServletRequest();
        request.addHeader(CommonAppTestsUtil.AUTHORIZATION_HEADER, bearerToken);

        final var authBearer = AuthProviderUtil.extractJWTToken(request, CommonAppTestsUtil.AUTHORIZATION_HEADER, CommonAppTestsUtil.AUTH_TOKEN_PREFIX);

        assertThat(authBearer).isEmpty();
    }

    @Test
    public void testExtractJWTTokenShouldReturnEmptyWithNullPrefix()
    {
        final var bearerToken = CommonAppTestsUtil.AUTH_TOKEN_PREFIX + CommonAppTestsUtil.AUTH_TOKEN;
        final var request = new MockHttpServletRequest();
        request.addHeader(CommonAppTestsUtil.AUTHORIZATION_HEADER, bearerToken);

        final var authBearer = AuthProviderUtil.extractJWTToken(request, CommonAppTestsUtil.AUTHORIZATION_HEADER, null);

        assertThat(authBearer).isEmpty();
    }

    @Test
    public void testExtractJWTTokenShouldReturnEmptyWithNull()
    {
        final var bearerToken = CommonAppTestsUtil.AUTH_TOKEN_PREFIX + CommonAppTestsUtil.AUTH_TOKEN;
        final var request = new MockHttpServletRequest();
        request.addHeader(CommonAppTestsUtil.AUTHORIZATION_HEADER, bearerToken);

        final var authBearer = AuthProviderUtil.extractJWTToken(request, null, null);

        assertThat(authBearer).isEmpty();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.utils.auth.AuthProviderUtil#isNotExpiredException(java.lang.Class)}.
     */
    @Test
    public void testIsNotExpiredException()
    {
        var signatureException = new SignatureException("Signature Exception.");

        final var isNotExpiration = AuthProviderUtil.isNotExpiredException(signatureException.getClass());

        assertThat(isNotExpiration).isTrue();
    }

    @Test
    public void testIsNotExpiredExceptionShouldReturnTrueWithMalformedJwtException()
    {
        var malformedJwtException = new MalformedJwtException("Jeton JWT mal construit.");

        final var isNotExpiration = AuthProviderUtil.isNotExpiredException(malformedJwtException.getClass());

        assertThat(isNotExpiration).isTrue();
    }

    @Test
    public void testIsNotExpiredExceptionShouldReturnTrueWithUnsupportedJwtException()
    {
        var unsupportedJwtException = new UnsupportedJwtException("Jeton JWT n'est pas construit dans le format attendu.");

        final var isNotExpiration = AuthProviderUtil.isNotExpiredException(unsupportedJwtException.getClass());

        assertThat(isNotExpiration).isTrue();
    }

    @Test
    public void testIsNotExpiredExceptionShouldReturnFalse()
    {
        var expiredJwtException = new ExpiredJwtException(null, null, null);

        final var isNotExpiration = AuthProviderUtil.isNotExpiredException(expiredJwtException.getClass());

        assertThat(isNotExpiration).isFalse();
    }
}
