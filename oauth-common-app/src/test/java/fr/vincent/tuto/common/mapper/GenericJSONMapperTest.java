/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : GenericJSONMapperTest.java
 * Date de création : 25 nov. 2020
 * Heure de création : 12:46:13
 * Package : fr.vincent.tuto.common.mapper
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Lists;

import fr.vincent.tuto.common.config.CommonAppBaseConfig;
import fr.vincent.tuto.common.constants.AppConstants;
import fr.vincent.tuto.common.exception.CustomAppException;
import fr.vincent.tuto.common.model.Article;
import fr.vincent.tuto.common.model.error.ApiResponseError;
import fr.vincent.tuto.common.model.error.ApiValidationError;

/**
 * Classe des tests unitares des objets de type {@link GenericJSONMapper}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@JsonTest
@TestPropertySource(value = { "classpath:common-app-db-test.properties", "classpath:common-app-application-test.properties",
        "classpath:common-app-messages-test.properties" })
@ContextConfiguration(name = "genericJSONMapperTest", classes = { CommonAppBaseConfig.class, GenericJSONMapper.class, JavaMailSenderImpl.class,
        StringHttpMessageConverter.class })
@ActiveProfiles("test")
public class GenericJSONMapperTest
{
    //
    private static final String ARTICLE_JSON = "{\"refArticle\":-9223372036854775808,\"titleArticle\":\"Titre de l'article\",\"categorieArticle\":\"Categorie de l'article\",\"dateCreationArticle\":\"2021-01-23T09:48:00.328\"}";
    private static final String ARTICLE_JSON_PRETTY_PRINT = "{\r\n" + "  \"refArticle\" : -9223372036854775808,\r\n"
    + "  \"titleArticle\" : \"Titre de l'article\",\r\n" + "  \"categorieArticle\" : \"Categorie de l'article\",\r\n"
    + "  \"dateCreationArticle\" : \"2021-01-23T09:51:39.949\"\r\n" + "}";

    private Article article;

    @Autowired
    private GenericJSONMapper genericJSONMapper;

    @Autowired
    private JacksonTester<Article> jsonTester;

    @Value("${vot.json.file.test.location}")
    private String jsonFilePathLocation;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.article = new Article();
        this.article.setCategorieArticle("Categorie de l'article");
        this.article.setTitleArticle("Titre de l'article");
        this.article.setRefArticle(Long.MIN_VALUE);

        this.genericJSONMapper.afterPropertiesSet();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.genericJSONMapper = null;
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.mapper.GenericJSONMapper#toStringJSON(java.lang.Object, java.lang.Boolean)}.
     * 
     * @throws IOException
     */
    @Test
    public void testToStringJSON() throws IOException
    {
        final String strJson = this.genericJSONMapper.toStringJSON(this.article, false);

        // System.err.println(">>>>>> Le flux JSON non formaté de l'articles est :\n" + strJson);

        assertThat(strJson.length()).isPositive();
        assertThat(strJson.toString()).contains("Titre de l'article");
        assertThat(strJson).endsWith("}");

        assertThat(this.jsonTester.write(this.article)).isNotNull();
        assertThat(this.jsonTester.write(this.article))//
        .extractingJsonPathStringValue("@.titleArticle")//
        .isEqualTo("Titre de l'article");
    }

    @Test
    public void testToStringJSONPrettyPrint() throws IOException
    {
        final String strJson = this.genericJSONMapper.toStringJSON(this.article, true);

        // System.err.println(">>>>>> Le flux JSON formaté de l'articles est :\n" + strJson);

        assertThat(strJson).isNotNull();
        assertThat(strJson.length()).isPositive();
        assertThat(strJson.toString()).contains("Categorie de l'article");
        assertThat(strJson).startsWith("{");

        assertThat(this.jsonTester.write(this.article)).isNotNull();
        assertThat(this.jsonTester.write(this.article))//
        .extractingJsonPathStringValue("@.titleArticle")//
        .isEqualTo("Titre de l'article");
    }

    @Test(expected = CustomAppException.class)
    public void testToStringJSONShouldCatchException()
    {
        final String strJson = this.genericJSONMapper.toStringJSON(null, true);

        assertThat(strJson).isNull();
    }

    @Test
    public void testToStringJSONShouldReturnNotPrettyPrint()
    {
        final String strJson = this.genericJSONMapper.toStringJSON(this.article, null);

        assertThat(strJson).isNotNull();
        assertThat(strJson.length()).isPositive();
        assertThat(strJson.toString()).contains("Titre de l'article");
        assertThat(strJson).startsWith("{");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testToStringJSON_parseToMap() throws IOException
    {
        final String strJson = this.genericJSONMapper.toStringJSON(this.article, false);
        assertThat(strJson).isNotNull();

        Map<String, Object> map = this.genericJSONMapper.toJSONObject(strJson, Map.class);

        // System.err.println(">>>>>> La Map est :\n" + map);
        // System.err.println(">>>>>> La Taill est :\n" + map.size());

        assertThat(map).isNotNull();
        assertThat(map.size()).isPositive();
        assertThat(map).containsEntry("titleArticle", "Titre de l'article");
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.mapper.GenericJSONMapper#toStringJSONList(java.util.Collection, java.lang.Boolean)}.
     */
    @Test
    public void testToStringJSONList()
    {
        final List<Article> articles = Lists.newArrayList();
        articles.add(this.article);
        articles.add(this.article);
        articles.add(this.article);

        final List<String> strings = this.genericJSONMapper.toStringJSONList(articles, false);

        assertThat(strings).isNotNull();
        assertThat(strings.size()).isEqualTo(3);
    }

    @Test
    public void testToStringJSONListWithPrettyPrint()
    {
        final List<Article> articles = Lists.newArrayList();
        articles.add(this.article);
        articles.add(this.article);
        articles.add(this.article);

        final List<String> strings = this.genericJSONMapper.toStringJSONList(articles, true);

        assertThat(strings).isNotNull();
        assertThat(strings.size()).isEqualTo(3);
    }

    @Test
    public void testToStringJSONListWithPartialNull()
    {
        final List<Article> articles = Lists.newArrayList();
        articles.add(this.article);
        articles.add(this.article);
        articles.add(null);

        final List<String> strings = this.genericJSONMapper.toStringJSONList(articles, false);

        assertThat(strings).isNotNull();
        assertThat(strings.size()).isEqualTo(2);
    }

    @Test
    public void testToStringJSONListWithFullElementNull()
    {
        final List<Article> articles = Lists.newArrayList();
        articles.add(null);
        articles.add(null);
        articles.add(null);

        final List<String> strings = this.genericJSONMapper.toStringJSONList(articles, false);

        assertThat(strings).isEmpty();
        assertThat(strings.size()).isNotPositive();
    }

    @Test
    public void testToStringJSONListWithNullList()
    {
        final List<String> strings = this.genericJSONMapper.toStringJSONList(null, false);

        assertThat(strings).isNotNull();
        assertThat(strings.size()).isNotPositive();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.mapper.GenericJSONMapper#toJSONObject(java.lang.String, java.lang.Class)}.
     */
    @Test
    public void testToJSONObject()
    {
        final Article article = this.genericJSONMapper.toJSONObject(ARTICLE_JSON, Article.class);

        assertThat(article).isExactlyInstanceOf(Article.class);
        assertThat(article.toString()).isNotNull();
    }

    @Test
    public void testToJSONObjectWithPretty()
    {
        final Article article = this.genericJSONMapper.toJSONObject(ARTICLE_JSON_PRETTY_PRINT, Article.class);

        assertThat(article).isExactlyInstanceOf(Article.class);
        assertThat(article.toString()).isNotNull();
    }

    @Test(expected = CustomAppException.class)
    public void testToJSONObjectWithNullStrJson()
    {
        final Article article = this.genericJSONMapper.toJSONObject(null, Article.class);

        assertThat(article).isNull();
    }

    @Test(expected = CustomAppException.class)
    public void testToJSONObjectWithNullType()
    {
        final Article article = this.genericJSONMapper.toJSONObject(ARTICLE_JSON, null);

        assertThat(article).isNull();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.mapper.GenericJSONMapper#toJSONObjectList(java.util.Collection, java.lang.Class)}.
     */
    @Test
    public void testToJSONObjectList()
    {
        final List<String> articlesStrList = Lists.newArrayList();
        articlesStrList.add(ARTICLE_JSON);
        articlesStrList.add(ARTICLE_JSON);
        articlesStrList.add(ARTICLE_JSON);

        final List<Article> articles = (List<Article>) this.genericJSONMapper.toJSONObjectList(articlesStrList, Article.class);

        assertThat(articles).isNotEmpty();
        assertThat(articles.size()).isPositive();

        assertThat(articles.get(0).getCategorieArticle()).isEqualTo("Categorie de l'article");
        assertThat(articles.get(0).getTitleArticle()).isEqualTo("Titre de l'article");
    }

    @Test
    public void testToJSONObjectListWithMixte()
    {
        final List<String> articlesStrList = Lists.newArrayList();
        // articlesStrList.add(ARTICLE_JSON);
        articlesStrList.add(ARTICLE_JSON_PRETTY_PRINT);
        articlesStrList.add(ARTICLE_JSON_PRETTY_PRINT);

        final List<Article> articles = (List<Article>) this.genericJSONMapper.toJSONObjectList(articlesStrList, Article.class);

        assertThat(articles).isNotEmpty();
        assertThat(articles.size()).isPositive();

        assertThat(articles.get(0).getCategorieArticle()).isEqualTo("Categorie de l'article");
        assertThat(articles.get(0).getTitleArticle()).isEqualTo("Titre de l'article");
    }

    @Test
    public void testToJSONObjectListPartialNull()
    {
        final List<String> articlesStrList = Lists.newArrayList();
        articlesStrList.add(ARTICLE_JSON);
        articlesStrList.add(ARTICLE_JSON_PRETTY_PRINT);
        articlesStrList.add(null);

        final List<Article> articles = (List<Article>) this.genericJSONMapper.toJSONObjectList(articlesStrList, Article.class);

        assertThat(articles).isNotEmpty();
        assertThat(articles.size()).isEqualTo(2);

        assertThat(articles.get(0).getCategorieArticle()).isEqualTo("Categorie de l'article");
        assertThat(articles.get(0).getTitleArticle()).isEqualTo("Titre de l'article");
    }

    @Test
    public void testToJSONObjectListFullElementNull()
    {
        final List<String> articlesStrList = Lists.newArrayList();
        articlesStrList.add(null);
        articlesStrList.add(null);
        articlesStrList.add(null);

        final List<Article> articles = (List<Article>) this.genericJSONMapper.toJSONObjectList(articlesStrList, Article.class);

        assertThat(articles).isEmpty();
        assertThat(articles.size()).isNotPositive();
    }

    @Test
    public void testToJSONObjectListNull()
    {
        final List<Article> articles = (List<Article>) this.genericJSONMapper.toJSONObjectList(null, Article.class);

        assertThat(articles).isEmpty();
        assertThat(articles.size()).isNotPositive();
    }

    @Test
    public void testToJSONObjectListFullNull()
    {
        final List<?> articles = (List<?>) this.genericJSONMapper.toJSONObjectList(null, null);

        assertThat(articles).isEmpty();
        assertThat(articles.size()).isNotPositive();
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.mapper.GenericJSONMapper#writeJSONFile(java.lang.String, java.lang.Boolean, java.lang.Object, java.lang.Boolean)}.
     */
    @Test
    public void testWriteJSONFile()
    {
        final List<Article> articles = Lists.newArrayList();
        articles.add(this.article);
        articles.add(this.article);
        articles.add(this.article);

        final String filename = this.jsonFilePathLocation + Article.class.getSimpleName() + "_Collection" + AppConstants.JSON_FILE_SUFFIXE;

        final Boolean isFileCreated = this.genericJSONMapper.writeJSONFile(filename, false, articles, false);

        assertThat(isFileCreated).isTrue();
    }

    @Test
    public void testWriteJSONFileWithoutSuffixe()
    {
        final List<Article> articles = Lists.newArrayList();
        articles.add(this.article);
        articles.add(this.article);
        articles.add(this.article);

        final String filename = this.jsonFilePathLocation + Article.class.getSimpleName() + "_Collection";

        final Boolean isFileCreated = this.genericJSONMapper.writeJSONFile(filename, false, articles, false);

        assertThat(isFileCreated).isTrue();
    }

    @Test
    public void testWriteJSONFileInMemory()
    {
        final List<Article> articles = Lists.newArrayList();
        articles.add(this.article);
        articles.add(this.article);
        articles.add(this.article);
        // false
        final String filename = Article.class.getSimpleName() + "_InMemory_Collection";

        final Boolean isFileCreated = this.genericJSONMapper.writeJSONFile(filename, true, articles, false);

        assertThat(isFileCreated).isTrue();
    }

    @Test
    public void testWriteJSONFilePrettyPrintTrue()
    {
        final List<Article> articles = Lists.newArrayList();
        articles.add(this.article);
        articles.add(this.article);
        articles.add(this.article);

        final String filename = this.jsonFilePathLocation + Article.class.getSimpleName() + "_Collection_formated" + AppConstants.JSON_FILE_SUFFIXE;

        final Boolean isFileCreated = this.genericJSONMapper.writeJSONFile(filename, false, articles, true);

        assertThat(isFileCreated).isTrue();
    }

    @Test(expected = CustomAppException.class)
    public void testWriteJSONFileWithNullFilename()
    {
        final List<Article> articles = Lists.newArrayList();
        articles.add(this.article);
        articles.add(this.article);
        articles.add(this.article);

        final Boolean isFileCreated = this.genericJSONMapper.writeJSONFile(null, false, articles, true);

        assertThat(isFileCreated).isFalse();
    }

    @Test(expected = CustomAppException.class)
    public void testWriteJSONFileInMemoryShouldCatchException()
    {
        final List<Article> articles = Lists.newArrayList();
        articles.add(this.article);
        articles.add(this.article);
        articles.add(this.article);

        final String filename = this.jsonFilePathLocation + Article.class.getSimpleName() + "_InMemory_Collection";

        final Boolean isFileCreated = this.genericJSONMapper.writeJSONFile(filename, true, articles, false);

        assertThat(isFileCreated).isFalse();
    }

    @Test
    public void testWriteJSONFile_ApiValidationError_PrettyPrintTrue()
    {
        ApiValidationError validationError = new ApiValidationError()//
        .object("Objet a traiter")//
        .field("Le champ en validation")//
        .rejectedValue("La valeur de rejet")//
        .message("Le message d'erreur ");

        final List<ApiValidationError> apiValidationErrors = Lists.newArrayList();

        apiValidationErrors.add(validationError);
        apiValidationErrors.add(validationError);
        apiValidationErrors.add(validationError);

        final String filename = this.jsonFilePathLocation + ApiValidationError.class.getSimpleName() + "_Collection_formated"
        + AppConstants.JSON_FILE_SUFFIXE;

        final Boolean isFileCreated = this.genericJSONMapper.writeJSONFile(filename, false, apiValidationErrors, true);

        assertThat(isFileCreated).isTrue();
    }

    @Test
    public void testWriteJSONFile_ApiResponseError_PrettyPrintTrue()
    {
        ApiValidationError validationError = new ApiValidationError()//
        .object("Objet a traiter en validation")//
        .field("Le champ en validation")//
        .rejectedValue("La valeur de rejet")//
        .message("Le message d'erreur ");

        final List<ApiValidationError> validationErrors = Lists.newArrayList();

        validationErrors.add(validationError);
        validationErrors.add(validationError);

        ApiResponseError responseError = new ApiResponseError()//
        .status(HttpStatus.CREATED)//
        .timestamp(LocalDateTime.now(ZoneId.systemDefault()))//
        .details("Erreur retour API")//
        .validationErrors(validationErrors);

        List<ApiResponseError> errors = Lists.newArrayList();
        errors.add(responseError);

        final String filename = this.jsonFilePathLocation + ApiResponseError.class.getSimpleName() + "_Collection_formated"
        + AppConstants.JSON_FILE_SUFFIXE;

        final Boolean isFileCreated = this.genericJSONMapper.writeJSONFile(filename, false, errors, true);

        assertThat(isFileCreated).isTrue();
    }

    @Test
    public void testWriteJSONFile_ApiResponseError_PrettyPrintTrue2()
    {
        ApiResponseError responseError = new ApiResponseError()//
        .status(HttpStatus.CREATED)//
        .timestamp(LocalDateTime.now(ZoneId.systemDefault()))//
        .details("Erreur retour API")//
        .validationErrors(null);

        List<ApiResponseError> errors = Lists.newArrayList();
        errors.add(responseError);

        final String filename = this.jsonFilePathLocation + ApiResponseError.class.getSimpleName() + "_Collection_formated_Null_List"
        + AppConstants.JSON_FILE_SUFFIXE;

        final Boolean isFileCreated = this.genericJSONMapper.writeJSONFile(filename, false, errors, true);

        assertThat(isFileCreated).isTrue();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.mapper.GenericJSONMapper#afterPropertiesSet()}.
     * 
     * @throws Exception
     */
    @Test
    public void testAfterPropertiesSet() throws Exception
    {
        this.genericJSONMapper.afterPropertiesSet();
        
        assertThat(this.genericJSONMapper).isNotNull();
    }
}
