/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : EnvHostConfigTest.java
 * Date de création : 30 nov. 2020
 * Heure de création : 11:41:17
 * Package : fr.vincent.tuto.common.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.config;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * Classe des tests Unitaires {@link EnvHostConfig}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:common-app-db-test.properties",
        "classpath:common-app-application-test.properties", "classpath:common-app-messages-test.properties" })
@ContextConfiguration(name = "envHostConfigTest", classes = { CommonAppBaseConfig.class, CommonBeansConfig.class,
        JavaMailSenderImpl.class, LogStartConfig.class, EnvHostConfig.class })
@ActiveProfiles("test")
@SpringBootTest
public class EnvHostConfigTest
{
    @Autowired
    private EnvHostConfig envHostConfig;

    @Value("${vot.swagger-props.nom-machine-dev}")
    private String hostAdresse;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        this.envHostConfig.afterPropertiesSet();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.envHostConfig = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.config.EnvHostConfig#getHostAddress()}.
     */
    @Test
    public void testGetHostAddress()
    {
        final String host = this.envHostConfig.getHostAddress();

        System.err.println(">>>>> Le Host :\n" + host);

        assertThat(host).isNotEmpty();
        assertThat(host.length()).isGreaterThan(0);
        assertThat(host).isEqualTo(this.hostAdresse.trim());
    }

    @Test
    public void testGetHostAddress_WithNull()
    {
        ReflectionTestUtils.setField(this.envHostConfig, "hostAddress", null);
        final String host = this.envHostConfig.getHostAddress();

        assertThat(host).isNull();
    }

    @Test
    public void testGetHostAddress_WithEmpty()
    {
        ReflectionTestUtils.setField(this.envHostConfig, "hostAddress", StringUtils.EMPTY);
        final String host = this.envHostConfig.getHostAddress();

        assertThat(host).isNull();
    }

    @Test
    public void testGetHostAddress_WithWrong()
    {
        ReflectionTestUtils.setField(this.envHostConfig, "hostAddress", "Wrong_value");
        final String host = this.envHostConfig.getHostAddress();

        assertThat(host).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.config.EnvHostConfig#isLocalHost()}.
     */
    @Test
    public void testIsLocalHost()
    {
        final Boolean isValidHost = this.envHostConfig.isLocalHost();

        assertThat(isValidHost).isNotNull();
        assertThat(isValidHost).isTrue();
    }

    @Test
    public void testIsLocalHost_False()
    {
        ReflectionTestUtils.setField(this.envHostConfig, "hostAddress", "Wrong_value");

        final Boolean isValidHost = this.envHostConfig.isLocalHost();

        assertThat(isValidHost).isNotNull();
        assertThat(isValidHost).isFalse();
    }

    @Test(expected = NullPointerException.class)
    public void testIsLocalHost_ShouldThrowException()
    {
        ReflectionTestUtils.setField(this.envHostConfig, "hostAddress", null);

        final Boolean isValidHost = this.envHostConfig.isLocalHost();

        assertThat(isValidHost).isNotNull();
        assertThat(isValidHost).isFalse();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.config.EnvHostConfig#afterPropertiesSet()}.
     * 
     * @throws Exception
     */
    @Test
    public void testAfterPropertiesSet() throws Exception
    {
        this.envHostConfig.afterPropertiesSet();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.config.EnvHostConfig#destroy()}.
     * 
     * @throws Exception
     */
    @Test
    public void testDestroy() throws Exception
    {
        this.envHostConfig.destroy();
    }
}
