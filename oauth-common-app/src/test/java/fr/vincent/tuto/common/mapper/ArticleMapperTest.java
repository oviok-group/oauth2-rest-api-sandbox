/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : ArticleMapperTest.java
 * Date de création : 26 nov. 2020
 * Heure de création : 02:43:40
 * Package : fr.vincent.tuto.common
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Lists;

import fr.vincent.tuto.common.config.CommonAppBaseConfig;
import fr.vincent.tuto.common.model.Article;
import fr.vincent.tuto.common.model.ArticleDTO;
import fr.vincent.tuto.common.model.ArticleMapper;

/**
 * Classe des tests unitaires des objets de type {@link ArticleMapper}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
// @JsonTest
@TestPropertySource(value = { "classpath:common-app-db-test.properties",
        "classpath:common-app-application-test.properties", "classpath:common-app-messages-test.properties" })
@ContextConfiguration(name = "articleMapperTest", classes = { CommonAppBaseConfig.class, GenericJSONMapper.class,
        ArticleMapper.class, JavaMailSenderImpl.class })
@ActiveProfiles("test")
@SpringBootTest
public class ArticleMapperTest
{
    //
    @Autowired
    private ArticleMapper articleMapper;

    private Article article;
    private ArticleDTO dto;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        // Article
        this.article = new Article();
        this.article.setCategorieArticle("Categorie de l'article");
        this.article.setTitleArticle("Titre de l'article");
        this.article.setRefArticle(Long.MIN_VALUE);

        // DTO
        this.dto = new ArticleDTO();
        this.dto.setCategorieArticle("Categorie de l'article");
        this.dto.setTitleArticle("Titre de l'article");
        this.dto.setRefArticle(Long.MIN_VALUE);

        this.articleMapper.afterPropertiesSet();
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        this.articleMapper = null;
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.model.ArticleMapper#toDestObject(fr.vincent.tuto.common.model.Article)}.
     */
    @Test
    public void testToDestObjectArticle()
    {
        final ArticleDTO dto = this.articleMapper.toDestObject(this.article);

        assertThat(dto).isNotNull();
        assertThat(dto.toString()).isNotNull();
        assertThat(dto.getRefArticle()).isEqualTo(this.dto.getRefArticle());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testToDestObjectArticleShoulRetunrException()
    {
        final ArticleDTO dto = this.articleMapper.toDestObject(null);

        assertThat(dto).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.model.ArticleMapper#toSourceObject(fr.vincent.tuto.common.model.ArticleDTO)}.
     */
    @Test
    public void testToSourceObjectArticleDTO()
    {
        final Article article = this.articleMapper.toSourceObject(this.dto);
        assertThat(article).isNotNull();
        assertThat(article.toString()).isNotNull();
        assertThat(article).isEqualTo(this.article);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testToSourceObjectArticleDTOShouldReturnException()
    {
        final Article article = this.articleMapper.toSourceObject(null);
        assertThat(article).isNull();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.mapper.GenericObjectMapper#toDestObjectList(java.util.Collection)}.
     */
    @Test
    public void testToDestObjectList()
    {
        List<Article> articles = Lists.newArrayList();
        articles.add(this.article);
        articles.add(this.article);
        articles.add(this.article);
        articles.add(this.article);

        final List<ArticleDTO> dtos = (List<ArticleDTO>) this.articleMapper.toDestObjectList(articles);

        assertThat(dtos).isNotEmpty();
        assertThat(dtos.size()).isEqualTo(4);
    }

    @Test
    public void testToDestObjectListMiste()
    {
        List<Article> articles = Lists.newArrayList();
        articles.add(this.article);
        articles.add(this.article);
        articles.add(null);
        articles.add(null);

        final List<ArticleDTO> dtos = (List<ArticleDTO>) this.articleMapper.toDestObjectList(articles);

        assertThat(dtos).isNotEmpty();
        assertThat(dtos.size()).isEqualTo(2);
    }

    @Test
    public void testToDestObjectListShouldReturnEmpty()
    {
        final List<ArticleDTO> dtos = (List<ArticleDTO>) this.articleMapper.toDestObjectList(null);

        assertThat(dtos).isEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.mapper.GenericObjectMapper#toSourceObjectList(java.util.Collection)}.
     */
    @Test
    public void testToSourceObjectList()
    {
        List<ArticleDTO> dtos = Lists.newArrayList();
        dtos.add(this.dto);
        dtos.add(this.dto);
        dtos.add(this.dto);
        dtos.add(this.dto);

        final List<Article> articles = (List<Article>) this.articleMapper.toSourceObjectList(dtos);

        assertThat(articles).isNotEmpty();
        assertThat(articles.size()).isEqualTo(4);
    }

    @Test
    public void testToSourceObjectListMixte()
    {
        List<ArticleDTO> dtos = Lists.newArrayList();
        dtos.add(this.dto);
        dtos.add(this.dto);
        dtos.add(null);
        dtos.add(null);

        final List<Article> articles = (List<Article>) this.articleMapper.toSourceObjectList(dtos);

        assertThat(articles).isNotEmpty();
        assertThat(articles.size()).isEqualTo(2);
    }

    @Test
    public void testToSourceObjectListShouldReturnEmpty()
    {
        final List<Article> dtos = (List<Article>) this.articleMapper.toSourceObjectList(null);

        assertThat(dtos).isEmpty();
    }

    /**
     * Test method for {@link fr.vincent.tuto.common.mapper.GenericObjectMapper#afterPropertiesSet()}.
     * 
     * @throws Exception
     */
    @Test
    public void testAfterPropertiesSet() throws Exception
    {
        this.articleMapper.afterPropertiesSet();
        assertThat(this.articleMapper).isNotNull();
    }

    @Test
    public void testNotNullResource()
    {
        assertThat(this.articleMapper).isNotNull();
    }

}
