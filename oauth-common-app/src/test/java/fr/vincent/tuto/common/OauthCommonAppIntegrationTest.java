/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : OauthCommonAppIntegrationTest.java
 * Date de création : 27 nov. 2020
 * Heure de création : 10:27:46
 * Package : fr.vincent.tuto.common
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import fr.vincent.tuto.common.config.CommonBeansConfigTest;
import fr.vincent.tuto.common.mapper.ArticleMapperTest;
import fr.vincent.tuto.common.mapper.GenericJSONMapperTest;
import fr.vincent.tuto.common.service.props.ApplicationPropsServiceTest;
import fr.vincent.tuto.common.service.props.DatabasePropsServiceTest;
import fr.vincent.tuto.common.utils.auth.AuthProviderUtilTest;
import fr.vincent.tuto.common.utils.crypto.CryptoProviderUtilTest;
import fr.vincent.tuto.common.utils.crypto.KeyPairProviderUtilTest;
import fr.vincent.tuto.common.utils.date.DatesConverterUtilTest;
import fr.vincent.tuto.common.utils.random.RandomUniqueIdUtilTest;
import fr.vincent.tuto.common.utils.schema.XsdSchemaGeneratorUtilTest;

/**
 * Classe des Tests d'intégration des tests unitaires du module applicatif.
 * 
 * @author Vincent Otchoun
 */
@Ignore
@SpringBootApplication
@RunWith(Suite.class)
// @SpringBootTest
// @ContextConfiguration(classes = { OAuthCommonAppApplication.class }, loader = SpringBootContextLoader.class)
@SuiteClasses(value = { CommonBeansConfigTest.class, DatabasePropsServiceTest.class, ApplicationPropsServiceTest.class,
        AuthProviderUtilTest.class, CryptoProviderUtilTest.class, KeyPairProviderUtilTest.class,
        RandomUniqueIdUtilTest.class, XsdSchemaGeneratorUtilTest.class, GenericJSONMapperTest.class,
        ArticleMapperTest.class, DatesConverterUtilTest.class })
public class OauthCommonAppIntegrationTest
{
    //
}
