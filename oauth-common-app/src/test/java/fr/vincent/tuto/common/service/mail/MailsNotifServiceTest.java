/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : MailsNotifServiceTest.java
 * Date de création : 28 nov. 2020
 * Heure de création : 17:25:52
 * Package : fr.vincent.tuto.common.service.mail
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.service.mail;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import fr.vincent.tuto.common.config.CommonAppBaseConfig;
import fr.vincent.tuto.common.model.mo.MailModel;
import fr.vincent.tuto.common.service.props.ApplicationPropsService;
import fr.vincent.tuto.common.service.props.DatabasePropsService;

/**
 * Classe des Test Unitaires des objet de type {@link MailsNotifService}
 * 
 * @author Vincent Otchoun
 */
@RunWith(SpringRunner.class)
@TestPropertySource(value = { "classpath:common-app-db-test.properties", "classpath:common-app-application-test.properties", "classpath:common-app-messages-test.properties" })
@ContextConfiguration(name = "mailsNotifServiceTest", classes = { CommonAppBaseConfig.class, DatabasePropsService.class, ApplicationPropsService.class, JavaMailSenderImpl.class,
        MailsNotifService.class })
@SpringBootTest
@ActiveProfiles("test")
public class MailsNotifServiceTest
{
    //
    @Autowired
    // @Resource
    // @InjectMocks
    private MailsNotifService mailsNotifService;

    // @Spy
    // @Autowired

    // @Mock
    @Resource
    @InjectMocks
    // @Autowired
    private JavaMailSenderImpl javaMailSender;

    // @Mock
    @Resource
    @InjectMocks
    private ApplicationPropsService applicationPropsService;

    @Captor
    private ArgumentCaptor<MimeMessage> messageCaptor;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        // MockitoAnnotations.initMocks(this);

        this.mailsNotifService.setJavaMailSender(this.javaMailSender);
        this.mailsNotifService.setApplicationPropsService(this.applicationPropsService);

        // when(this.applicationPropsService.getMailProps().getFrom()).thenReturn(FROM);

        // doNothing().when(this.javaMailSender).send(ArgumentMatchers.any(MimeMessage.class));
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
    }

    /**
     * Test method for
     * {@link fr.vincent.tuto.common.service.mail.MailsNotifService#sendMail(fr.vincent.tuto.common.model.mo.MailModel)}.
     */
    @Test
    public void testSendMail()
    {
        MailModel mailModel = new MailModel()//
        .email("vincokri31@live.fr")//
        .subject("testSubject")//
        .content("testContent")//
        .isMultipart(Boolean.FALSE.booleanValue())//
        .isHtml(Boolean.FALSE.booleanValue());

        mailsNotifService.sendMail(mailModel);
    }

    @Ignore
    @Test
    public void testSendMail_WithNullNull()
    {
        @SuppressWarnings("unused")
        MailModel mailModel = new MailModel()//
        .email("john.doe@example.com")//
        .subject("testSubject")//
        .content("testContent")//
        .isMultipart(Boolean.FALSE.booleanValue())//
        .isHtml(Boolean.FALSE.booleanValue());

        fail("Not yet implemented");
    }

    @Ignore
    @Test
    public void testSendMail_WithArrayTo()
    {
        @SuppressWarnings("unused")
        MailModel mailModel = new MailModel()//
        .email("john.doe@example.com, to@first-email.com, to@second-email.com")//
        .subject("testSubject")//
        .content("testContent")//
        .isMultipart(Boolean.FALSE.booleanValue())//
        .isHtml(Boolean.FALSE.booleanValue());

        fail("Not yet implemented");
    }

    @Ignore
    @Test
    public void testSendMail_Html()
    {
        @SuppressWarnings("unused")
        MailModel mailModel = new MailModel()//
        .email("john.doe@example.com")//
        .subject("testSubject")//
        .content("testContent")//
        .isMultipart(Boolean.FALSE.booleanValue())//
        .isHtml(Boolean.FALSE.booleanValue());

        fail("Not yet implemented");
    }

    @Ignore
    @Test
    public void testSendMail_Multipart()
    {
        @SuppressWarnings("unused")
        MailModel mailModel = new MailModel()//
        .email("john.doe@example.com")//
        .subject("testSubject")//
        .content("testContent")//
        .isMultipart(Boolean.FALSE.booleanValue())//
        .isHtml(Boolean.FALSE.booleanValue());

        fail("Not yet implemented");
    }

    @Ignore
    @Test
    public void testSendMail_Multipart_Html()
    {
        @SuppressWarnings("unused")
        MailModel mailModel = new MailModel()//
        .email("john.doe@example.com")//
        .subject("testSubject")//
        .content("testContent")//
        .isMultipart(Boolean.FALSE.booleanValue())//
        .isHtml(Boolean.FALSE.booleanValue());

        fail("Not yet implemented");
    }

    @Test(expected = NullPointerException.class)
    public void testSendMailWithNullParam() throws MessagingException
    {
        this.mailsNotifService.sendMail(null);
    }

    @Test
    public void testNotNullResources()
    {
        assertThat(this.mailsNotifService.getApplicationPropsService()).isNotNull();
        assertThat(this.mailsNotifService.getJavaMailSender()).isNotNull();
    }

}
