/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : ArticleMapper.java
 * Date de création : 26 nov. 2020
 * Heure de création : 02:28:25
 * Package : fr.vincent.tuto.common
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.model;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import fr.vincent.tuto.common.mapper.GenericObjectMapper;

/**
 * @author Vincent Otchoun
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ArticleMapper extends GenericObjectMapper<Article, ArticleDTO>
{
    /**
     * @param pModelMapper
     */
    protected ArticleMapper(ModelMapper pModelMapper)
    {
        super(pModelMapper);
    }

    //
    @Override
    public ArticleDTO toDestObject(Article pSourceObject)
    {
        final ArticleDTO dto = this.modelMapper.map(pSourceObject, ArticleDTO.class);
        dto.setRefArticle(pSourceObject.getRefArticle());
        dto.setCategorieArticle(pSourceObject.getCategorieArticle());
        dto.setTitleArticle(pSourceObject.getTitleArticle());
        dto.setDateCreationArticle(pSourceObject.getDateCreationArticle());

        return dto;
    }

    @Override
    public Article toSourceObject(ArticleDTO pDestObject)
    {
        final Article article = this.modelMapper.map(pDestObject, Article.class);

        article.setRefArticle(pDestObject.getRefArticle());
        article.setCategorieArticle(pDestObject.getCategorieArticle());
        article.setTitleArticle(pDestObject.getTitleArticle());
        article.setDateCreationArticle(pDestObject.getDateCreationArticle());

        return article;
    }
}
