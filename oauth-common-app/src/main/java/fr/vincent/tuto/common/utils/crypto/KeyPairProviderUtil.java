/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : KeyPairProviderUtil.java
 * Date de création : 22 nov. 2020
 * Heure de création : 21:24:12
 * Package : fr.vincent.tuto.common.utils.crypto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.utils.crypto;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import org.springframework.util.ResourceUtils;

import fr.vincent.tuto.common.constants.AppConstants;
import fr.vincent.tuto.common.exception.CustomAppException;
import fr.vincent.tuto.common.model.mo.CryptoModel;

/**
 * Utilitaire pour la cryptographie asymétique avec clef publique (RSA).
 * <p>
 * <ul>
 * <li>Générer la paire de clés privée/publique à partir de l'algorithme.</li>
 * <li>Construire la paire de clés privée/publique à partir du Keystore.</li>
 * <li>Construire le certificat X509 encodé Base64 à partir du Keystore.</li>
 * <li>Lire la clé dans un fichier.</li>
 * <li>Construire la clé privée à partir de la paire de clés.</li>
 * <li>Construire la clé privée RSA encodée Base64.</li>
 * <li>Construire la clé privée RSA à partir d'une chaîne.</li>
 * <li>Construire la clé privée RSA à partir d'un fichier.</li>
 * <li>Construire la clé publique à partir de la paire de clés.</li>
 * <li>Construire la clé publique RSA encodée Base64.</li>
 * <li>Construire la clé publique RSA à partir d'une chaîne.</li>
 * <li>Construire la clé publique RSA à partir d'un fichier.</li>
 * <li>Ecrire le contenu de la clé dans un fichier.</li>
 * <li>Chiffrer un message avec la clé publique issue de la paire de clés.</li>
 * <li>Déchiffrer avec la clé privée un message crypté avec la clé publique correspondante.</li>
 * <li>Signer un message avec la clé privée issue de la paire de clés.</li>
 * <li>Valider/Vérifier avec la clé publique la signature d'un message signé avec la clé privée correspondante.</li>
 * </ul>
 * 
 * @author Vincent Otchoun
 */
public final class KeyPairProviderUtil
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(KeyPairProviderUtil.class);

    private static final String STORE_PASS_MESSAGE = "Le 'storepass' du magasin des clés ne peut être null ou vide.";
    private static final String ALIAS_MESSAGE = "L'alias du magasin des clés ne peut être null ou vide.";
    private static final String KEY_PASS_MESSAGE = "Le 'keypass' du magasin des clés ne peut être null ou vide.";

    /**
     * Constructeur private de l'utilitaire.
     */
    private KeyPairProviderUtil()
    {
    }

    /**
     * Construire la paire de clés privée/publique.
     * 
     * @param pAlgorithm algorithme de génération
     * @return la paire de clés privée/publique.
     */
    public static KeyPair generateKeyPair(final String pAlgorithm)
    {
        LOGGER.info("[genererKeyPair] - Construire la paire de clés - Algorithme : [{}]", pAlgorithm);

        KeyPair keyPair = null;

        try
        {
            final var generator = KeyPairGenerator.getInstance(pAlgorithm);
            final var random = SecureRandom.getInstanceStrong();
            generator.initialize(AppConstants.RSA_GEN_KEY_SIZE, random);
            keyPair = generator.generateKeyPair();
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new CustomAppException(e);
        }
        return keyPair;
    }

    /**
     * Recuperer la paire de clés à partir du magasin des clés : le Keystore.
     * 
     * @param pFileLocation emplacement du magasin.
     * @param pCryptoModel  les paramètres de génération des clés du magasin.
     * @return la paire de clés privée/publique.
     */
    public static KeyPair getKeyPairFromKeystore(final String pFileLocation, final CryptoModel pCryptoModel)
    {
        LOGGER.info("[getKeyPairFromKeystore] - Recuperer la paire de clés du magasin des clés.");

        KeyPair keyPair = null;

        try
        {
            final var alias = pCryptoModel.getAliasKeystore();
            final var keystorePaswword = pCryptoModel.getKeystorePassword(); // -storepass
            final var keypassword = pCryptoModel.getKeyPassword(); // -keypass

            Assert.notNull(alias, ALIAS_MESSAGE);
            Assert.notNull(keystorePaswword, STORE_PASS_MESSAGE);
            Assert.notNull(keypassword, KEY_PASS_MESSAGE);

            final var magasinCle = ResourceUtils.getFile(pFileLocation);

            final var magasin = KeyStore.getInstance(magasinCle, keystorePaswword.toCharArray());
            final var passwordProtection = new PasswordProtection(keypassword.toCharArray());

            final var privateKeyEntry = (PrivateKeyEntry) magasin.getEntry(alias, passwordProtection);
            final var cert = magasin.getCertificate(alias);

            final var certPublicKey = cert.getPublicKey();
            final var entryPrivateKey = privateKeyEntry.getPrivateKey();
            keyPair = new KeyPair(certPublicKey, entryPrivateKey);
        }
        catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException | UnrecoverableEntryException e)
        {
            throw new CustomAppException(e);
        }
        return keyPair;
    }

    /**
     * Recuperer la clé à partir du magasin des clés.
     * 
     * @param pFileLocation emplacement du magasin.
     * @param pCryptoModel  les paramètres de génération des clés du magasin.
     * @return la clé.
     */
    public static Key getKeyFromKeystore(final String pFileLocation, final CryptoModel pCryptoModel)
    {
        LOGGER.info("[getKeyFromKeystore] - Recuperer la clé à partir du magasin des clés.");

        Key key = null;

        try
        {
            final var alias = pCryptoModel.getAliasKeystore();
            final var keystorePaswword = pCryptoModel.getKeystorePassword(); // -storepass
            final var keypassword = pCryptoModel.getKeyPassword(); // -keypass

            Assert.notNull(alias, ALIAS_MESSAGE);
            Assert.notNull(keystorePaswword, STORE_PASS_MESSAGE);
            Assert.notNull(keypassword, KEY_PASS_MESSAGE);

            final var magasinCle = ResourceUtils.getFile(pFileLocation);
            final var magasin = KeyStore.getInstance(magasinCle, keystorePaswword.toCharArray());

            key = magasin.getKey(alias, keypassword.toCharArray());
        }
        catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException | UnrecoverableEntryException e)
        {
            throw new CustomAppException(e);
        }
        return key;
    }

    /**
     * Construire le certificat X509 à partir du magasin des clés privées : le Keystore.
     * 
     * @param pFileLocation emplacement du magasin.
     * @param pCryptoModel  les paramètres de génération des clés du magasin.
     * @return la pairede clés privée/publique.
     */
    public static String getCertFromKeystore(final String pFileLocation, final CryptoModel pCryptoModel)
    {
        LOGGER.info("[getCertFromKeystore] - Construire le certificat X509 à partir du magasin des clés.");

        String x509CertStr = null;

        try
        {
            final var alias = pCryptoModel.getAliasKeystore();
            final var keystorePaswword = pCryptoModel.getKeystorePassword(); // -storepass

            final var magasinCle = ResourceUtils.getFile(pFileLocation);
            final var magasin = KeyStore.getInstance(magasinCle, keystorePaswword.toCharArray());

            final var cert = magasin.getCertificate(alias);
            final var certBytes = cert.getEncoded();
            x509CertStr = AppConstants.encodeBase64String(certBytes);
        }
        catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e)
        {
            throw new CustomAppException(e);
        }
        return x509CertStr;
    }

    /**
     * Lire une clé dans un fichier au format DER (.der) car le format PEM (.key) généré à partir d'OpenSSL n'est pas
     * accepté par l'API Java.
     * 
     * @param pFilePath la localisation du ficher à lire.
     * @return la concatenation des blocs de clés dans le fichier.
     */
    public static String getKeyFromFile(final String pFilePath)
    {
        LOGGER.info("[getKeyFromFile] - Lire une clé dans un fichier.");
        //
        final StringBuilder ligneBuilder = new StringBuilder();
        String pemKeyLineStr = null;

        final var fichier = Paths.get(pFilePath);
        traiterFichier(fichier);

        try
        {
            final List<String> lines = Files.readAllLines(fichier, AppConstants.CHARSET_UTF_8);
            lines.stream().forEach(ligneBuilder::append);
            pemKeyLineStr = ligneBuilder.toString();
        }
        catch (Exception e)
        {
            throw new CustomAppException(e);
        }
        return pemKeyLineStr;
    }

    /**
     * Construire la clé privée de la paire de clés privée/publique.
     * 
     * @param pKeyPair la paire de clés.
     * @return la clé privée.
     */
    public static PrivateKey getPrivateKey(final KeyPair pKeyPair)
    {
        LOGGER.info("[getPrivateKey] - Construire la clé privée de la paire clés privée/publique.");
        return pKeyPair.getPrivate();
    }

    /**
     * Générer une clé privée encodée Base64.
     * 
     * @param pKeyPair la paire clé privée/publique.
     * @return la clé privée RSA encodée Base64.
     */
    public static String rsaPrivateKey(final RSAPrivateKey pRsaPrivateKey)
    {
        LOGGER.info("[rsaPrivateKey] - Fournir une clé privée encodée Base6.");
        return AppConstants.encodeBase64String(pRsaPrivateKey.getEncoded());
    }

    /**
     * Construire une clé privée (RSA) à partir de la chaîne donnée.
     * 
     * @param pKey la chaîne contenant la clé.
     * @return la clé privée RSA.
     */
    public static RSAPrivateKey getPrivateKeyFromString(final String pKey)
    {
        LOGGER.info("[getPrivateKeyFromString] - Construire la clé privée à partir d'une chaîne.");

        String privateKeyContent = pKey;
        RSAPrivateKey rsaPrivateKey = null;

        try
        {
            // Supprimer la ligne d'en-tête de de fin de la clé privée s'il y en a.
            if (StringUtils.contains(privateKeyContent, AppConstants.PRIVATE_KEY_BEGIN) || StringUtils.contains(privateKeyContent, AppConstants.PRIVATE_KEY_END))
            {
                privateKeyContent = StringUtils.replace(privateKeyContent, AppConstants.PRIVATE_KEY_BEGIN, StringUtils.EMPTY);
                privateKeyContent = StringUtils.replace(privateKeyContent, AppConstants.PRIVATE_KEY_END, StringUtils.EMPTY);
            }

            // decode Base64 du contenu
            final byte[] keyBytes = AppConstants.decodeBase64(privateKeyContent);
            final var kf = KeyFactory.getInstance(AppConstants.RSA_ALGORITHM_KEY);
            final var keySpecPKCS8 = new PKCS8EncodedKeySpec(keyBytes);
            rsaPrivateKey = (RSAPrivateKey) kf.generatePrivate(keySpecPKCS8);
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException e)
        {
            throw new CustomAppException(e);
        }
        return rsaPrivateKey;
    }

    /**
     * Construire la clé privée à partir du fichier fourni en entrée.
     * 
     * @param pFilename le nom ou emplacement du fichier.
     * @return la clé privée RSA.
     */
    public static RSAPrivateKey getPrivateKeyFromFile(final String pFilename)
    {
        LOGGER.info("[getPrivateKeyFromFile] - Construire la clé privée à partir d'un fichier.");

        final String privateKeyStr = getKeyFromFile(pFilename);
        return getPrivateKeyFromString(privateKeyStr);
    }

    /**
     * Récupérer la clé publique de la paire de clés privée/publique.
     * 
     * @param pKeyPair la paire clé privée/publique.
     * @return la clé publique.
     */
    public static PublicKey getPublicKey(final KeyPair pKeyPair)
    {
        LOGGER.info("[getPublicKey] - Récupérer une clé publique de la paire clés privée/publique.");
        return pKeyPair.getPublic();
    }

    /**
     * Construire une clé publique encodée Base64.
     * 
     * @param pKeyPair la paire cle privée/publique.
     * @return la clé publique RSA encodée Base64.
     */
    public static String rsaPublicKey(final RSAPublicKey pRsaPublicKey)
    {
        LOGGER.info("[rsaPublicKey] - Construire la clé publique encodée Base64 de la paire clés privée/publique.");
        return AppConstants.encodeBase64String(pRsaPublicKey.getEncoded());
    }

    /**
     * Construire une clé publique (RSA) à partir de la chaîne donnée.
     * 
     * @param pKey la chaîne contenant la clé.
     * @return la clé publique RSA.
     */
    public static RSAPublicKey getPublicKeyFromString(final String pKey)
    {
        LOGGER.info("[getPublicKeyFromString] - Construire la clé publique à partir d'une chaîne.");

        String publicKeyContent = pKey;
        RSAPublicKey rsaPublicKey = null;

        try
        {
            // Supprimer la ligne d'en-tête de de fin de la clé privée s'il y en a.
            if (StringUtils.contains(publicKeyContent, AppConstants.PUBLIC_KEY_BEGIN) || StringUtils.contains(publicKeyContent, AppConstants.PUBLIC_KEY_END))
            {
                publicKeyContent = StringUtils.replace(publicKeyContent, AppConstants.PUBLIC_KEY_BEGIN, StringUtils.EMPTY);
                publicKeyContent = StringUtils.replace(publicKeyContent, AppConstants.PUBLIC_KEY_END, StringUtils.EMPTY);
            }

            // decode Base64 du contenu
            final byte[] keyBytes = AppConstants.decodeBase64(publicKeyContent);

            final var kf = KeyFactory.getInstance(AppConstants.RSA_ALGORITHM_KEY);
            final var keySpecX509 = new X509EncodedKeySpec(keyBytes);
            rsaPublicKey = (RSAPublicKey) kf.generatePublic(keySpecX509);
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException e)
        {
            throw new CustomAppException(e);
        }
        return rsaPublicKey;
    }

    /**
     * Construire la clé publique à partir du fichier fourni en entrée.
     * 
     * @param pFilename le nom ou emplacement du fichier.
     * @return la clé publique RSA.
     */
    public static RSAPublicKey getPublicKeyFromFile(final String pFilename)
    {
        LOGGER.info("[getPublicKeyFromFile] - Construire la clé publique à partir d'un fichier.");

        final String privateKeyStr = getKeyFromFile(pFilename);
        return getPublicKeyFromString(privateKeyStr);
    }

    /**
     * Ecrire le flux encodée Base64 dans un fichier.
     * 
     * @param pEncryptedText la chaîne cryptée à écritre dans le fichier.
     * @param pFilePath      emplacement ou chemin du fichier en sorite.
     */
    public static void writeToFile(final String pEncryptedText, final String pFilePath)
    {
        LOGGER.info("[writeToFile]- Ecrire dans le fichier - Emplacement : [{}]", pFilePath);

        final Path fichier = Paths.get(pFilePath);
        traiterFichier(fichier);

        //
        try (final OutputStream outputStream = Files.newOutputStream(fichier); final BufferedOutputStream writer = new BufferedOutputStream(outputStream);)
        {
            //
            final byte[] keyBytes = pEncryptedText.getBytes();
            writer.write(keyBytes);
            writer.flush();
        }
        catch (Exception e)
        {
            throw new CustomAppException(e);
        }
    }

    /**
     * @param fichier
     */
    private static void traiterFichier(final Path fichier)
    {
        fichier.toFile().setWritable(true);
    }

    /**
     * Crypter en Base64 une chaine de caracteres avec algorithme asymétrique RSA.
     * 
     * @param pStringToBeEncrypted la chaîne à crypter et encoder.
     * @param pCryptoModel         le modèle de données des paramètres RSA pour le chiffrement.
     * @return la chaîne cryptée et encodée en Base64.
     */
    public static String encrypt(final String pStringToBeEncrypted, final CryptoModel pCryptoModel)
    {
        return CryptoProviderUtil.encrypt(pStringToBeEncrypted, pCryptoModel);
    }

    /**
     * Decrypter une chaine encodée Base64 avec algorithme asymétrique RSA.
     * 
     * @param pEncryptedText la chaîne à décrypter.
     * @param pCryptoModel   le modèle de données des paramètres RSA di déchiffrement.
     * @return la chaîne décryptée.
     */
    public static String decrypt(final String pEncryptedText, final CryptoModel pCryptoModel)
    {
        return CryptoProviderUtil.decrypt(pEncryptedText, pCryptoModel);
    }

    /**
     * Construire avec la clé privée RSA la signature d'un message ou une chaîne de caractères.
     * 
     * @param pPlainText   le message à signer.
     * @param pCryptoModel le modèle de données des paramètres RSA pour la signature.
     * @return le message signé encodé Base64 avec empreinte de la signature.
     */
    public static String sign(final String pPlainText, final CryptoModel pCryptoModel)
    {
        LOGGER.info("[sign] - Signer une chaîne avec la clé privée RSA.");

        String signedText = null;
        try
        {
            final var signatureInstance = pCryptoModel.getSignatureInstance();
            final var privateKeyStr = pCryptoModel.getPrivateKey();
            final var encoding = pCryptoModel.getEncoding();

            final var rsaPrivateKey = getPrivateKeyFromString(privateKeyStr);

            final var privateSignature = Signature.getInstance(signatureInstance);
            privateSignature.initSign(rsaPrivateKey);
            privateSignature.update(pPlainText.getBytes(encoding));
            signedText = AppConstants.encodeBase64String(privateSignature.sign());
        }
        catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException | UnsupportedEncodingException e)
        {
            throw new CustomAppException(e);
        }
        return signedText;
    }

    /**
     * Vérifier/Valider avec la clé publique la signature d'une chaîne signée.
     * 
     * @param pPlainText   le message signé à vérifier.
     * @param pSignature   la signature du message encodée Base64.
     * @param pCryptoModel le modèle de données des paramètres RSA pour la signature.
     * @return true si valide flase sinon.
     */
    public static Boolean verify(final String pPlainText, final String pSignature, final CryptoModel pCryptoModel)
    {
        LOGGER.info("[verify] - Vérifier/Valider la signature d'une chaîne signée.");

        Boolean isValidSignature = null;

        try
        {
            final var signatureInstance = pCryptoModel.getSignatureInstance();
            final var publicKeyStr = pCryptoModel.getPublicKey();
            final var encoding = pCryptoModel.getEncoding();
            final var publicKey = getPublicKeyFromString(publicKeyStr);

            final var publicSignature = Signature.getInstance(signatureInstance);
            publicSignature.initVerify(publicKey);
            publicSignature.update(pPlainText.getBytes(encoding));

            // Decode Base64 de la signature
            final byte[] signatureBytes = AppConstants.decodeBase64(pSignature);
            isValidSignature = publicSignature.verify(signatureBytes);
        }
        catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException | UnsupportedEncodingException e)
        {
            throw new CustomAppException(e);
        }
        return isValidSignature;
    }
}
