/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : GenericApiResponse.java
 * Date de création : 5 déc. 2020
 * Heure de création : 09:45:29
 * Package : fr.vincent.tuto.common.model.payload
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.model.payload;

import com.fasterxml.jackson.annotation.JsonInclude;

import fr.vincent.tuto.common.model.error.ApiResponseError;

/**
 * Classe d'implémentation de la réponse générique des endpoints pour les API.
 * 
 * @author Vincent Otchoun
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenericApiResponse<T>
{
    //
    private T data;
    private ApiResponseError errors;

    /**
     * Constructeur par défaut.
     */
    public GenericApiResponse()
    {
        //
    }

    public T getData()
    {
        return this.data;
    }

    public ApiResponseError getErrors()
    {
        return this.errors;
    }

    public void setData(T data)
    {
        this.data = data;
    }

    public void setErrors(ApiResponseError errors)
    {
        this.errors = errors;
    }
}
