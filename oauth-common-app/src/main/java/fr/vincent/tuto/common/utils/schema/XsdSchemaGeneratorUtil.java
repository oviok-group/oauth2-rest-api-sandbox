/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : XsdSchemaGeneratorUtil.java
 * Date de création : 19 nov. 2020
 * Heure de création : 18:43:34
 * Package : fr.vincent.tuto.common.utils
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.utils.schema;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.vincent.tuto.common.exception.CustomAppException;

/**
 * Classe Utilitaire de génération de schémas XSD.
 * 
 * @author Vincent Otchoun
 */
public final class XsdSchemaGeneratorUtil
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(XsdSchemaGeneratorUtil.class);

    /**
     * Constructeur privé de l'utilitaire.
     */
    private XsdSchemaGeneratorUtil()
    {
    }

    /**
     * Générer le schéma XSD de l'objet passé en paramètre et le déposer à l'endroit spécifié.
     * 
     * @param <T>               le type de l'objet.
     * @param pClassesToBeBound l'objet dont le schéma est à générer.
     * @param pFileName         emplacement de dépôt du schéma généré.
     */
    public static <T> void xsdSchemaGeneration(final Class<T> pClassesToBeBound, final String pFileName)
    {
        if (pClassesToBeBound != null)
        {
            LOGGER.info("[xsdSchemaGeneration] - Génération du schéma XSD de l'objet : [{}]", pClassesToBeBound);
        }
        else
        {
            LOGGER.info("[xsdSchemaGeneration] - Génération du schéma XSD");
        }

        //
        try
        {
            //
            final File fichier = Paths.get(pFileName).toFile();
            fichier.setWritable(true);

            //
            final var context = JAXBContext.newInstance(pClassesToBeBound);
            context.generateSchema(new SchemaOutputResolver()
            {
                @Override
                public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException
                {
                    final var streamResult = new StreamResult(fichier);
                    // streamResult.setSystemId(file.getName()); // Génère et dépose le fichier à la racine du projet
                    streamResult.setSystemId(fichier); // Génère et dépose le fichier à l'emplacement spécifié.
                    return streamResult;
                }
            });
        }
        catch (Exception e)
        {
            throw new CustomAppException(e.getMessage());
        }
    }
}
