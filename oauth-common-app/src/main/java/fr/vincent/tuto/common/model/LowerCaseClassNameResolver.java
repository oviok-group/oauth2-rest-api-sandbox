/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : LowerCaseClassNameResolver.java
 * Date de création : 5 déc. 2020
 * Heure de création : 10:01:54
 * Package : fr.vincent.tuto.common.model
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.databind.jsontype.impl.TypeIdResolverBase;

/**
 * Classe d'implémenatation de {@link TypeIdResolverBase} pour définir une gestion de type entièrement personnalisée.
 * 
 * @author Vincent Otchoun
 */
public class LowerCaseClassNameResolver extends TypeIdResolverBase
{

    @Override
    public String idFromValue(Object value)
    {
        return value.getClass().getSimpleName().toLowerCase();
    }

    @Override
    public String idFromValueAndType(Object value, Class<?> suggestedType)
    {
        return idFromValue(value);
    }

    @Override
    public Id getMechanism()
    {
        return JsonTypeInfo.Id.CUSTOM;
    }

}
