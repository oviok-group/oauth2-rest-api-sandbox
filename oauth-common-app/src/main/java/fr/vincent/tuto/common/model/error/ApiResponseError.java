/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : ApiResponseError.java
 * Date de création : 5 déc. 2020
 * Heure de création : 05:59:20
 * Package : fr.vincent.tuto.common.model.error
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.model.error;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.google.common.collect.Lists;

import fr.vincent.tuto.common.constants.AppConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Objet de mapping de la composante de gestion des erreurs dans les retours des API. Il sera intégré à l'objet de
 * gestion des retours.
 * 
 * @author Vincent Otchoun
 */
@JsonPropertyOrder({ "status", "timestamp", "details", "debugMessage", "validationErrors" })
@ApiModel(description = "Encapsulation des erreurs retour API")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponseError implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 4175516611213540591L;

    @JsonProperty(value = "status")
    @JsonPropertyDescription("Le code retour HTTP et descriptif associé")
    @ApiModelProperty(name = "status", dataType = "org.springframework.http.HttpStatus", value = "Le code retour HTTP et descriptif associé", position = 0)
    private HttpStatus status;

    @JsonProperty(value = "timestamp")
    @JsonPropertyDescription("Horodatage de constatation de l'erreur")
    @ApiModelProperty(name = "timestamp", dataType = "java.time.LocalDateTime", value = "Horodatage de constatation de l'erreur", position = 1)
    @JsonSerialize(using = ToStringSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = AppConstants.CET_DATE_FORMAT_WITHOUT_TIMEZONE_TEXT, locale = AppConstants.FR_LOCALE, timezone = AppConstants.CET_TIMEZONE)
    private LocalDateTime timestamp;

    @JsonProperty(value = "details")
    @JsonPropertyDescription("Le message d'erreur")
    @ApiModelProperty(name = "details", dataType = "java.lang.String", value = "Le message d'erreur", position = 2)
    private String details;

    @JsonProperty(value = "debugMessage")
    @JsonPropertyDescription("Le message d'erreur localisé")
    @ApiModelProperty(name = "debugMessage", dataType = "java.lang.String", value = "Le message localisé", position = 3)
    private String debugMessage;

    @JsonProperty(value = "validationErrors")
    @JsonPropertyDescription("Erreurs de validation")
    @ApiModelProperty(name = "validationErrors", dataType = "java.lang.String", value = "Erreurs de validation", position = 4)
    private List<ApiValidationError> validationErrors;

    /**
     * Constructeur par défaut.
     */
    public ApiResponseError()
    {
        //
    }

    public ApiResponseError status(final HttpStatus pHttpStatus)
    {
        this.status = pHttpStatus;
        return this;
    }

    public ApiResponseError timestamp(final LocalDateTime pLocalDateTime)
    {
        this.timestamp = pLocalDateTime;
        return this;
    }

    public ApiResponseError details(final String pDetails)
    {
        this.details = pDetails;
        return this;
    }

    public ApiResponseError debugMessage(final String pDebugMessage)
    {
        this.debugMessage = pDebugMessage;
        return this;
    }

    public ApiResponseError validationErrors(final List<ApiValidationError> validationErrors)
    {
        if (validationErrors == null) {
            this.validationErrors = Lists.newArrayList();
        }
        else {
            this.validationErrors = validationErrors;
        }
        return this;
    }
    /////////////////////////////////
    ////
    ////////////////////////////

    public void addValidationErrorsFieldErros(List<FieldError> fieldErrors)
    {
        fieldErrors.forEach(this::addValidationErrorWithFieldError);
    }

    public void addValidationErrorGlablError(List<ObjectError> globalErrors)
    {
        globalErrors.forEach(this::addValidationErrorWithObjectError);
    }

    public void addValidationErrorsCV(Set<ConstraintViolation<?>> constraintViolations)
    {
        constraintViolations.forEach(this::addValidationErrorWithCV);
    }

    //////////////////
    /// ACCESSEURS
    //////////////////

    public HttpStatus getStatus()
    {
        return this.status;
    }

    public LocalDateTime getTimestamp()
    {
        return this.timestamp;
    }

    public String getDetails()
    {
        return this.details;
    }

    public List<ApiValidationError> getValidationErrors()
    {
        return this.validationErrors;
    }

    public String getDebugMessage()
    {
        return this.debugMessage;
    }

    public void setStatus(HttpStatus status)
    {
        this.status = status;
    }

    public void setTimestamp(LocalDateTime timestamp)
    {
        this.timestamp = timestamp;
    }

    public void setDetails(String details)
    {
        this.details = details;
    }

    public void setValidationErrors(List<ApiValidationError> validationErrors)
    {
        this.validationErrors = validationErrors;
    }

    public void setDebugMessage(String debugMessage)
    {
        this.debugMessage = debugMessage;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(java.lang.Object o)
    {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ApiResponseError apiResponseError = (ApiResponseError) o;
        return Objects.equals(this.status, apiResponseError.status)//
        && Objects.equals(this.timestamp, apiResponseError.timestamp);//
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(this.status, this.timestamp);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    ////////////////////////////////
    ///// METHODES PRIVEES
    ////////////////////////////////

    private void addValidationErrorWithCV(ConstraintViolation<?> cv)
    {
        this.addValidationError(cv.getRootBeanClass().getSimpleName(), ((PathImpl) cv.getPropertyPath()).getLeafNode().asString(), cv
        .getInvalidValue(), cv.getMessage());
    }

    private void addValidationErrorWithObjectError(ObjectError objectError)
    {
        this.addValidationError(objectError.getObjectName(), objectError.getDefaultMessage());
    }

    private void addValidationErrorWithFieldError(FieldError fieldError)
    {
        this.addValidationError(fieldError.getObjectName(), fieldError.getField(), fieldError.getRejectedValue(), fieldError.getDefaultMessage());
    }

    private void addValidationError(String object, String message)
    {
        this.addSubError(this.buildApiValidationError(object, null, null, message));
    }

    private void addValidationError(String object, String field, Object rejectedValue, String message)
    {
        this.addSubError(this.buildApiValidationError(object, field, rejectedValue, message));
    }

    private void addSubError(ApiValidationError validationError)
    {
        if (this.validationErrors == null) {
            this.validationErrors = new ArrayList<>();
        }
        this.validationErrors.add(validationError);
    }

    private ApiValidationError buildApiValidationError(String object, String field, Object rejectedValue, String message)
    {
        final ApiValidationError validationError = new ApiValidationError();

        validationError.setObject(object);
        validationError.setMessage(message);

        if (StringUtils.isNotBlank(field)) {
            validationError.setField(field);
        }

        if (rejectedValue != null) {
            validationError.setRejectedValue(rejectedValue);
        }

        return validationError;
    }
}
