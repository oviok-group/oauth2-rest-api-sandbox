/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : BaseRolesEnum.java
 * Date de création : 24 nov. 2020
 * Heure de création : 19:36:13
 * Package : fr.vincent.tuto.common.enumeration
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.enumeration;

import org.springframework.security.core.GrantedAuthority;

import fr.vincent.tuto.common.constants.AppConstants;

/**
 * Enumeration de base pour la défintion des rôles (Authority) des utilisateurs de l'application.
 * 
 * @author Vincent Otchoun
 */
public enum BaseRolesEnum implements GrantedAuthority
{
    ADMIN, CLIENT, ANONYMOUS, USER;

    @Override
    public String getAuthority()
    {
        return AppConstants.ROLE_PREFIXE + name();
    }
}
