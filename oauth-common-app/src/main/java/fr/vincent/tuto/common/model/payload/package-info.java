/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 5 déc. 2020
 * Heure de création : 09:44:10
 * Package : fr.vincent.tuto.common.model.payload
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Objets génériques de défintions des Request et Response des API.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.model.payload;
