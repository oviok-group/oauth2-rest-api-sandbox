/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : ApplicationPropsService.java
 * Date de création : 19 nov. 2020
 * Heure de création : 12:12:55
 * Package : fr.vincent.tuto.common.service.props
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.service.props;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

/**
 * Service de gestion des propriétés applicatives externalisées hormis celles d'accès à la base de données qui est
 * fourni par {@link DatabasePropsService}
 * 
 * @author Vincent Otchoun
 */
@Service
@ConfigurationProperties(prefix = "vot", ignoreInvalidFields = false, ignoreUnknownFields = true)
public class ApplicationPropsService
{
    //
    private final SwaggerProps swaggerProps = new SwaggerProps();
    private final JwtProps jwtProps = new JwtProps();
    private final OauthProps oauthProps = new OauthProps();
    private final CorsProps corsProps = new CorsProps();
    private final MailProps mailProps = new MailProps();
    private final AsyncProps asyncProps = new AsyncProps();
    private final CryptoProps cryptoProps = new CryptoProps();
    private final MessageProps messageProps = new MessageProps();
    private final EhcacheProps ehcacheProps = new EhcacheProps();

    public SwaggerProps getSwaggerProps()
    {
        return this.swaggerProps;
    }

    public JwtProps getJwtProps()
    {
        return this.jwtProps;
    }

    public OauthProps getOauthProps()
    {
        return this.oauthProps;
    }

    public CorsProps getCorsProps()
    {
        return this.corsProps;
    }

    public MailProps getMailProps()
    {
        return this.mailProps;
    }

    public AsyncProps getAsyncProps()
    {
        return this.asyncProps;
    }

    public CryptoProps getCryptoProps()
    {
        return this.cryptoProps;
    }

    public MessageProps getMessageProps()
    {
        return this.messageProps;
    }

    public EhcacheProps getEhcacheProps()
    {
        return this.ehcacheProps;
    }

    /**
     * Gestion des propriétés de génération de la documentation Swagger et contexte racine.
     * 
     * @author Vincent Otchoun
     */
    public static class SwaggerProps
    {
        private String title;
        private String description;
        private String version;
        private String termeOfServiceUrl;
        private String contactName;
        private String contactUrl;
        private String contactEmail;
        private String licence;
        private String licenceUrl;
        private String defaultIncludePattern;
        private String host;
        private List<String> protocols = new ArrayList<>();
        private Boolean useDefaultResponseMessages;
        private Boolean codeGeneration;
        private String appBasePackage;
        private String urlPaths;

        // Swagger root context properties
        private String nomMachineDev;
        private String contexteRacineDefault;
        private String contexteRacine;

        public String getTitle()
        {
            return this.title;
        }

        public String getDescription()
        {
            return this.description;
        }

        public String getVersion()
        {
            return this.version;
        }

        public String getTermeOfServiceUrl()
        {
            return this.termeOfServiceUrl;
        }

        public String getContactName()
        {
            return this.contactName;
        }

        public String getContactUrl()
        {
            return this.contactUrl;
        }

        public String getContactEmail()
        {
            return this.contactEmail;
        }

        public String getLicence()
        {
            return this.licence;
        }

        public String getLicenceUrl()
        {
            return this.licenceUrl;
        }

        public String getDefaultIncludePattern()
        {
            return this.defaultIncludePattern;
        }

        public String getHost()
        {
            return this.host;
        }

        public List<String> getProtocols()
        {
            return this.protocols;
        }

        public Boolean getUseDefaultResponseMessages()
        {
            return this.useDefaultResponseMessages;
        }

        public Boolean getCodeGeneration()
        {
            return this.codeGeneration;
        }

        public String getAppBasePackage()
        {
            return this.appBasePackage;
        }

        public String getUrlPaths()
        {
            return this.urlPaths;
        }

        public String getContexteRacine()
        {
            return this.contexteRacine;
        }

        public String getNomMachineDev()
        {
            return this.nomMachineDev;
        }

        public String getContexteRacineDefault()
        {
            return this.contexteRacineDefault;
        }

        public void setTitle(final String pTitle)
        {
            this.title = pTitle;
        }

        public void setDescription(final String pDescription)
        {
            this.description = pDescription;
        }

        public void setVersion(final String pVersion)
        {
            this.version = pVersion;
        }

        public void setTermeOfServiceUrl(final String pTermeOfServiceUrl)
        {
            this.termeOfServiceUrl = pTermeOfServiceUrl;
        }

        public void setContactName(final String pContactName)
        {
            this.contactName = pContactName;
        }

        public void setContactUrl(final String pContactUrl)
        {
            this.contactUrl = pContactUrl;
        }

        public void setContactEmail(final String pContactEmail)
        {
            this.contactEmail = pContactEmail;
        }

        public void setLicence(final String pLicence)
        {
            this.licence = pLicence;
        }

        public void setLicenceUrl(final String pLicenceUrl)
        {
            this.licenceUrl = pLicenceUrl;
        }

        public void setDefaultIncludePattern(final String pDefaultIncludePattern)
        {
            this.defaultIncludePattern = pDefaultIncludePattern;
        }

        public void setHost(final String pHost)
        {
            this.host = pHost;
        }

        public void setProtocols(final List<String> pProtocols)
        {
            this.protocols = pProtocols;
        }

        public void setUseDefaultResponseMessages(Boolean pUseDefaultResponseMessages)
        {
            this.useDefaultResponseMessages = pUseDefaultResponseMessages;
        }

        public void setCodeGeneration(Boolean pCodeGeneration)
        {
            this.codeGeneration = pCodeGeneration;
        }

        public void setAppBasePackage(final String pAppBasePackage)
        {
            this.appBasePackage = pAppBasePackage;
        }

        public void setNomMachineDev(final String pNomMachineDev)
        {
            this.nomMachineDev = pNomMachineDev;
        }

        public void setContexteRacineDefault(final String pContexteRacineDefault)
        {
            this.contexteRacineDefault = pContexteRacineDefault;
        }

        public void setContexteRacine(final String pContexteRacine)
        {
            this.contexteRacine = pContexteRacine;
        }

        public void setUrlPaths(final String pUrlPaths)
        {
            this.urlPaths = pUrlPaths;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    /**
     * Gestion authentification et autorisation avec JWT
     * 
     * @author Vincent Otchoun
     */
    public static class JwtProps
    {
        private String tokenSecret;
        private String base64TokenSecret;
        private Long tokenValidity;
        private Long tokenValidityForRememberMe;
        private Long refreshTokenValidity;
        private String authorizationHeader;
        private String authoritiesKey;
        private String bearerToken;

        public String getTokenSecret()
        {
            return this.tokenSecret;
        }

        public String getBase64TokenSecret()
        {
            return this.base64TokenSecret;
        }

        public Long getTokenValidity()
        {
            return this.tokenValidity;
        }

        public Long getTokenValidityForRememberMe()
        {
            return this.tokenValidityForRememberMe;
        }

        public Long getRefreshTokenValidity()
        {
            return this.refreshTokenValidity;
        }

        public String getAuthorizationHeader()
        {
            return this.authorizationHeader;
        }

        public String getAuthoritiesKey()
        {
            return this.authoritiesKey;
        }

        public String getBearerToken()
        {
            return this.bearerToken;
        }

        public void setTokenSecret(final String pTokenSecret)
        {
            this.tokenSecret = pTokenSecret;
        }

        public void setBase64TokenSecret(final String pBase64TokenSecret)
        {
            this.base64TokenSecret = pBase64TokenSecret;
        }

        public void setTokenValidity(final Long pTokenValidity)
        {
            this.tokenValidity = pTokenValidity;
        }

        public void setTokenValidityForRememberMe(final Long pTokenValidityForRememberMe)
        {
            this.tokenValidityForRememberMe = pTokenValidityForRememberMe;
        }

        public void setRefreshTokenValidity(final Long pRefreshTokenValidity)
        {
            this.refreshTokenValidity = pRefreshTokenValidity;
        }

        public void setAuthorizationHeader(final String pAuthorizationHeader)
        {
            this.authorizationHeader = pAuthorizationHeader;
        }

        public void setAuthoritiesKey(final String pAuthoritiesKey)
        {
            this.authoritiesKey = pAuthoritiesKey;
        }

        public void setBearerToken(final String pBearerToken)
        {
            this.bearerToken = pBearerToken;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    /**
     * Gestion Authentification et Autorisation OAuth2
     * 
     * @author Vincent Otchoun
     */
    public static class OauthProps
    {
        private String clientId;
        private String clientSecret;
        private Integer accessTokenValidity;
        private Integer refreshTokenValidity;
        private String authorizedGrantTypes;
        private String privateKey;
        private String publicKey;
        private String accessTokenUri;
        private String userAuthorizationUri;
        private String resourceUri;

        public String getClientId()
        {
            return this.clientId;
        }

        public String getClientSecret()
        {
            return this.clientSecret;
        }

        public Integer getAccessTokenValidity()
        {
            return this.accessTokenValidity;
        }

        public Integer getRefreshTokenValidity()
        {
            return this.refreshTokenValidity;
        }

        public String getAuthorizedGrantTypes()
        {
            return this.authorizedGrantTypes;
        }

        public String getPrivateKey()
        {
            return this.privateKey;
        }

        public String getPublicKey()
        {
            return this.publicKey;
        }

        public String getAccessTokenUri()
        {
            return this.accessTokenUri;
        }

        public String getUserAuthorizationUri()
        {
            return this.userAuthorizationUri;
        }

        public String getResourceUri()
        {
            return this.resourceUri;
        }

        public void setClientId(final String pClientId)
        {
            this.clientId = pClientId;
        }

        public void setClientSecret(final String pClientSecret)
        {
            this.clientSecret = pClientSecret;
        }

        public void setAccessTokenValidity(final Integer pAccessTokenValidity)
        {
            this.accessTokenValidity = pAccessTokenValidity;
        }

        public void setRefreshTokenValidity(final Integer pRefreshTokenValidity)
        {
            this.refreshTokenValidity = pRefreshTokenValidity;
        }

        public void setAuthorizedGrantTypes(final String pAuthorizedGrantTypes)
        {
            this.authorizedGrantTypes = pAuthorizedGrantTypes;
        }

        public void setPrivateKey(final String pPrivateKey)
        {
            this.privateKey = pPrivateKey;
        }

        public void setPublicKey(final String pPublicKey)
        {
            this.publicKey = pPublicKey;
        }

        public void setAccessTokenUri(final String pAccessTokenUri)
        {
            this.accessTokenUri = pAccessTokenUri;
        }

        public void setUserAuthorizationUri(final String pUserAuthorizationUri)
        {
            this.userAuthorizationUri = pUserAuthorizationUri;
        }

        public void setResourceUri(final String pResourceUri)
        {
            this.resourceUri = pResourceUri;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    /**
     * Gestion des propriétés CORS : Cross-origin resource sharing.
     * 
     * @author Vincent Otchoun
     */
    public static class CorsProps
    {
        private String origin;
        private Boolean credentials;
        private String methods;
        private Integer maxAge;
        private String headers;

        public String getOrigin()
        {
            return this.origin;
        }

        public Boolean getCredentials()
        {
            return this.credentials;
        }

        public String getMethods()
        {
            return this.methods;
        }

        public Integer getMaxAge()
        {
            return this.maxAge;
        }

        public String getHeaders()
        {
            return this.headers;
        }

        public void setOrigin(final String pOrigin)
        {
            this.origin = pOrigin;
        }

        public void setCredentials(final Boolean pCredentials)
        {
            this.credentials = pCredentials;
        }

        public void setMethods(final String pMethods)
        {
            this.methods = pMethods;
        }

        public void setMaxAge(final Integer pMaxAge)
        {
            this.maxAge = pMaxAge;
        }

        public void setHeaders(final String pHeaders)
        {
            this.headers = pHeaders;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    /**
     * Propriétés de gestion d'envoi de mails
     * 
     * @author Vincent Otchoun
     */
    public static class MailProps
    {
        private String host;
        private Integer port;
        private String username;
        private String password;
        private String protocol;
        private Boolean tls;
        private Boolean auth;
        private String from;
        private Boolean quitWait;

        public String getHost()
        {
            return this.host;
        }

        public Integer getPort()
        {
            return this.port;
        }

        public String getUsername()
        {
            return this.username;
        }

        public String getPassword()
        {
            return this.password;
        }

        public String getProtocol()
        {
            return this.protocol;
        }

        public Boolean getTls()
        {
            return this.tls;
        }

        public Boolean getAuth()
        {
            return this.auth;
        }

        public String getFrom()
        {
            return this.from;
        }

        public Boolean getQuitWait()
        {
            return this.quitWait;
        }

        public void setHost(final String pHost)
        {
            this.host = pHost;
        }

        public void setPort(final Integer pPort)
        {
            this.port = pPort;
        }

        public void setUsername(final String pUsername)
        {
            this.username = pUsername;
        }

        public void setPassword(final String pPassword)
        {
            this.password = pPassword;
        }

        public void setProtocol(final String pProtocol)
        {
            this.protocol = pProtocol;
        }

        public void setTls(final Boolean pTls)
        {
            this.tls = pTls;
        }

        public void setAuth(final Boolean pAuth)
        {
            this.auth = pAuth;
        }

        public void setFrom(final String pFrom)
        {
            this.from = pFrom;
        }

        public void setQuitWait(final Boolean pQuitWait)
        {
            this.quitWait = pQuitWait;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    /**
     * Gestion Asynchrone
     * 
     * @author Vincent Otchoun
     */
    public static class AsyncProps
    {
        private Integer corePoolSize;
        private Integer maxPoolSize;
        private Integer queueCapacity;

        public Integer getCorePoolSize()
        {
            return this.corePoolSize;
        }

        public Integer getMaxPoolSize()
        {
            return this.maxPoolSize;
        }

        public Integer getQueueCapacity()
        {
            return this.queueCapacity;
        }

        public void setCorePoolSize(final Integer pCorePoolSize)
        {
            this.corePoolSize = pCorePoolSize;
        }

        public void setMaxPoolSize(final Integer pMaxPoolSize)
        {
            this.maxPoolSize = pMaxPoolSize;
        }

        public void setQueueCapacity(final Integer pQueueCapacity)
        {
            this.queueCapacity = pQueueCapacity;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    /**
     * Gestion des propriétés de la norme de chiffrement symétrique AES.
     * 
     * @author Vincent Otchoun
     */
    public static class CryptoProps
    {
        // AES
        private String algorithm;
        private String instancePad;
        private String secureRandomInstance;
        private Integer bytesLength;
        private String keyValue;
        private String encoding;

        // RSA
        private String privateKey;
        private String publicKey;
        private String signatureInstance;
        private String publicKeyFileLocation; //

        // KeyStroe
        private String keystorePassword; // -storepass
        private String keyPassword; // -keypass
        private String aliasKeystore; // -alias
        private String keystoreFileLocation; //
        private String keyStoreType; // JKS ou PKCS12

        // Trust Store
        private String trustStorePath;
        private String trustStorePassword;
        private String trustStoreType; // JKS ou PKCS12
        private String httpsProtocols; // set to TLSv1.1 or TLSv1.2

        public String getAlgorithm()
        {
            return this.algorithm;
        }

        public String getInstancePad()
        {
            return this.instancePad;
        }

        public String getSecureRandomInstance()
        {
            return this.secureRandomInstance;
        }

        public Integer getBytesLength()
        {
            return this.bytesLength;
        }

        public String getKeyValue()
        {
            return this.keyValue;
        }

        public String getEncoding()
        {
            return this.encoding;
        }

        public String getPrivateKey()
        {
            return this.privateKey;
        }

        public String getPublicKey()
        {
            return this.publicKey;
        }

        public String getKeystorePassword()
        {
            return this.keystorePassword;
        }

        public String getKeyPassword()
        {
            return this.keyPassword;
        }

        public String getAliasKeystore()
        {
            return this.aliasKeystore;
        }

        public String getSignatureInstance()
        {
            return this.signatureInstance;
        }

        public String getKeystoreFileLocation()
        {
            return this.keystoreFileLocation;
        }

        public String getPublicKeyFileLocation()
        {
            return this.publicKeyFileLocation;
        }

        public String getKeyStoreType()
        {
            return this.keyStoreType;
        }

        public String getTrustStorePath()
        {
            return this.trustStorePath;
        }

        public String getTrustStorePassword()
        {
            return this.trustStorePassword;
        }

        public String getTrustStoreType()
        {
            return this.trustStoreType;
        }

        public String getHttpsProtocols()
        {
            return this.httpsProtocols;
        }

        public void setAlgorithm(final String pAlgorithm)
        {
            this.algorithm = pAlgorithm;
        }

        public void setInstancePad(final String pInstancePad)
        {
            this.instancePad = pInstancePad;
        }

        public void setSecureRandomInstance(final String pSecureRandomInstance)
        {
            this.secureRandomInstance = pSecureRandomInstance;
        }

        public void setBytesLength(final Integer pBytesLength)
        {
            this.bytesLength = pBytesLength;
        }

        public void setKeyValue(final String pKeyValue)
        {
            this.keyValue = pKeyValue;
        }

        public void setEncoding(final String pEncoding)
        {
            this.encoding = pEncoding;
        }

        public void setPrivateKey(final String pPrivateKey)
        {
            this.privateKey = pPrivateKey;
        }

        public void setPublicKey(final String pPublicKey)
        {
            this.publicKey = pPublicKey;
        }

        public void setKeystorePassword(final String pKeystorePassword)
        {
            this.keystorePassword = pKeystorePassword;
        }

        public void setKeyPassword(final String pKeyPassword)
        {
            this.keyPassword = pKeyPassword;
        }

        public void setAliasKeystore(final String pAliasKeystore)
        {
            this.aliasKeystore = pAliasKeystore;
        }

        public void setSignatureInstance(final String pSignatureInstance)
        {
            this.signatureInstance = pSignatureInstance;
        }

        public void setKeystoreFileLocation(final String pKeystoreFileLocation)
        {
            this.keystoreFileLocation = pKeystoreFileLocation;
        }

        public void setPublicKeyFileLocation(final String pPublicKeyFileLocation)
        {
            this.publicKeyFileLocation = pPublicKeyFileLocation;
        }

        public void setKeyStoreType(final String pKeyStoreType)
        {
            this.keyStoreType = pKeyStoreType;
        }

        /**
         * @param trustStorePath the trustStorePath to set
         */
        public void setTrustStorePath(final String pTrustStorePath)
        {
            this.trustStorePath = pTrustStorePath;
        }

        public void setTrustStorePassword(final String pTrustStorePassword)
        {
            this.trustStorePassword = pTrustStorePassword;
        }

        public void setTrustStoreType(final String pTrustStoreType)
        {
            this.trustStoreType = pTrustStoreType;
        }
        

        public void setHttpsProtocols(final String pHttpsProtocols)
        {
            this.httpsProtocols = pHttpsProtocols;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    /**
     * Proprités pour l'optimisation des accès aux données pour une meilleure perfomance.
     * 
     * @author Vincent Otchoun
     */
    public static class EhcacheProps
    {
        private Integer timeToLiveSeconds;
        private String maxBytesLocalHeap;
        private Long maxEntries;

        public Integer getTimeToLiveSeconds()
        {
            return this.timeToLiveSeconds;
        }

        public String getMaxBytesLocalHeap()
        {
            return this.maxBytesLocalHeap;
        }

        public Long getMaxEntries()
        {
            return this.maxEntries;
        }

        public void setTimeToLiveSeconds(final Integer pTimeToLiveSeconds)
        {
            this.timeToLiveSeconds = pTimeToLiveSeconds;
        }

        public void setMaxBytesLocalHeap(final String pMaxBytesLocalHeap)
        {
            this.maxBytesLocalHeap = pMaxBytesLocalHeap;
        }

        public void setMaxEntries(final Long pMaxEntries)
        {
            this.maxEntries = pMaxEntries;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }

    /**
     * Propriétés des gestion des messages de logs pour le démarrage de l'application.
     * 
     * @author Vincent Otchoun
     */
    public static class MessageProps
    {
        private String appNameKey;
        private String securityKey;
        private String serverPortKey;
        private String contextPathKey;
        private String defaultProfileKey;
        private String protocol;
        private String appSecureProtocol;
        private String defaultContextPath;
        private String hostnameErrorMessage;
        private String startLogMessage;
        private String hostname;
        private String prodDevErrorMessage;
        private String cloudDevErrorMessage;

        public String getAppNameKey()
        {
            return this.appNameKey;
        }

        public String getSecurityKey()
        {
            return this.securityKey;
        }

        public String getServerPortKey()
        {
            return this.serverPortKey;
        }

        public String getContextPathKey()
        {
            return this.contextPathKey;
        }

        public String getDefaultProfileKey()
        {
            return this.defaultProfileKey;
        }

        public String getProtocol()
        {
            return this.protocol;
        }

        public String getAppSecureProtocol()
        {
            return this.appSecureProtocol;
        }

        public String getDefaultContextPath()
        {
            return this.defaultContextPath;
        }

        public String getHostnameErrorMessage()
        {
            return this.hostnameErrorMessage;
        }

        public String getStartLogMessage()
        {
            return this.startLogMessage;
        }

        public String getHostname()
        {
            return this.hostname;
        }

        public String getProdDevErrorMessage()
        {
            return this.prodDevErrorMessage;
        }

        public String getCloudDevErrorMessage()
        {
            return this.cloudDevErrorMessage;
        }

        public void setAppNameKey(final String pAppNameKey)
        {
            this.appNameKey = pAppNameKey;
        }

        public void setSecurityKey(final String pSecurityKey)
        {
            this.securityKey = pSecurityKey;
        }

        public void setServerPortKey(final String pServerPortKey)
        {
            this.serverPortKey = pServerPortKey;
        }

        public void setContextPathKey(final String pContextPathKey)
        {
            this.contextPathKey = pContextPathKey;
        }

        public void setDefaultProfileKey(final String pDefaultProfileKey)
        {
            this.defaultProfileKey = pDefaultProfileKey;
        }

        public void setProtocol(final String pProtocol)
        {
            this.protocol = pProtocol;
        }

        public void setAppSecureProtocol(final String pAppSecureProtocol)
        {
            this.appSecureProtocol = pAppSecureProtocol;
        }

        public void setDefaultContextPath(final String pDefaultContextPath)
        {
            this.defaultContextPath = pDefaultContextPath;
        }

        public void setHostnameErrorMessage(final String pHostnameErrorMessage)
        {
            this.hostnameErrorMessage = pHostnameErrorMessage;
        }

        public void setStartLogMessage(final String pStartLogMessage)
        {
            this.startLogMessage = pStartLogMessage;
        }

        public void setHostname(final String pHostname)
        {
            this.hostname = pHostname;
        }

        public void setProdDevErrorMessage(final String pProdDevErrorMessage)
        {
            this.prodDevErrorMessage = pProdDevErrorMessage;
        }

        public void setCloudDevErrorMessage(final String pCloudDevErrorMessage)
        {
            this.cloudDevErrorMessage = pCloudDevErrorMessage;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString()
        {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }
    }
}
