/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 22 nov. 2020
 * Heure de création : 06:48:01
 * Package : fr.vincent.tuto.common.model
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient le modèle de données communes.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.model;
