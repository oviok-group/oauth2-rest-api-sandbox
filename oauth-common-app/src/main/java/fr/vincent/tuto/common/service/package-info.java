/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 18 nov. 2020
 * Heure de création : 13:29:35
 * Package : fr.vincent.tuto.common.service
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Mutualise les services communs
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.service;
