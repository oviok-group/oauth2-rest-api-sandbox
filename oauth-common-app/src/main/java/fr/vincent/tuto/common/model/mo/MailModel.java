/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : MailModel.java
 * Date de création : 28 nov. 2020
 * Heure de création : 12:48:20
 * Package : fr.vincent.tuto.common.model.mo
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.model.mo;

import java.io.Serializable;
import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Model de données des éléments à fournir au service construction envoi du message.
 * 
 * @author Vincent Otchoun
 */
public class MailModel implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 2666758071913843358L;

    private String email; // l'adresse du destinataire
    private String subject; // le sujet du mail
    private String content; // le contenu du mail
    private boolean isMultipart; // true utilise le format mime en plusieur parties, false sinon
    private boolean isHtml; // true utilise le type mime 'text/html', false sinon ('text/pali')

    public MailModel email(final String pEmail)
    {
        this.email = pEmail;
        return this;
    }

    public MailModel subject(final String pSubject)
    {
        this.subject = pSubject;
        return this;
    }

    public MailModel content(final String pContent)
    {
        this.content = pContent;
        return this;
    }

    public MailModel isMultipart(boolean pMultipart)
    {
        this.isMultipart = pMultipart;
        return this;
    }

    public MailModel isHtml(boolean pHtml)
    {
        this.isHtml = pHtml;
        return this;
    }

    public String getEmail()
    {
        return this.email;
    }

    public String getSubject()
    {
        return this.subject;
    }

    public String getContent()
    {
        return this.content;
    }

    public boolean isMultipart()
    {
        return this.isMultipart;
    }

    public boolean isHtml()
    {
        return this.isHtml;
    }

    public void setEmail(final String pEmail)
    {
        this.email = pEmail;
    }

    public void setSubject(final String pSubject)
    {
        this.subject = pSubject;
    }

    public void setContent(final String pContent)
    {
        this.content = pContent;
    }


    public void setMultipart(boolean isMultipart)
    {
        this.isMultipart = isMultipart;
    }

    public void setHtml(boolean isHtml)
    {
        this.isHtml = isHtml;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(java.lang.Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        MailModel article = (MailModel) o;
        return Objects.equals(this.email, article.email);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(this.email);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
