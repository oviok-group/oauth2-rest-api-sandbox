/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : AppConstants.java
 * Date de création : 20 nov. 2020
 * Heure de création : 06:53:11
 * Package : fr.vincent.tuto.common.constants
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.constants;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Base64Utils;

/**
 * Utilitaire definissant les constantes et fonctons communes.
 * 
 * @author Vincent Otchoun
 */
public final class AppConstants
{
    // Erreur HTTP
    public static final String URL_ERROR = "Impossible de trouver la méthode '%s' pour l'URL '%s'.";
    public static final String METHOD_ERROR = "Le paramètre '%s' de la valeur '%s' n'a pas pu être converti en type '%s'";
    public static final String HTTP_CLIENT_ERROR = "Erreur client HTTP.";
    public static final String SERVER_INTERNAL_ERROR = "Erreur interne du serveur.";
    public static final String FORMAT_ERROR = "Erreur format pour lecture et écriture.";
    public static final String CONTRAINST_VALDATION_ERROR = "Erreur violation de contraintes.";
    public static final String NOT_FOUND_ERROR = "Erreur recherche infructueuse de données.";
    public static final String INTEGRITY_ERROR = "Erreur de violations d'intégrité des données.";
    public static final String ACCESS_DENIED = "Accès non autorisés.";

    // App profiles
    public static final String DEFAULT_PROFILE = "spring.profiles.default";
    public static final String DEVELOPMENT_PROFILE = "dev";
    public static final String TEST_PROFILE = "test";
    public static final String PRODUCTION_PROFILE = "prod";
    public static final String CLOUD_PROFILE = "cloud";
    public static final String SWAGGER_PROFILE = "swagger";
    public static final String K8S_PROFILE = "k8s";

    public static final String HTTP = "http";
    public static final String HTTPS = "https";
    public static final String SECURITY_KEY = "server.ssl.key-store";
    public static final String SERVER_PORT = "server.port";
    public static final String CONTEXT_PATH = "server.servlet.context-path";
    public static final String DEFAULT_CONTEXT_PATH = "/";
    public static final String HOST_ADRESS = "localhost";
    public static final String APP_NAME = "spring.application.name";
    public static final String DEV_MESSAGE_ERROR_KEY = "vot.message-props.prod-dev-error-message";
    public static final String CLOUD_MESSAGE_ERROR_KEY = "vot.message-props.cloud-dev-error-message";
    public static final String START_LOG_MESSAGE_ERROR_KEY = "vot.message-props.start-log-message";
    public static final String HOSTNAME_MESSAGE_ERROR_KEY = "vot.message-props.hostname-error-message";
    public static final String NOM_MACHINE_KEY = "vot.swagger-props.nom-machine-dev";

    public static final Pattern UUID_PATTERN = Pattern.compile("^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$");
    public static final String PASSWORD_PATTERN = "^.*(?=.{8,})(?=.*\\d)(?=.*[a-zA-Z]).*$";
    public static final String EMAIL_PATTERN = "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$";

    public static final String EMPTY_STR = StringUtils.EMPTY;
    public static final String SPACE_STR = StringUtils.SPACE;

    // MAIL
    public static final String SMTP_AUTH_KEY = "mail.smtp.auth";
    public static final String TLS_ENABLE_KEY = "mail.smtp.starttls.enable";
    public static final String QUIT_WAIT_KEY = "mail.smtp.quitwait";

    // Encoding
    public static final String UTF_8 = StandardCharsets.UTF_8.name();
    public static final String UTF_16 = StandardCharsets.UTF_16.name();
    public static final String ISO = StandardCharsets.ISO_8859_1.name();
    public static final Charset CHARSET_UTF_8 = StandardCharsets.UTF_8;

    // AES
    public static final String ALGORITHM_KEY = "ALGORITHM";
    public static final String INSTANCE_KEY = "INSTANCE_PAD";
    public static final String SECURE_RANDOM_KEY = "SECURE_RANDOM";
    public static final String LENGTH_KEY = "BYTES_LENGTH";
    public static final String KEY_VALUE = "KEY_VALUE";
    public static final String ENCODINGE_KEY = "ENCODING";
    public static final int AES_GEN_KEY_SIZE = 128;

    // RSA
    public static final int RSA_GEN_KEY_SIZE = 4096;
    public static final String RSA_ALGORITHM_KEY = "RSA";
    public static final String KEYSTORE_TYPE = "JKS";

    public static final String PRIVATE_KEY_BEGIN = "-----BEGIN PRIVATE KEY-----";
    public static final String PRIVATE_KEY_END = "-----END PRIVATE KEY-----";

    public static final String PUBLIC_KEY_BEGIN = "-----BEGIN PUBLIC KEY-----";
    public static final String PUBLIC_KEY_END = "-----END PUBLIC KEY-----";

    public static final String CSR_BEGIN = "-----BEGIN CERTIFICATE REQUEST-----";
    public static final String CSR_END = "-----END CERTIFICATE REQUEST-----";

    // User Rôles
    public static final String ROLE_PREFIXE = "ROLE_";

    // Files
    public static final String JSON_FILE_SUFFIXE = ".json";
    public static final String TEMP_FILE_SUFFIXE = ".temp";

    // Messages
    public static final String NOT_NULL_BEAN_MESSAGE = "l'objet doit être défini";
    public static final String NULL_OBJECT_MESSAGE = "l'objet défini est null";

    // Dates
    // MM/dd/yyyy’T’HH:mm:ss:SSS z pattern. e.g. "08/03/2019T16:20:17:717 UTC+05:30"
    // EEE MMM dd HH:mm:ss zzz yyyy --> Thu Mar 31 00:00:00 CEST 2016

    /*
     * yyyy-MM-dd (2009-12-31)
     * dd-MM-YYYY (31-12-2009)
     * yyyy-MM-dd HH:mm:ss (2009-12-31 23:59:59)
     * HH:mm:ss.SSS (23:59.59.999)
     * yyyy-MM-dd HH:mm:ss.SSS (2009-12-31 23:59:59.999)
     * yyyy-MM-dd HH:mm:ss.SSS Z (2009-12-31 23:59:59.999 +0100)
     */

    /*
     * y = year (yy or yyyy)
     * M = month (MM)
     * d = day in month (dd)
     * h = hour (0-12) (hh)
     * H = hour (0-23) (HH)
     * m = minute in hour (mm)
     * s = seconds (ss)
     * S = milliseconds (SSS)
     * z = time zone text (e.g. Pacific Standard Time...)
     * Z = time zone, time offset (e.g. -0800)
     */

    public static final String GEN_DATE_FORMAT_TIMEZONE_TEXT = "yyyy-MM-dd 'T'HH:mm:ss:SSS z";
    public static final String GEN_DATE_FORMAT_TIMEZONE_TIME_OFFSET = "yyyy-MM-dd 'T'HH:mm:ss:SSS Z";
    
    public static final String DATE_FORMAT_WITH_TIMEZONE_TEXT = "yyyy-MM-dd'T'HH:mm:ss.SSS z";
    public static final String DATE_FORMAT_WITH_TIMEZONE_TIME_OFFSET = "yyyy-MM-dd'T'HH:mm:ss.SSS Z";
    public static final String DATE_FORMAT_WITHOUT_TIMEZONE_TIME_OFFSET = "yyyy-MM-dd'T'HH:mm:ss.SSS";

    public static final String UTC_DATE_FORMAT_MAJ_Z = "yyyy-MM-dd 'T'HH:mm:ss:SSS Z";
    public static final String UTC_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss:SSS z";

    public static final String GMT_DATE_FORMAT_MAJ_Z = "yyyy-MM-dd 'T'HH:mm:ss:SSS Z";
    public static final String GMT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss:SSS z";
    // Europe/Paris : ECT /CET --> Central European Time
    public static final String CET_DATE_FORMAT_MAJ_Z = "yyyy-MM-dd 'T'HH:mm:ss.SSS Z"; //
    public static final String CET_DATE_FORMAT = "yyyy-MM-dd 'T'HH:mm:ss.SSS z";
    public static final String CET_DATE_FORMAT_WITHOUT_TIMEZONE_TEXT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public static final String CET_TIMEZONE = "Europe/Paris";
    public static final String FR_LOCALE = "fr-FR";
    public static final String US_LOCALE = "en-US";

    public static final String UTC_TIME_ZONE = "UTC";
    public static final String CET_TIME_ZONE = "CET"; //
    public static final String GMT_TIME_ZONE = "GMT";

    protected static final String[][] OLD_MAPPING = new String[][] { //
            { "ACT", "Australia/Darwin" }, //
            { "AET", "Australia/Sydney" }, //
            { "AGT", "America/Argentina/Buenos_Aires" },
            { "ART", "Africa/Cairo" },
            { "AST", "America/Anchorage" },
            { "BET", "America/Sao_Paulo" },
            { "BST", "Asia/Dhaka" },
            { "CAT", "Africa/Harare" },
            { "CNT", "America/St_Johns" },
            { "CST", "America/Chicago" },
            { "CTT", "Asia/Shanghai" },
            { "EAT", "Africa/Addis_Ababa" },
            { "ECT", CET_TIMEZONE },
            { "IET", "America/Indiana/Indianapolis" },
            { "IST", "Asia/Kolkata" },
            { "JST", "Asia/Tokyo" },
            { "MIT", "Pacific/Apia" },
            { "NET", "Asia/Yerevan" },
            { "NST", "Pacific/Auckland" },
            { "PLT", "Asia/Karachi" },
            { "PNT", "America/Phoenix" },
            { "PRT", "America/Puerto_Rico" },
            { "PST", "America/Los_Angeles" },
            { "SST", "Pacific/Guadalcanal" },
            { "VST", "Asia/Ho_Chi_Minh" }, };

    // Fonctions
    public static String encodeBase64String(final byte[] pByteToBeEncrpted)
    {
        return Base64Utils.encodeToString(pByteToBeEncrpted);
    }

    public static byte[] decodeBase64(final String pToBeDecrypted)
    {
        return Base64Utils.decodeFromString(pToBeDecrypted);
    }

    /**
     * Constructeur privé de l'utilitaire.
     */
    private AppConstants()
    {
    }
}
