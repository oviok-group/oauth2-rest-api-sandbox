/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 18 nov. 2020
 * Heure de création : 13:31:47
 * Package : fr.vincent.tuto.common.utils
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contien les utilitaires communs
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.utils;
