/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : GenericObjectMapper.java
 * Date de création : 25 nov. 2020
 * Heure de création : 16:26:51
 * Package : fr.vincent.tuto.common.mapper
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.mapper;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import fr.vincent.tuto.common.constants.AppConstants;

/**
 * Service générique d'abstraction de mappeur d'objets.
 * 
 * @author Vincent Otchoun
 * @param <S> objet source à traiter.
 * @param <D> objet destinataire à construire.
 */
@Component
public abstract class GenericObjectMapper<S, D> implements InitializingBean
{
    //
    protected final ModelMapper modelMapper;

    /**
     * Constructeur avec paramètres pour injection du beans en dépendances.
     * 
     * @param modelMapper  le bean de conversion des modèles selon le type.
     */
    @Autowired
    protected GenericObjectMapper(final ModelMapper pModelMapper)
    {
        this.modelMapper = pModelMapper;
    }

    /**
     * Obtenir un objet D à partir de l'objet source S.
     * 
     * @param pSourceObject objet source à traiter.
     * @return l'objet à construire.
     */
    public abstract D toDestObject(final S pSourceObject);

    /**
     * Obtenir l'objet source S à partir de l'objet final D construit.
     * 
     * @param pDestObject objet final à traiter pour reconstruire l'objet source.
     * @return objet source intial.
     */
    public abstract S toSourceObject(final D pDestObject);

    /**
     * Obtenir la liste d'objets finaux de la lsite des objets sources.
     * 
     * @param pSourceObjetList la iste d'objets sources.
     * @return la collection d'objets finaux.
     */
    public Collection<D> toDestObjectList(final Collection<S> pSourceObjetList)
    {
        //
        return Optional.ofNullable(pSourceObjetList)//
        .orElseGet(Collections::emptyList)//
        .stream()//
        .filter(Objects::nonNull)//
        .map(this::toDestObject)//
        .collect(Collectors.toList());
    }

    /**
     * Obtenir la liste d'objets source de la lsite des objets construits.
     * 
     * @param pDestbjetList la liste construits à traiter.
     * @return la lsite d'objets source.
     */
    public Collection<S> toSourceObjectList(final Collection<D> pDestbjetList)
    {
        //
        return Optional.ofNullable(pDestbjetList)//
        .orElseGet(Collections::emptyList)//
        .stream()//
        .filter(Objects::nonNull)//
        .map(this::toSourceObject)//
        .collect(Collectors.toList());
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        Assert.notNull(this.modelMapper, AppConstants.NOT_NULL_BEAN_MESSAGE);
    }
}
