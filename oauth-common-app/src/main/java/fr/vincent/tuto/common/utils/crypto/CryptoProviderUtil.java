/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : CryptoProviderUtil.java
 * Date de création : 22 nov. 2020
 * Heure de création : 06:27:17
 * Package : fr.vincent.tuto.common.utils.crypto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.utils.crypto;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import fr.vincent.tuto.common.constants.AppConstants;
import fr.vincent.tuto.common.exception.CustomAppException;
import fr.vincent.tuto.common.model.mo.CryptoModel;

/**
 * Utilitaire pour la cryptographie avec algorithmes symétrique AES et asymétrique RSA.
 * <ul>
 * <li>Encoder une chaîne en Base64 avec mot de pass/la clé (AES 128 bits ou RSA).</li>
 * <li>Encoder avec {@link SecureRandom} une chaîne en Base64 avec la clé (AES 128 bits).</li>
 * <li>Déchiffrer une chaîne cryptee en Base64 avec mot de passe/clé (AES 128 bits ou RSA)./li>
 * <li>Convertir une chaine encoder Base64 en fichier</li>
 * <li>Crypter en Base64 un fichier</li>
 * <li>Générer une clé de crytage {@link SecureRandom}</li>
 * </ul>
 * 
 * @author Vincent Otchoun
 */
public final class CryptoProviderUtil
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(CryptoProviderUtil.class);

    private static final String ENCRYPTION_KEY_MSG = "La clé de cryptage ne peut être null ou vide.";

    /**
     * Constructeur privé de l'utilitaire.
     */
    private CryptoProviderUtil()
    {
    }

    /**
     * Crypter en Base64 une chaine de caracteres avec algorithme symétrique AES 128 bits ou asymétrique RSA.
     * 
     * @param pStringToBeEncrypted la chaîne à crypter et encoder.
     * @param pCryptoModel         le modèle de données des paramètres des alogrithmes.
     * @return la chaîne cryptée et encodée en Base64.
     */
    public static String encrypt(final String pStringToBeEncrypted, final CryptoModel pCryptoModel)
    {
        LOGGER.info("[encrypt]- Chiffrement et encodage Base64 : [{}] ", pStringToBeEncrypted);

        String encryptedData = null;

        // Chargement des propriétés
        final var algorithm = pCryptoModel.getAlgorithm();
        final var encoding = pCryptoModel.getEncoding();
        final var instancePad = pCryptoModel.getInstancePad();
        final var encryptionKey = pCryptoModel.getKeyValue();

        final var flag = pCryptoModel.getAesSwitchFlag();
        final var publicKey = pCryptoModel.getPublicKey();

        Cipher cipher = null;
        try
        {
            // Traitement du flag
            if (BooleanUtils.isTrue(flag))
            {
                LOGGER.info("[encrypt]- Chiffrement AES et encodage Base64.");
                // Initialisation pour le traitement avec algorithme symétrique AES.
                final var keySpec = new SecretKeySpec(encryptionKey.getBytes(encoding), algorithm);
                cipher = Cipher.getInstance(instancePad);
                cipher.init(Cipher.ENCRYPT_MODE, keySpec);
            }
            else
            {
                LOGGER.info("[encrypt]- Chiffrement RSA et encodage Base64.");
                // Initialisation pour le traitement avec algorithme asymétrique RSA.
                final var rsaPublicKey = KeyPairProviderUtil.getPublicKeyFromString(publicKey);
                cipher = Cipher.getInstance(algorithm);
                cipher.init(Cipher.ENCRYPT_MODE, rsaPublicKey);
            }

            // Cryptage
            final var encryptedBytes = cipher.doFinal(pStringToBeEncrypted.getBytes(encoding));
            encryptedData = AppConstants.encodeBase64String(encryptedBytes);
        }
        catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException
        | BadPaddingException e)
        {
            throw new CustomAppException(e);
        }
        return encryptedData;
    }

    /**
     * Crypter en Base64 une chaine de caracteres avec SecureRandom algorithme symétrique AES 128 bits ou asymétrique
     * RSA.
     * 
     * @param pStringToBeEncrypted la chaîne à crypter.
     * @param pCryptoModel         le modèle de données des paramètres AES.
     * @return la chaîne cryptée en Base64.
     */
    @SuppressWarnings("static-access")
    public static String encryptSecureRandom(final String pStringToBeEncrypted, final CryptoModel pCryptoModel)
    {
        LOGGER.info("[encryptSecureRandom]- Chiffrement SecureRandom AES 128 bits de la chaîne : [{}] ", pStringToBeEncrypted);

        String secureEncryptedData = null;

        // Chargement des propriétés
        final var algorithm = pCryptoModel.getAlgorithm();
        final var encoding = pCryptoModel.getEncoding();
        final var instancePad = pCryptoModel.getInstancePad();
        final var encryptionKey = pCryptoModel.getKeyValue();
        final var secureRandomInstance = pCryptoModel.getSecureRandomInstance();

        try
        {
            // Génération aléatoire de la clef fournie pour le cryptage
            final var secureRandom = SecureRandom.getInstance(secureRandomInstance);
            Assert.notNull(encryptionKey, ENCRYPTION_KEY_MSG);
            byte[] keyBytes = encryptionKey.getBytes(encoding);
            keyBytes = secureRandom.getSeed(encryptionKey.length());

            // Initialisation
            final var keySpec = new SecretKeySpec(keyBytes, algorithm);
            final var cipher = Cipher.getInstance(instancePad);
            cipher.init(Cipher.ENCRYPT_MODE, keySpec);

            // Cryptage
            final var secureEncryptedBytes = cipher.doFinal(pStringToBeEncrypted.getBytes(encoding));
            secureEncryptedData = AppConstants.encodeBase64String(secureEncryptedBytes);
            return secureEncryptedData;
        }
        catch (NoSuchAlgorithmException | UnsupportedEncodingException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException
        | BadPaddingException e)
        {
            throw new CustomAppException(e);
        }
    }

    /**
     * Decrypter une chaine encodée Base64 avec AES 128 bits ou RSA.
     * 
     * @param pEncryptedText la chaîne à décrypter.
     * @param pCryptoModel   le modèle de données des paramètres AES.
     * @return la chaîne décryptée.
     */
    public static String decrypt(final String pEncryptedText, final CryptoModel pCryptoModel)
    {
        LOGGER.info("[decrypt]- Déchiffrement chaîne encodée Base64 : [{}]", pEncryptedText);

        String decryptedData = null;

        // Chargement des propriétés
        final var algorithm = pCryptoModel.getAlgorithm();
        final var encoding = pCryptoModel.getEncoding();
        final var instancePad = pCryptoModel.getInstancePad();
        final var encryptionKey = pCryptoModel.getKeyValue();

        final var flag = pCryptoModel.getAesSwitchFlag();
        final var privateKey = pCryptoModel.getPrivateKey();

        Cipher cipher = null;

        try
        {

            if (BooleanUtils.isTrue(flag))
            {
                LOGGER.info("[decrypt]- Déchiffrement AES de chaîne encodée Base64.");
                // Initialisation
                final var keyBytes = encryptionKey.getBytes(encoding);
                final var keySpec = new SecretKeySpec(keyBytes, algorithm);
                cipher = Cipher.getInstance(instancePad);
                cipher.init(Cipher.DECRYPT_MODE, keySpec);
            }
            else
            {
                LOGGER.info("[decrypt]- Déchiffrement RSA de chaîne encodée Base64.");
                cipher = Cipher.getInstance(algorithm);
                final var rsaPrivateKey = KeyPairProviderUtil.getPrivateKeyFromString(privateKey);
                cipher.init(Cipher.DECRYPT_MODE, rsaPrivateKey);
            }

            // Décryptage
            final var encryptedTextBytesBase64Decode = AppConstants.decodeBase64(pEncryptedText);
            final var decryptedBytes = cipher.doFinal(encryptedTextBytesBase64Decode);
            final var decryptData = new String(decryptedBytes, encoding);
            decryptedData = decryptData;
            return decryptedData;
        }
        catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException
        | BadPaddingException e)
        {
            throw new CustomAppException(e);
        }
    }

    /**
     * Ecrire le flux encodé Base64 dans un fichier.
     * 
     * @param pEncryptedText la chaîne cryptée à écritre dans le fichier.
     * @param pFilePath      emplacement ou chemin du fichier en sorite.
     */
    public static void writeToFile(final String pEncryptedText, final String pFilePath)
    {
        LOGGER.info("[writeToFile]- Ecrire dans le fichier - Emplacement : [{}]", pFilePath);

        final var fichier = Paths.get(pFilePath);
        fichier.toFile()//
        .setWritable(true);

        //
        try (final OutputStream outputStream = Files.newOutputStream(fichier);
        final BufferedOutputStream writer = new BufferedOutputStream(outputStream);)
        {
            //
            final var decoded = AppConstants.decodeBase64(pEncryptedText);
            writer.write(decoded);
            writer.flush();
        }
        catch (Exception e)
        {
            throw new CustomAppException(e);
        }
    }

    /**
     * Transformer ou encoder un fichier en Base64.
     * 
     * @param pFileName le chemin complet our relatif du fichier.
     * @return le fichier encodé Base 64.
     */
    public static String encodeFileToBase64(final String pFileName)
    {
        LOGGER.info("[encodeFileToBase64]- Encoder en Base 64 le fichier : [{}]", pFileName);

        String encryptedFile = null;

        final var fichier = Paths.get(pFileName);

        //
        try (final InputStream stream = Files.newInputStream(fichier); final BufferedInputStream reader = new BufferedInputStream(stream);)
        {
            //
            final int length = (int) fichier.toFile().length();
            final var bytes = new byte[length];
            reader.read(bytes, 0, length);
            encryptedFile = AppConstants.encodeBase64String(bytes);
            return encryptedFile;
        }
        catch (Exception e)
        {
            encryptedFile = null;
            throw new CustomAppException(e);
        }
    }

    /**
     * Générer la clé de chiffrement/déchiffrement AES.
     * 
     * @param pSecretKeyTextProvider le texte de production de la clé de cryptage.
     * @return la clé de cryptage/décryptage.
     */
    public static String createSecureRandomSecretKey(final String pSecretKeyTextProvider, final CryptoModel pAesModel)
    {
        LOGGER.info("[createSecureRandomSecretKey]- Générer la clé de Cryptage/Décryptage");

        String secretKey = null;

        try
        {
            final var algorithm = pAesModel.getAlgorithm();
            final var keygen = KeyGenerator.getInstance(algorithm);
            final var random = new SecureRandom(pSecretKeyTextProvider.getBytes());
            keygen.init(random);
            keygen.init(AppConstants.AES_GEN_KEY_SIZE);
            var key = keygen.generateKey();
            secretKey = AppConstants.encodeBase64String(key.getEncoded());
            return secretKey;
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new CustomAppException(e);
        }
    }
}
