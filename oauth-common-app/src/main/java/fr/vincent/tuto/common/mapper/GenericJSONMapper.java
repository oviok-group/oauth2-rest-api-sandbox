/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : GenericJSONMapper.java
 * Date de création : 25 nov. 2020
 * Heure de création : 09:15:22
 * Package : fr.vincent.tuto.common.mapper
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.mapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.vincent.tuto.common.constants.AppConstants;
import fr.vincent.tuto.common.exception.CustomAppException;

/**
 * Composant générique de traitement des objets au format JSON.
 * 
 * @author Vincent Otchoun
 */
@Component
public class GenericJSONMapper implements InitializingBean
{
    private static final Logger LOGGER = LoggerFactory.getLogger(GenericJSONMapper.class);

    private static final String WRITABLE_FILE_MSG = "Le fichier doit être autorisé en écriture.";

    private final ObjectMapper objectMapper;

    /**
     * Constructeur avec paramètres pour injection du beans en dépendances.
     * 
     * @param objectMapper le bean de conversion des objets selon le type.
     */
    @Autowired
    public GenericJSONMapper(final ObjectMapper pObjectMapper)
    {
        this.objectMapper = pObjectMapper;
    }

    /**
     * Construire le flux au format JSON de l'objet en entrée.
     * 
     * @param <T>              le type de l'objet à traiter.
     * @param pObjectToBeBound l'objet à traiter.
     * @param pOutPrettyPrint  true formate le flux de sortie, false sinon.
     * @return le flux au format JSON.
     */
    public <T> String toStringJSON(final T pObjectToBeBound, final Boolean pOutPrettyPrint)
    {
        String strJSON = null;

        try
        {
            //
            if (pObjectToBeBound == null)
            {
                throw new IllegalArgumentException(AppConstants.NULL_OBJECT_MESSAGE);
            }

            // Traitement si le type de'objet traité n'est pas null
            if (BooleanUtils.isTrue(pOutPrettyPrint))
            {
                // formate la sortie
                return this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(pObjectToBeBound);
            }
            else
            {
                // Sortie non formatée
                strJSON = this.objectMapper.writeValueAsString(pObjectToBeBound);
            }
        }
        catch (Exception e)
        {
            throw new CustomAppException(e);
        }
        return strJSON;
    }

    /**
     * Construire la liste de flux au format JSON à partir de la iste d'objet en entrée.
     * 
     * @param <T>                  le type des objets à traiter.
     * @param pObjectListToBeBound la liste des objets à traiter.
     * @param pOutPrettyPrint      true formate le flux de sortie, false sinon.
     * @return la liste de flux au format JSON.
     */
    public <T> List<String> toStringJSONList(final Collection<T> pObjectListToBeBound, final Boolean pOutPrettyPrint)
    {
        LOGGER.info("[toStringJSONList] - Obtenir une liste de flux au format JSON de la liste d'objet.");

        return Optional.ofNullable(pObjectListToBeBound)//
        .orElseGet(Collections::emptyList)//
        .stream()//
        .filter(Objects::nonNull)//
        .map(objectToBeBound -> toStringJSON(objectToBeBound, pOutPrettyPrint))//
        .collect(Collectors.toList());
    }

    /**
     * Construire le modèle objet du flux au format JSON en entrée.
     * 
     * @param <T>               le type de l'objet à retourner.
     * @param pStringJsonFormat le flux au format JSON à traiter.
     * @param pClassesToBeBound l'objet à retourner.
     * @return le modèle le modèle objet du flux au format JSON.
     */
    public <T> T toJSONObject(final String pStringJsonFormat, final Class<T> pClassesToBeBound)
    {
        T jsonObject = null;

        try
        {
            //
            if (pStringJsonFormat == null || pClassesToBeBound == null)
            {
                throw new IllegalArgumentException(AppConstants.NULL_OBJECT_MESSAGE);
            }

            jsonObject = this.objectMapper.readValue(pStringJsonFormat, pClassesToBeBound);
        }
        catch (Exception e)
        {
            throw new CustomAppException(e);
        }
        return jsonObject;
    }

    /**
     * Construire la liste de modèles objets à partir de liste de flux au format JSON en entrée.
     * 
     * @param <T>                le type de l'objet à retourner.
     * @param pStringsJsonFormat la liste de flux au format JSON à traiter.
     * @param pClassesToBeBound  l'objet à retourner.
     * @return la liste d'objets de flux au format JSON.
     */
    public <T> Collection<T> toJSONObjectList(final Collection<String> pStringsJsonFormat, final Class<T> pClassesToBeBound)
    {
        LOGGER.info("[toJSONObjectList] - Obtenir une liste d'objet de flux au format JSON.");

        // return pStringsJsonFormat.stream()//
        return Optional.ofNullable(pStringsJsonFormat)//
        .orElseGet(Collections::emptyList)//
        .stream()//
        .filter(Objects::nonNull)//
        .map(strJson -> toJSONObject(strJson, pClassesToBeBound))//
        .collect(Collectors.toList());
    }

    /**
     * Vérifier la création du flux physique au format JSON de l'objet en entrée.
     * 
     * @param <T>                  le type de l'objet à traiter.
     * @param pFileNameLocation    le chemin complet du fichier à créer.
     * @param isMemoryCreate       true créée le fichier en mémoire, false le flux physique.
     * @param pObjectListToBeBound la liste des éléments à créer dans le fichier en sortie.
     * @param isOutPrettyPrint     true formate les éléments du fichier en sortie, false sinon.
     * @return le flux physique ou mémoire des élements au format JSON.
     */
    public <T> Boolean writeJSONFile(final String pFileNameLocation, final Boolean isMemoryCreate, final Collection<T> pObjectListToBeBound,
    final Boolean isOutPrettyPrint)
    {
        LOGGER.info("[toJSONFile] - Création de fichier au format JSON de l'objet.");

        File file = null;
        Boolean creerFichier = null;

        try
        {
            // Traitement pour la création de l'ossature du flux physique ou mémoire
            file = this.creerFichier(pFileNameLocation, isMemoryCreate);

            // Traitements pour l'écrure des données formatées ou non
            if (BooleanUtils.isTrue(isOutPrettyPrint))
            {
                this.objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, pObjectListToBeBound);
                creerFichier = Boolean.TRUE;
            }
            else
            {
                this.objectMapper.writeValue(file, pObjectListToBeBound);
                creerFichier = Boolean.TRUE;
            }
        }
        catch (Exception e)
        {
            throw new CustomAppException(e);
        }
        return creerFichier;
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        Assert.notNull(objectMapper, AppConstants.NOT_NULL_BEAN_MESSAGE);
    }


    /**
     * Créer le flux physique ou le fichier en mémoire.
     * 
     * @param pFileNameLocation le chemin complet du fichier.
     * @param isMemoryCreate    false le flux physique, true le flux en mémoire (temporaire).
     * @return le fichier physique ou temporaire créée.
     * @throws IOException exception levée lorsque survient une erreur.
     */
    private File creerFichier(final String pFileNameLocation, final Boolean isMemoryCreate) throws IOException
    {
        LOGGER.info("[creerFichier] - Créer flux physique ou temporaire des éleménts au format JSON.");
        File file = null;
        if (BooleanUtils.isTrue(isMemoryCreate))
        {
            file = Files.createTempFile(pFileNameLocation, AppConstants.JSON_FILE_SUFFIXE).toFile();
            file.deleteOnExit();
        }
        else
        {
            final Path fichier = Paths.get(pFileNameLocation);
            file = fichier.toFile();
            this.setFileWritable(file);
        }
        return file;
    }

    /**
     * Accorder les dorist en écriture dans le fichier.
     * 
     * @param file le fichier à remplir.
     */
    private void setFileWritable(File file)
    {
        final boolean canBeWrite = file.setWritable(true);
        if (!canBeWrite)
        {
            final boolean result = !file.setWritable(true);
            Assert.isTrue(result, WRITABLE_FILE_MSG);
        }
    }
}
