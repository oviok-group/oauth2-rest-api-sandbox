/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : AuthProviderUtil.java
 * Date de création : 24 nov. 2020
 * Heure de création : 06:26:34
 * Package : fr.vincent.tuto.common.utils.auth
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.utils.auth;

import java.security.Key;
import java.time.Clock;
import java.time.Instant;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import fr.vincent.tuto.common.constants.AppConstants;
import fr.vincent.tuto.common.enumeration.BaseRolesEnum;
import fr.vincent.tuto.common.model.mo.TokenModel;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.SignatureException;

/**
 * Utilitaire fournissant des éléments d'authentification de Spring Security et JWT (Json Web Tokens).
 * 
 * @author Vincent Otchoun
 */
public final class AuthProviderUtil
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthProviderUtil.class);

    /**
     * Construteur privé de l'utilitaire.
     */
    private AuthProviderUtil()
    {
    }

    /**
     * Obtenir l'identifiant (Username) de l'utilisateur courant, actuel.
     * 
     * @return le login de l'utilisateur courant.
     */
    public static Optional<String> getSecureUserLogin()
    {
        LOGGER.info("[getSecureUserLogin] - Obtenir le login de l'utilisateur courant.");

        // Chagrement du contexte de sécurité
        final var securityContext = SecurityContextHolder.getContext();

        return Optional.ofNullable(securityContext.getAuthentication())//
        .map(authentication -> {
            if (UserDetails.class.isAssignableFrom(authentication.getPrincipal().getClass()))
            {
                final UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
                return springSecurityUser.getUsername();
            }
            else if (String.class.isAssignableFrom(authentication.getPrincipal().getClass()))
            {
                return (String) authentication.getPrincipal();
            }
            return null;
        });
    }

    /**
     * Obtenir le jeton JWT pour l'utilisateur courant.
     * 
     * @return le jeton JWT de l'utilisateur.
     */
    public static Optional<String> getCurrentUserJWTToken()
    {
        LOGGER.info("[getCurrentUserJWTToken] - Obtenir le jeton JWT de l'utilisateur courant.");

        // Chagrement du contexte de sécurité
        final var securityContext = SecurityContextHolder.getContext();

        return Optional.ofNullable(securityContext.getAuthentication())//
        .filter(authentication -> String.class.isAssignableFrom(authentication.getCredentials().getClass()))//
        .map(authentication -> (String) authentication.getCredentials());
    }

    /**
     * Obtenir la chaîne concatenée de l'ensemble des rôles de l'utilisateur.
     * 
     * @return la chaîne concatenée des rôles de l'utilisateur.
     */
    public static String getConcatAllUserRoles(final Authentication pAuthentication, final String pDelimiter)
    {
        LOGGER.info("[getConcatAllUserRoles] - Obtenir la concatenation de l'ensemble des rôles de l'utilisateur.");
        return pAuthentication.getAuthorities().stream()//
        .filter(Objects::nonNull)//
        .map(GrantedAuthority::getAuthority)//
        .collect(Collectors.joining(pDelimiter));
    }

    /**
     * Obtenir les reclamations du jeton JWT.
     * 
     * @param pStrToken le jeton JWT à traiter.
     * @param pKey      la clé de signature du jeton.
     * @return les reclamations du jeton JWT.
     */
    public static Claims getTokenClaims(final String pStrToken, final Key pKey)
    {
        LOGGER.info("[getTokenClaims] - Obtenir les reclamations du jeton JWT.");

        return Jwts.parser()//
        .setSigningKey(pKey)//
        .parseClaimsJws(pStrToken)//
        .getBody();
    }

    /**
     * Obtenir le nom de l'utilisateur du jeton JWT.
     * 
     * @param pStrToken le jeton JWT à traiter.
     * @param pKey      la clé de signature du jeton.
     * @return le nom de l'utilisateur.
     */
    public static String getUsername(final String pStrToken, final Key pKey)
    {
        LOGGER.info("[getUsername] - Obtenir le nom d'utilisateur du jeton JWT.");

        return getTokenClaims(pStrToken, pKey).getSubject();
    }

    /**
     * Obtenir la signature du jeton JWT.
     * 
     * @param pStrToken le jeton JWT à traiter.
     * @param pKey      la clé de signature du jeton.
     * @return la signature du jeton JWT.
     */
    public static String getTokenSignature(final String pStrToken, final Key pKey)
    {
        LOGGER.info("[getTokenSignature] - Obtenir la signature du jeton JWT.");
        return Jwts.parser()//
        .setSigningKey(pKey)//
        .parseClaimsJws(pStrToken)//
        .getSignature();
    }

    /**
     * Obtenir l'entête du jeton JWT.
     * 
     * @param pStrToken le jeton JWT à traiter.
     * @param pKey      la clé de signature du jeton.
     * @return l'entête du jeton JWT.
     */
    @SuppressWarnings("rawtypes")
    public static JwsHeader getTokenHeader(final String pStrToken, final Key pKey)
    {
        LOGGER.info("[getTokenHeader] - Obtenir l'entête du jeton JWT.");
        return Jwts.parser()//
        .setSigningKey(pKey)//
        .parseClaimsJws(pStrToken)//
        .getHeader();
    }

    /**
     * Obtenir la date de validité du jeton d'authentification.
     * 
     * @param pTokenModel les données de traitement du jeton d'authnetification.
     * @return la date de validité du jeton d'authentification.
     */
    public static Date getTokenValidity(final TokenModel pTokenModel)
    {
        LOGGER.info("[getTokenValidity] - Obtenir la date de validité du jeton d'authentification.");

        if (pTokenModel == null)
        {
            return null;
        }

        Date validity = null;

        // Récupérer les données de traitement du jeton
        final Boolean rememberMe = pTokenModel.getRememberMe();
        final Boolean refreshToken = pTokenModel.getRefreshToken();

        final Long rememberMeTokenValidity = pTokenModel.getRememberMeTokenValidity();
        final Long refreshTokenValidity = pTokenModel.getRefreshTokenValidity();
        final Long tokenValidity = pTokenModel.getRememberMeTokenValidity();

        // Date courante en secondes et calcul de la durée de traitement
        final Long instant = Instant.now(Clock.systemDefaultZone()).getEpochSecond();
        if (BooleanUtils.isTrue(rememberMe))
        {
            validity = new Date(instant + rememberMeTokenValidity);
        }
        else if (BooleanUtils.isTrue(refreshToken))
        {
            validity = new Date(instant + refreshTokenValidity);
        }
        else
        {
            validity = new Date(instant + tokenValidity);
        }
        return validity;
    }

    /**
     * Vérifier que l'utilisateur connecté est authentifié.
     * 
     * @return true si l'utilisateur connecté n'est pas anonyme (n'a pas le rôle ANONYMOUS), false sinon.
     */
    public static Boolean isAuthenticatedUser()
    {
        LOGGER.info("[isAuthenticatedUser] - Vérifer que l'utilisateur est authentifié.");

        // Chagrement du jeton de demande d'authentification à partir du contexte de sécurité
        final var authentication = SecurityContextHolder.getContext().getAuthentication();
        return getUserRoles(authentication)//
        .noneMatch(BaseRolesEnum.ANONYMOUS.getAuthority()::equals);
    }

    /**
     * Vérifier que l'utilisateur connecté est authentifié.
     * 
     * @return true si l'utilisateur connecté n'est pas anonyme (n'a pas le rôle ANONYMOUS), false sinon.
     */
    public static Boolean isAuthenticated()
    {
        LOGGER.info("[isAuthenticated] - Vérifer que l'utilisateur est authentifié.");

        // Chagrement du jeton de demande d'authentification à partir du contexte de sécurité
        final var authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || AnonymousAuthenticationToken.class.isAssignableFrom(authentication.getClass()))
        {
            return Boolean.FALSE;
        }

        return authentication.isAuthenticated();
    }

    /**
     * Vérifier que l'utilisateur authentifié ne dispose pas du rôle spécifié.
     * 
     * @param pRoleName le ôle non attendu.
     * @return true si l'utilisateur ne dispose pas du rôle spécifié (par exemple n'est pas aonyme, n'a pas le rôle
     *         ANONYMOUS), false sinon.
     */
    public static Boolean isAuthenticatedUser(final String pRoleName)
    {
        LOGGER.info("[isAuthenticatedUser] - Vérifer que l'utilisateur est authentifié (pas anonyme).");

        // Chagrement du jeton de demande d'authentification à partir du contexte de sécurité
        final var authentication = SecurityContextHolder.getContext().getAuthentication();
        return getUserRoles(authentication)//
        .noneMatch(pRoleName::equals);
    }

    /**
     * Vérfier si l'utilisateur actuel dispose d'une autorité spécifique (rôle de sécurité).
     * 
     * @param pRole l'autorité spécifique à vérifier.
     * @return true si l'utilisateur actuel a le rôle, false dans le cas contraire.
     */
    public static Boolean isUserInRole(final String pRole)
    {
        LOGGER.info("[isUserInRole] - Vérifer pour pour l'utilisateur courant le rôle : [{}]", pRole);

        // Chagrement du jeton de demande d'authentification à partir du contexte de sécurité
        final var authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null && getUserRoles(authentication).anyMatch(pRole::equals);
    }

    /**
     * Vérifier la validité de l'en-tête dans la requête d'authentification transmise.
     * 
     * @param pBearerToken la chaîne contenant le jeton d'authentification (Bearer) préfixé.
     * @param pTokenPrefix le préfixe de la chaîne d'authentification dans l'en-tête.
     * @return true si valide, false sinon.
     */
    public static Boolean isAuthorizationHeader(final String pBearerToken, final String pTokenPrefix)
    {
        LOGGER.info("[isAuthorizationHeader] - Vérifier la validité de l'en-tête de la requête d'authentification.");

        return StringUtils.isNotBlank(pBearerToken) && StringUtils.contains(pBearerToken, AppConstants.SPACE_STR) && StringUtils.startsWith(pBearerToken, pTokenPrefix);
    }

    /**
     * Construire le jeton JWT d'authentification avec les données de la requête HTTP.
     * 
     * @param pHttpServletRequest  la requête HTTP Servlet
     * @param pAuthorizationHeader la chaîne contenant le jeton d'authentification (Bearer) préfixé.
     * @param pTokenPrefix         le préfixe de la chaîne d'authentification dans l'en-tête.
     * @return le jeton JWT d'authentification.
     */
    public static Optional<String> extractJWTToken(final HttpServletRequest pHttpServletRequest, final String pAuthorizationHeader, final String pTokenPrefix)
    {
        LOGGER.info("[extractJWTToken] - Extraire le jeton JWT d'authentification de la requête.");

        // Recupérer la chaîne d'authentification de la requête.
        final var bearerToken = pHttpServletRequest.getHeader(pAuthorizationHeader);

        return Optional.ofNullable(pAuthorizationHeader)//
        .filter(token -> isAuthorizationHeader(bearerToken, pTokenPrefix))//
        .map(token -> StringUtils.substring(bearerToken, pTokenPrefix.length(), bearerToken.length()).trim());
    }

    /**
     * Vérifier que l'exception levée parmi celles disponibles pour JWT n'est pas dûe à l'expiration du jeton
     * d'authentification.
     * 
     * @param <T>                       le type de l'exception levée.
     * @param pExceptionClasseToBeBound l'exception à vérifier.
     * @return true si non liée à l'expiration du jeton JWT, false sinon.
     */
    public static <T> Boolean isNotExpiredException(final Class<T> pExceptionToBeBound)
    {
        LOGGER.info("[isNotExpiredException] - Vérifier l'expiration du jeton d'authentification JWT.");

        return SignatureException.class.isAssignableFrom(pExceptionToBeBound) || MalformedJwtException.class.isAssignableFrom(pExceptionToBeBound) || UnsupportedJwtException.class
        .isAssignableFrom(pExceptionToBeBound);
    }

    /**
     * Obtenir le jeton pour une demande d'authentification
     * 
     * @param pUser utilisateur spring
     * @return le jeton de deamnde d'authenficiation
     */
    public static Authentication createAuthentication(final User pUser)
    {
        LOGGER.info("[createAuthentication] - Jeton de la demande d'authentification avec infos utilisateur.");
        return new UsernamePasswordAuthenticationToken(pUser, pUser.getPassword(), pUser.getAuthorities());
    }

    /**
     * Construire le jeton de la demande d'authentification avec les infomrations d'identification de l'utilisateur.
     * 
     * @param pPrincipal   l'utilisateur à authentifier.
     * @param pCredentials les informations d'identification.
     * @return le jeton de la demande d'authentification.
     */
    public static UsernamePasswordAuthenticationToken createAuthentication(final String pPrincipal, final String pCredentials)
    {
        LOGGER.info("[createAuthentication] - Jeton de la demande d'authentification avec credentials.");
        return new UsernamePasswordAuthenticationToken(pPrincipal, pCredentials);
    }

    /**
     * Construire le jeton de la demande d'authentification avec les infomrations d'identification de l'utilisateur.
     * 
     * @param pPrincipal   l'utilisateur à authentifier.
     * @param pCredentials les informations d'identification.
     * @param pAuthorities les rôles de l'utilisateur.
     * @return le jeton de la demande d'authentification.
     */
    public static UsernamePasswordAuthenticationToken creatieAuthentication(final String pPrincipal, final String pCredentials, final Collection<GrantedAuthority> pAuthorities)
    {
        LOGGER.info("[creatieAuthentication] - Obtenir le jeton de la demande d'authentification.");
        return new UsernamePasswordAuthenticationToken(pPrincipal, pCredentials, pAuthorities);
    }

    /**
     * Construire le jeton d'authentification anonyme
     * 
     * @param pKey         pour identifier si cet objet réalisé par un client autorisé
     * @param pPrincipal   le mandant (l'utilisateur à authentifier).
     * @param pAuthorities les rôles accordés au mandant (à l'utilisateur).
     * @return le jeton de la demande d'authentification anonyme.
     */
    public static AnonymousAuthenticationToken createAnonymousAuthentication(final String pKey, final Object pPrincipal, final Collection<GrantedAuthority> pAuthorities)
    {
        LOGGER.info("[createAnonymousAuthentication] - Obtenir le jeton d'authentification anonyme.");
        return new AnonymousAuthenticationToken(pKey, pPrincipal, pAuthorities);
    }

    /**
     * Obtenir le jeton de la dmeande d'authentification avec les informations de {@link User} et les rôles définis.
     * 
     * @param pUser        les informations de l'utilisateur.
     * @param pAuthorities les rôles ou authorités de l'"utilisateur.
     * @return le jeton de la demande d'authentifications.
     */
    public static UsernamePasswordAuthenticationToken createAuthentication(final User pUser, final Collection<GrantedAuthority> pAuthorities)
    {
        LOGGER.info("[createAuthentication] - Obtenir le jeton de la demande d'authentification.");
        return new UsernamePasswordAuthenticationToken(pUser, pUser.getPassword(), pAuthorities);
    }

    /**
     * Obtenir le jeton de la dmeande d'authentification avec les informations de {@link UserDetails} et les rôles définis.
     * 
     * @param pUser        les informations de l'utilisateur.
     * @param pAuthorities les rôles ou authorités de l'"utilisateur.
     * @return le jeton de la demande d'authentifications.
     */
    public static UsernamePasswordAuthenticationToken createAuthentication(final UserDetails pUser, final Collection<GrantedAuthority> pAuthorities)
    {
        LOGGER.info("[createAuthentication] - Obtenir le jeton de la demande d'authentification.");
        return new UsernamePasswordAuthenticationToken(pUser, pUser.getPassword(), pAuthorities);
    }

    /**
     * Construire un utilisateur au sens Spring Security avec les informations d'identitifcation.
     * 
     * @param pUsername    le login de l'utilisateur.
     * @param pPassword    le mot de passe de l'utilisateur.
     * @param pAuthorities les rôles de l'utilisateur.
     * @return
     */
    public static User createSpringUser(final String pUsername, final String pPassword, final Collection<GrantedAuthority> pAuthorities)
    {
        LOGGER.info("[createSpringUser] - Obtenir les informations de l'utilisateur à authentifier.");
        return new User(pUsername, pPassword, pAuthorities);
    }



    /**
     * Construire une autorité accordée à un objet d'authentification.
     * 
     * @param pRole le rôle associé.
     * @return
     */
    public static SimpleGrantedAuthority createRole(final String pRole)
    {
        return new SimpleGrantedAuthority(pRole);
    }

    /**
     * Récupérez les rôles de l'utilisateur du jeton de demande d'authentification dans un flux de chaînes de
     * caractères.
     * 
     * @param pAuthentication le jeton pour une demande d'authentification.
     * @return la chaînes de flux de rôles.
     */
    private static Stream<String> getUserRoles(final Authentication pAuthentication)
    {
        LOGGER.info("[getUserRoles] - Obtenir la chaîne de flux de rôles du jeton d'authentification.");

        return pAuthentication.getAuthorities()//
        .stream()//
        .filter(Objects::nonNull)//
        .map(GrantedAuthority::getAuthority);
    }
}
