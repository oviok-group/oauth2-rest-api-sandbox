/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : DatesConverterUtil.java
 * Date de création : 27 nov. 2020
 * Heure de création : 03:42:51
 * Package : fr.vincent.tuto.common.utils.date
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.utils.date;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utilitaire de conversion des dates. Il permet de :
 * <p>
 * <ul>
 * <li>Construire {@link LocalDate} à partir de {@link Date} ou {@link java.sql.Date}.</li>
 * <li>Construire {@link LocalDateTime} à partir de {@link Date} ou {@link java.sql.Date}.</li>
 * <li>Construire {@link Date} à partir de {@link LocalDateTime}.</li>
 * <li>Construire {@link Date} à partir de {@link LocalDate}.</li>
 * <li>Construire {@link ZonedDateTime} à partir de la durée en milliseconde.</li>
 * <li>Construire {@link Instant} à partir de la durée en milliseconde.</li>
 * <li>Construire {@link XMLGregorianCalendar} à partir de {@link LocalDate} et vice versa.</li>
 * </ul>
 * 
 * @author Vincent Otchoun
 */
public final class DatesConverterUtil
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DatesConverterUtil.class);
    
    private static final String MESSAGE = "[getFromDateAsString] - Erreur conversion de date : \n {} ";

    /**
     * Constructeur privé.
     */
    private DatesConverterUtil()
    {
    }

    /**
     * Obtenir {@link LocalDate} de {@link Date}.
     * 
     * @param pDate la date à convertir ou à transformer.
     * @return la date transformée.
     */
    public static LocalDate convertToLocalDate(final Date pDate)
    {
        if (pDate == null)
        {
            return null;
        }

        return LocalDate.ofInstant(pDate.toInstant(), ZoneId.systemDefault());
    }

    /**
     * Obtenir {@link LocalDate} de {@link Date} via {@link Instant}.
     * 
     * @param pDate la date à convertir ou à transformer.
     * @return la date transformée.
     */
    public static LocalDate convertToLocalDateWithInstant(final Date pDate)
    {
        if (pDate == null)
        {
            return null;
        }

        return pDate.toInstant()//
        .atZone(ZoneId.systemDefault())//
        .toLocalDate();
    }

    /**
     * Obtenir {@link LocalDate} de {@link Date} avec {@link java.sql.Date} .
     * 
     * @param pDate la date sql à traiter.
     * @return la date transformée.
     */
    public static LocalDate convertToLocalDateWithSqlDate(final Date pDate)
    {
        if (pDate == null)
        {
            return null;
        }
        return new java.sql.Date(pDate.getTime())//
        .toLocalDate();
    }

    /**
     * Obtenir {@link LocalDate} de {@link Date} via millisecond.
     * 
     * @param pDate la date sql à traiter.
     * @return la date transformée.
     */
    public static LocalDate convertToLocalDateWithMilisecond(final Date pDate)
    {
        if (pDate == null)
        {
            return null;
        }

        return Instant.ofEpochMilli(pDate.getTime())//
        .atZone(ZoneId.systemDefault())//
        .toLocalDate();
    }

    /**
     * Obtenir {@link LocalDateTime} de {@link Date}.
     * 
     * @param pDate la date à convertir ou à transformer.
     * @return la date transformée.
     */
    public static LocalDateTime convertToLocalDateTime(final Date pDate)
    {
        return convertWithInstant(pDate);
    }

    /**
     * Obtenir {@link LocalDateTime} de {@link Date} via {@link Instant}.
     * 
     * @param pDate la date à convertir ou à transformer.
     * @return la date transformée.
     */
    public static LocalDateTime convertToLocalDateTimeWithInstant(final Date pDate)
    {
        return convertWithInstant(pDate);
    }

    /**
     * @param pDate
     * @return
     */
    private static LocalDateTime convertWithInstant(final Date pDate)
    {
        if (pDate == null)
        {
            return null;
        }

        return pDate.toInstant()//
        .atZone(ZoneId.systemDefault())//
        .toLocalDateTime();
    }

    /**
     * Obtenir {@link LocalDateTime} de {@link Date} avec {@link Timestamp}.
     * 
     * @param pDate la date sql à traiter.
     * @return la date transformée.
     */
    public static LocalDateTime convertToLocalDateTimeWithSqlTimestamp(final Date pDate)
    {
        if (pDate == null)
        {
            return null;
        }
        return new java.sql.Timestamp(pDate.getTime())//
        .toLocalDateTime();
    }

    /**
     * Obtenir {@link LocalDateTime} de {@link Date} via millisecond.
     * 
     * @param pDate la date sql à traiter.
     * @return la date transformée.
     */
    public static LocalDateTime convertToLocalDateTimeWithMilisecond(final Date pDate)
    {
        if (pDate == null)
        {
            return null;
        }

        return Instant.ofEpochMilli(pDate.getTime())//
        .atZone(ZoneId.systemDefault())//
        .toLocalDateTime();
    }

    /**
     * Obtenir la date courrante du système à partir de {@link LocalDateTime}.
     * 
     * @return la date courante du système.
     */
    public static Date getCurrentDateTime()
    {
        return Date.from(LocalDateTime.now()//
        .atZone(ZoneId.systemDefault())// ZoneId.
        .toInstant());
    }

    public static Date getCurrentDateTime(final String pZoneId)
    {
        return Date.from(LocalDateTime.now()//
        .atZone(ZoneId.systemDefault())//
        .withZoneSameInstant(ZoneId.of(pZoneId))
        .toInstant());
    }


    /**
     * Obtenir la date courrante du système à partir de {@link LocalDate}.
     * 
     * @return la date courante du système.
     */
    public static Date getCurrentDate()
    {
        return Date.from(LocalDate.now()//
        .atStartOfDay()//
        .atZone(ZoneId.systemDefault())//
        .toInstant());
    }

    /**
     * Obtenir {@link Date} de {@link LocalDate} via {@link java.sql.Date}.
     * 
     * @param pLocalDate la date à traiter.
     * @return la date transformée.
     */
    public static Date convertToDateWithSqlDate(final LocalDate pLocalDate)
    {
        if (pLocalDate == null)
        {
            return null;
        }

        return java.sql.Date.valueOf(pLocalDate);
    }

    /**
     * Obtenir {@link Date} de {@link LocalDateTime} via {@link Instant}.
     * 
     * @param pLocalDate la date à transformer.
     * @return la date transformée.
     */
    public static Date convertToDateWithInstant(final LocalDate pLocalDate)
    {
        if (pLocalDate == null)
        {
            return null;
        }

        return java.util.Date.from(pLocalDate//
        .atStartOfDay()//
        .atZone(ZoneId.systemDefault())//
        .toInstant());
    }

    /**
     * Obtenir {@link Date} de {@link LocalDateTime} via {@link Instant}.
     * 
     * @param pLocalDateTime la date à transformer.
     * @return la date transformée.
     */
    public static Date convertToDateWithInstant(final LocalDateTime pLocalDateTime)
    {
        if (pLocalDateTime == null)
        {
            return null;
        }

        return java.util.Date.from(pLocalDateTime//
        .atZone(ZoneId.systemDefault())//
        .toInstant());
    }

    /**
     * Obtenir {@link Date} de {@link LocalDateTime} via {@link Timestamp}.
     * 
     * @param pLocalDateTime la date à transformer.
     * @return la date transformée.
     */
    public static Date convertToDateTimeWithSqlTimestamp(final LocalDateTime pLocalDateTime)
    {
        if (pLocalDateTime == null)
        {
            return null;
        }

        return java.sql.Timestamp.valueOf(pLocalDateTime);
    }

    /**
     * Obtenir la date-heure avec fuseau horaire {@link ZonedDateTime} à partir de la durée en millisecond.
     * <p>
     * Exemple : 2007-12-03T10:15:30+01:00 Europe/Paris.
     * 
     * @param pMillisecond la durée en millisecond.
     * @return la date-heure avec fuseau horaire.
     */
    public static ZonedDateTime getZonedDateTime(final long pMillisecond)
    {
        final LocalDateTime dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(pMillisecond), ZoneId
        .systemDefault());

        return ZonedDateTime.of(dateTime, ZoneId.systemDefault());
    }

    /**
     * Obtenir un point instantané sur la ligne du temps.
     * 
     * @param pMillisecond la durée en millisecond.
     * @return le point instantané.
     */
    public static Instant getInstant(final long pMillisecond)
    {
        return Instant.ofEpochMilli(pMillisecond);
    }

    /**
     * Obtenir l'horodatage à partir du wrappeur {@link Timestamp}.
     * 
     * @return l'horodatege.
     */
    public static Timestamp getTimestamp()
    {
        return Timestamp.from(Instant.now(Clock.systemDefaultZone()));
    }

    /**
     * Ontenir {@link XMLGregorianCalendar} de {@link LocalDate}.
     * 
     * @param pLocalDate la date à transformer.
     * @return la date XML obtenue.
     * @throws DatatypeConfigurationException exception levée lorsque survient une erreur.
     */
    public static XMLGregorianCalendar getXMLGregorianCalendar(final LocalDate pLocalDate)
    throws DatatypeConfigurationException
    {
        if (pLocalDate == null)
        {
            return null;
        }

        return DatatypeFactory.newInstance().newXMLGregorianCalendar(pLocalDate.toString());
    }

    /**
     * Obtenir {@link LocalDate} de {@link XMLGregorianCalendar}.
     * 
     * @param pXmlGregorianCalendar la date XML à transformer.
     * @return la date locale obtenue.
     */
    public static LocalDate getLocalDate(final XMLGregorianCalendar pXmlGregorianCalendar)
    {
        if (pXmlGregorianCalendar == null)
        {
            return null;
        }

        return LocalDate.of(pXmlGregorianCalendar.getYear(), pXmlGregorianCalendar.getMonth(), pXmlGregorianCalendar
        .getDay());
    }

    /**
     * Convertir la date indiquée sous forme de chaîne de caractères avec {@link DateFormat} et
     * {@link SimpleDateFormat}.
     * 
     * @param <T>              le type de l'objet à convertir.
     * @param pDateFormat      le format de date attendu.
     * @param pTimeZone        le fuseau horaire choisi.
     * @param pObjectToBeBound l'objet à traiter.
     * @return la date convertie en chaîne de caractères si tous les pramaètres renseignés, sinon retourne null.
     */
    public static <T> String formateDateToString(final String pDateFormat, final String pTimeZone,
    final T pObjectToBeBound)
    {
        if (pObjectToBeBound == null)
        {
            return null;
        }

        final var df = getDateFormat(pDateFormat, pTimeZone);
        return df != null ?  df.format(pObjectToBeBound): null;
    }


    /**
     * Transformer une date au format chaîne de caractères en objet {@link Date}.
     * 
     * @param pDateAsString la date au format chaîne de caractères à traiter.
     * @param pDateFormat
     * @param pTimeZone
     * @return
     */
    public static Date getFromDateAsString(final String pDateAsString, final String pDateFormat, final String pTimeZone)
    {
        if (StringUtils.isBlank(pDateAsString))
        {
            return null;
        }

        try
        {
            final var df = getDateFormat(pDateFormat, pTimeZone);
            return df!= null ? df.parse(pDateAsString): null;
        }
        catch (ParseException e)
        {
            LOGGER.warn(MESSAGE, e);
        }
        return null;
    }

    /**
     * Obtenir le convertisseur de date selon le fuseau horaire avec le format spécifié ou attendu.
     * 
     * @param pDateFormat le foramt de date attendu.
     * @param pTimeZone   le fuseau horaire spécifié.
     * @return le converetisseur de dates si fuseau horaire et format de date renseignés, sinon retourne null.
     */
    public static DateFormat getDateFormat(final String pDateFormat, final String pTimeZone)
    {
        if (StringUtils.isBlank(pDateFormat) || StringUtils.isBlank(pTimeZone))
        {
            return null;
        }

        DateFormat df = DateFormat.getDateTimeInstance();
        df = new SimpleDateFormat(pDateFormat);
        df.setTimeZone(TimeZone.getTimeZone(pTimeZone));
        return df;
    }
}
