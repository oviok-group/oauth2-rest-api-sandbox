/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 22 nov. 2020
 * Heure de création : 10:11:19
 * Package : fr.vincent.tuto.common.model.mo
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les objets non persistant.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.model.mo;
