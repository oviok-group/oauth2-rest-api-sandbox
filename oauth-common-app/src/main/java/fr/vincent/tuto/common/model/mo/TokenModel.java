/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : TokenModel.java
 * Date de création : 17 mars 2021
 * Heure de création : 05:18:12
 * Package : fr.vincent.tuto.common.model.mo
 * Auteur : Vincent Otchoun
 * Copyright © 2021 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.model.mo;

import java.io.Serializable;
import java.security.Key;
import java.security.KeyPair;
import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Objet de mapping des données de construction/traitement des jeton (Token).
 * 
 * @author Vincent Otchoun
 */

public class TokenModel implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -5778037399351192308L;

    //
    private Boolean rememberMe;
    private Boolean refreshToken;
    private Long rememberMeTokenValidity;
    private Long refreshTokenValidity;
    private Long tokenValidity;

    private Key key; // Le conteneur des éléménts de cryptographie du magasin de clés.
    private KeyPair keyPair; // le conteneur des clés privées/publiques du magasindes clés.

    public TokenModel rememberMe(final Boolean pRememberMe)
    {
        this.rememberMe = pRememberMe;
        return this;
    }

    public TokenModel refreshToken(final Boolean pRefreshToken)
    {
        this.refreshToken = pRefreshToken;
        return this;
    }

    public TokenModel rememberMeTokenValidity(final Long pRememberMeTokenValidity)
    {
        this.rememberMeTokenValidity = pRememberMeTokenValidity;
        return this;
    }

    public TokenModel refreshTokenValidity(final Long pRefreshTokenValidity)
    {
        this.refreshTokenValidity = pRefreshTokenValidity;
        return this;
    }

    public TokenModel key(final Key pKey)
    {
        this.key = pKey;
        return this;
    }

    public TokenModel keyPair(final KeyPair pKeyPair)
    {
        this.keyPair = pKeyPair;
        return this;
    }

    public TokenModel tokenValidity(final Long pTokenValidity)
    {
        this.tokenValidity = pTokenValidity;
        return this;
    }

    public Boolean getRememberMe()
    {
        return this.rememberMe;
    }

    public Boolean getRefreshToken()
    {
        return this.refreshToken;
    }

    public Long getRememberMeTokenValidity()
    {
        return this.rememberMeTokenValidity;
    }

    public Long getRefreshTokenValidity()
    {
        return this.refreshTokenValidity;
    }

    public Long getTokenValidity()
    {
        return this.tokenValidity;
    }

    public Key getKey()
    {
        return this.key;
    }

    public KeyPair getKeyPair()
    {
        return this.keyPair;
    }

    public void setRememberMe(final Boolean pRememberMe)
    {
        this.rememberMe = pRememberMe;
    }

    public void setRefreshToken(final Boolean pRefreshToken)
    {
        this.refreshToken = pRefreshToken;
    }

    public void setRememberMeTokenValidity(final Long pRememberMeTokenValidity)
    {
        this.rememberMeTokenValidity = pRememberMeTokenValidity;
    }

    public void setRefreshTokenValidity(final Long pRefreshTokenValidity)
    {
        this.refreshTokenValidity = pRefreshTokenValidity;
    }

    public void setTokenValidity(final Long pTokenValidity)
    {
        this.tokenValidity = pTokenValidity;
    }

    public void setKey(final Key pKey)
    {
        this.key = pKey;
    }


    public void setKeyPair(final KeyPair pKeyPair)
    {
        this.keyPair = pKeyPair;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(java.lang.Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        TokenModel tokenModel = (TokenModel) o;
        return Objects.equals(this.rememberMe, tokenModel.rememberMe)//
        && Objects.equals(this.refreshToken, tokenModel.refreshToken)//
        && Objects.equals(this.rememberMeTokenValidity, tokenModel.rememberMeTokenValidity)//
        && Objects.equals(this.refreshTokenValidity, tokenModel.refreshTokenValidity)//
        && Objects.equals(this.tokenValidity, tokenModel.tokenValidity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(this.rememberMe, this.refreshToken, this.rememberMeTokenValidity, this.refreshTokenValidity, this.tokenValidity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
