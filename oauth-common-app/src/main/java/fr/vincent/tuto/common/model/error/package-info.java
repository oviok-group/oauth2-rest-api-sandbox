/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 5 déc. 2020
 * Heure de création : 05:55:15
 * Package : fr.vincent.tuto.common.model.error
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Conteint les objets de gestion des erreurs dans les modules applicatifs.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.model.error;
