/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 26 nov. 2020
 * Heure de création : 04:08:49
 * Package : fr.vincent.tuto.common.service.mail
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les services de notifications par mail.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.service.mail;
