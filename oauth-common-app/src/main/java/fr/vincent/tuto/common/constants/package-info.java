/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 20 nov. 2020
 * Heure de création : 06:50:20
 * Package : fr.vincent.tuto.common.constants
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les utilitaires definssant de constantes.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.constants;
