/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : ApiValidationError.java
 * Date de création : 5 déc. 2020
 * Heure de création : 06:50:18
 * Package : fr.vincent.tuto.common.model.error
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.model.error;

import java.io.Serializable;
import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Objet d'encapsulation des erreurs de validation pour les API.
 * 
 * @author Vincent Otchoun
 */
@JsonPropertyOrder({ "object", "field", "rejectedValue", "message" })
@ApiModel(description = "Encapsulation des erreurs de validation")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiValidationError implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -7035005025630907764L;

    @JsonProperty(value = "object")
    @JsonPropertyDescription("Objet à traiter en validation")
    @ApiModelProperty(name = "object", dataType = "java.lang.String", value = "Objet à traiter en validation", position = 0)
    private String object;

    @JsonProperty(value = "field")
    @JsonPropertyDescription("Le champ en valisation")
    @ApiModelProperty(name = "field", dataType = "java.lang.String", value = "Le champ en valisation", position = 1)
    private String field;

    @JsonProperty(value = "rejectedValue")
    @JsonPropertyDescription("La valeur de rejet de la validation")
    @ApiModelProperty(name = "rejectedValue", dataType = "java.lang.Object", value = "La valeur de rejet de la validation", position = 2)
    private Object rejectedValue;

    @JsonProperty(value = "message")
    @JsonPropertyDescription("Le message d'erreur de validation")
    @ApiModelProperty(name = "message", dataType = "java.lang.String", value = "Le message d'erreur de validation", position = 3)
    private String message;

    /**
     * Constructeur par défaut.
     */
    public ApiValidationError()
    {
    }

    public ApiValidationError object(final String pObject)
    {
        this.object = pObject;
        return this;
    }

    public ApiValidationError field(final String pField)
    {
        this.field = pField;
        return this;
    }

    public ApiValidationError rejectedValue(final String pRejectedValue)
    {
        this.rejectedValue = pRejectedValue;
        return this;
    }

    public ApiValidationError message(final String pMessage)
    {
        this.message = pMessage;
        return this;
    }

    //////////////////
    /// ACCESSEURS
    //////////////////

    public String getObject()
    {
        return this.object;
    }

    public String getField()
    {
        return this.field;
    }

    public Object getRejectedValue()
    {
        return this.rejectedValue;
    }

    public String getMessage()
    {
        return this.message;
    }

    public void setObject(String object)
    {
        this.object = object;
    }

    public void setField(String field)
    {
        this.field = field;
    }

    public void setRejectedValue(Object rejectedValue)
    {
        this.rejectedValue = rejectedValue;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(java.lang.Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        ApiValidationError validationError = (ApiValidationError) o;
        return Objects.equals(this.object, validationError.object)//
        && Objects.equals(this.field, validationError.field);//
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(this.object, this.field);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
