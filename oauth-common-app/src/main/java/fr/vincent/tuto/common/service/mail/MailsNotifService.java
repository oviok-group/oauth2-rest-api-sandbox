/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : MailsNotifService.java
 * Date de création : 26 nov. 2020
 * Heure de création : 17:03:56
 * Package : fr.vincent.tuto.common.service.mail
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.service.mail;


import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.google.common.annotations.VisibleForTesting;

import fr.vincent.tuto.common.constants.AppConstants;
import fr.vincent.tuto.common.model.mo.MailModel;
import fr.vincent.tuto.common.service.props.ApplicationPropsService;
import fr.vincent.tuto.common.utils.date.DatesConverterUtil;

/**
 * Service de production et envoi de mails de notifications des utilisateurs.
 * <p>
 * L'envoi des mails se fera de façon asynchrone par utilisation de
 * {@link Async}
 * 
 * @author Vincent Otchoun
 */
@Service
public class MailsNotifService
{
    //
    private static final Logger LOGGER = LoggerFactory.getLogger(MailsNotifService.class);

    @Autowired
    private ApplicationPropsService applicationPropsService;

    @Autowired
    private JavaMailSenderImpl javaMailSender;

    /**
     * Construire le message et envoyer le mail.
     * 
     * @param pMailModel le model de fourniture des paramètres de construction du mail. Il fournit :
     *                   <ul>
     *                   <li>l'adresse mail du destinataire</li>
     *                   <li>le sujet du mail à envoyer.</li>
     *                   <li>le contenu du mail à envoyer.</li>
     *                   <li>indique si le format mime en plusieurs partie est utilsé ou pas.</li>
     *                   <li>indique si le type mime 'text/html' est utilisé ou pas ('text/plain').</li>
     *                   </ul>
     */
    @Async
    public void sendMail(final MailModel pMailModel)
    {
        // Préparer le message avec utilisation d'un helper Spring
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        try
        {
                // addresse de l'émeteur du message
                final String fromMail = this.applicationPropsService.getMailProps().getFrom();

                // helper Spring pour construire le message
                final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, pMailModel.isHtml(),
                AppConstants.UTF_8);

                // Constuire le message
                message.setTo(pMailModel.getEmail()); // le destinataire du message
                message.setFrom(fromMail); // émetteur du message
                message.setSubject(pMailModel.getSubject()); // le sujet du message
                message.setText(pMailModel.getContent(), pMailModel.isHtml()); // contenu,formats proposés :
                                                                                // 'text/html'(true) ou
                                                      // 'text/plain'(false)
                // horodatage envoi du mail
                final Date horodatage = DatesConverterUtil.getCurrentDateTime();
                message.setSentDate(horodatage);

                // traitement et envoi du message
                javaMailSender.send(mimeMessage);
        }
        catch (MailException | MessagingException e)
        {
            LOGGER.warn("[sendMail] - Impossiblie d'envoyer le message.", e);
        }
    }

    public ApplicationPropsService getApplicationPropsService()
    {
        return this.applicationPropsService;
    }

    public JavaMailSenderImpl getJavaMailSender()
    {
        return this.javaMailSender;
    }

    @VisibleForTesting
    public void setApplicationPropsService(ApplicationPropsService applicationPropsService)
    {
        this.applicationPropsService = applicationPropsService;
    }

    @VisibleForTesting
    public void setJavaMailSender(JavaMailSenderImpl javaMailSender)
    {
        this.javaMailSender = javaMailSender;
    }

    /**
     * @param pMailModel le model de données des éléments de construction envoi du message. Contrôle effectué sur :
     *                   <ul>
     *                   <li>l'adresse mail du destinataire</li>
     *                   <li>le sujet du mail à envoyer.</li>
     *                   <li>le contenu du mail à envoyer.</li>
     *                   </ul>
     * @return
     */
    @SuppressWarnings("unused")
    private boolean isValidMandatoryParams(final MailModel pMailModel)
    {
        return pMailModel != null && StringUtils.isNotBlank(pMailModel.getEmail()) && StringUtils.isNotBlank(pMailModel
        .getSubject()) && StringUtils
        .isNotBlank(pMailModel.getContent());
    }
}
