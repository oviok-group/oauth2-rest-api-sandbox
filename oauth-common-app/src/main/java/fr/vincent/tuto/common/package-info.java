/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 18 nov. 2020
 * Heure de création : 12:47:15
 * Package : fr.vincent.tuto.common
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Package de base du module applicatif.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common;
