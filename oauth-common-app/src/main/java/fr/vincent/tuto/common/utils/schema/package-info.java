/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 22 nov. 2020
 * Heure de création : 06:22:11
 * Package : fr.vincent.tuto.common.utils.schema
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Utilitaires de génération de schémas XSD.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.utils.schema;
