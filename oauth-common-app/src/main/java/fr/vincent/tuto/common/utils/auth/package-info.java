/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 24 nov. 2020
 * Heure de création : 06:12:58
 * Package : fr.vincent.tuto.common.utils.auth
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Utilitaires pour l'authentification.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.utils.auth;
