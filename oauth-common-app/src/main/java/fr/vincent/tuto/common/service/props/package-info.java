/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 18 nov. 2020
 * Heure de création : 13:32:56
 * Package : fr.vincent.tuto.common.service.props
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Centralise les services de gestion des propriétés externalisées dans les module applicatifs.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.service.props;
