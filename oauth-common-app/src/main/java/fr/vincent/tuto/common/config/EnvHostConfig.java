/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : EnvHostConfig.java
 * Date de création : 30 nov. 2020
 * Heure de création : 11:29:08
 * Package : fr.vincent.tuto.common.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.config;

import java.net.InetAddress;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;

import fr.vincent.tuto.common.constants.AppConstants;

/**
 * @author Vincent Otchoun
 */
@Configuration
public class EnvHostConfig implements InitializingBean, DisposableBean
{
    private static final Logger LOGGER = LoggerFactory.getLogger(EnvHostConfig.class);

    @Autowired
    private ConfigurableEnvironment environment;

    private String hostAddress = null;

    /**
     * Obtenir l'adresse de l'hôte.
     * 
     * @return l'adresse de l'hôte.
     */
    public String getHostAddress()
    {
        LOGGER.info("[getHostAddress] - Obtenir l'adresse de l'hôte.");

        //
        final String envMachine = this.environment.getProperty(AppConstants.NOM_MACHINE_KEY).trim();
        return StringUtils.isNotBlank(this.hostAddress) && StringUtils.isNotBlank(envMachine) && envMachine.equals(
        this.hostAddress) ? this.hostAddress.trim() : null;
    }

    /**
     * Vérifiez s'il s'agit de l'hôte local.
     * 
     * @return true si hôte local, false sinon.
     */
    public Boolean isLocalHost()
    {
        LOGGER.info("[isLocalHost] - Vérifiez s'il s'agit de l'hôte local.");

        final String localMachineName = this.environment.getProperty(AppConstants.NOM_MACHINE_KEY).trim();
        return StringUtils.isNotBlank(localMachineName) && StringUtils.isNotBlank(this.getHostAddress()) && this
        .getHostAddress().contains(localMachineName) ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        this.hostAddress = InetAddress.getLocalHost().getHostName();
    }

    @Override
    public void destroy() throws Exception
    {
        this.environment = null;
        this.hostAddress = null;
    }
}
