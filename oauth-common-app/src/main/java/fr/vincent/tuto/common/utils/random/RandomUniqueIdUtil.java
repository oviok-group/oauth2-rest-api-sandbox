/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : RandomUniqueIdUtil.java
 * Date de création : 19 nov. 2020
 * Heure de création : 16:53:20
 * Package : fr.vincent.tuto.common.utils
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.utils.random;

import java.security.SecureRandom;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Classe Utilitaire de génération aléatoire de chaînes de caractères fournissant les élements suivants :
 * <ul>
 * <li>Générer un mot de passe aléatoire sécurisé.</li>
 * <li>Générer une clé d'activation aléatoire sécurisé.</li>
 * <li>Générer une clé de réinitialisation aléatoire sécurisé.</li>
 * <li>Générer un identifiant unique.</li>
 * <li>Générer un {@link UUID} à partir d'un flux d'octets.</li>
 * </ul>
 * 
 * @author Vincent Otchoun
 */
public final class RandomUniqueIdUtil
{
    //
    private static final int RANDOM_DEF_COUNT = 50;
    private static final int RANDOM_BYTE_SIZE = 64;
    private static final SecureRandom SECURE_RANDOM;

    /**
     * Initialisation de la clé de sécurisdation aléatoire.
     */
    static
    {
        SECURE_RANDOM = new SecureRandom();
        SECURE_RANDOM.nextBytes(new byte[RANDOM_BYTE_SIZE]);
    }

    /**
     * Générer un mot de passe alphanumérique de façon aléatoire et sécurisée.
     * 
     * @return
     */
    public static String generatePassword()
    {
        return generateRandomAlphanumeric();
    }

    /**
     * Générer une clé d'activation alphanumérique de façon aléatoire et sécurisée.
     * 
     * @return
     */
    public static String generateActivationKey()
    {
        return generateRandomAlphanumeric();
    }

    /**
     * Générer une clé de réinitialisation alphanumérique de façon aléatoire et sécurisée.
     * 
     * @return
     */
    public static String generateResetKey()
    {
        return generateRandomAlphanumeric();
    }

    /**
     * Générer un identifiant unique UUID.
     * 
     * @return
     */
    public static String generateUniqueId()
    {
        return UUID.randomUUID().toString();
    }

    /**
     * @param pArray
     * @return
     */
    public static UUID generateUUID(final byte[] pArray)
    {
        return UUID.nameUUIDFromBytes(pArray);
    }

    /**
     * Constructeur privé de l'utilitaire
     */
    private RandomUniqueIdUtil()
    {
    }

    /**
     * Générer une chaîne de caractères alpha-numérique de façon aléatoire et sécurisée.
     * 
     * @return
     */
    private static String generateRandomAlphanumeric()
    {
        return RandomStringUtils.random(RANDOM_DEF_COUNT, 0, 0, true, true, null, SECURE_RANDOM);
    }
}
