/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : LogStartConfig.java
 * Date de création : 30 nov. 2020
 * Heure de création : 08:14:18
 * Package : fr.vincent.tuto.common.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.config;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;

import com.google.common.collect.Maps;

import fr.vincent.tuto.common.constants.AppConstants;

/**
 * Configuration pour la fournisture de journaux au démarrage de l'application.
 * 
 * @author Vincent Otchoun
 */
@Configuration
public class LogStartConfig implements InitializingBean
{
    private static final Logger LOGGER = LoggerFactory.getLogger(LogStartConfig.class);

    @Autowired
    private ConfigurableEnvironment environment;

    /**
     * Obtenir la journalisation au démarrage de l'application.
     * 
     * @param pEnvironment Interface l'environnement actuel dans lequel s'exécute l'application.
     */
    public static void logStartUp(final Environment pEnvironment)
    {
        // traitement du protocol
        final String securityKey = pEnvironment.getProperty(AppConstants.SECURITY_KEY);
        final String protocol = (StringUtils.isBlank(securityKey)) ? AppConstants.HTTP : AppConstants.HTTPS;

        // traitement du port du serveur
        final String portServer = pEnvironment.getProperty(AppConstants.SERVER_PORT);
        final String contextPathContent = pEnvironment.getProperty(AppConstants.CONTEXT_PATH);
        final String contextPath = (StringUtils.isBlank(contextPathContent)) ? AppConstants.DEFAULT_CONTEXT_PATH
        : contextPathContent;

        String hostAdresse = null;
        final String hostErroMessage = pEnvironment.getProperty(AppConstants.HOSTNAME_MESSAGE_ERROR_KEY);
        try
        {
            hostAdresse = InetAddress.getLocalHost().getHostAddress();
        }
        catch (UnknownHostException e)
        {
            hostAdresse = AppConstants.HOST_ADRESS;
            LOGGER.error(hostErroMessage);
        }

        // app name process
        final String appLog = pEnvironment.getProperty(AppConstants.START_LOG_MESSAGE_ERROR_KEY);
        final String appName = pEnvironment.getProperty(AppConstants.APP_NAME);
        LOGGER.info(appLog, appName, protocol, portServer, contextPath, protocol, hostAdresse, portServer, contextPath,
        pEnvironment.getActiveProfiles());
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        //
        final Collection<String> activeProfiles = Arrays.asList(this.environment.getActiveProfiles());
        if (activeProfiles.contains(AppConstants.DEVELOPMENT_PROFILE) && activeProfiles.contains(
        AppConstants.PRODUCTION_PROFILE))
        {
            final String prodDevErrorMessage = this.environment.getProperty(AppConstants.DEV_MESSAGE_ERROR_KEY);
            LOGGER.error(prodDevErrorMessage);
            return;
        }

        if (activeProfiles.contains(AppConstants.DEVELOPMENT_PROFILE) && activeProfiles.contains(
        AppConstants.CLOUD_PROFILE))
        {
            final String cloudDevErrorMessage = this.environment.getProperty(AppConstants.CLOUD_MESSAGE_ERROR_KEY);
            LOGGER.error(cloudDevErrorMessage);
            return;
        }
    }

    /**
     * Ajouterle profile par défaut.
     * 
     * @param pApplication
     */
    public static void addDefaultProfile(final SpringApplication pApplication)
    {
        final Map<String, Object> defaultProperties = Maps.newHashMap();
        defaultProperties.put(AppConstants.DEFAULT_PROFILE, AppConstants.DEVELOPMENT_PROFILE);
        pApplication.setDefaultProperties(defaultProperties);
    }
}
