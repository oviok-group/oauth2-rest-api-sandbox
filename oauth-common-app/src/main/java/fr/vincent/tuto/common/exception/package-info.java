/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 19 nov. 2020
 * Heure de création : 19:03:15
 * Package : fr.vincent.tuto.common.exception
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient d'interception des erreurs/exceptions survenues.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.exception;
