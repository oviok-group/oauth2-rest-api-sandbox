/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : GenericGlobalExceptionHandler.java
 * Date de création : 5 déc. 2020
 * Heure de création : 23:19:56
 * Package : fr.vincent.tuto.common.exception
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.exception;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.fasterxml.jackson.core.JsonParseException;

import fr.vincent.tuto.common.model.payload.GenericApiResponse;

/**
 * Composant générique de gestion des exceptions au niveau front.
 * 
 * @author Vincent Otchoun
 */
@ControllerAdvice
public abstract class GenericGlobalExceptionHandler<T>
{

    /**
     * Interception des exceptions levées lorsqu'une erreur ayant le status (code HTTP) 4xx survient.
     * 
     * @param ex erreur survenue.
     * @return entité de réponse HTTP avec le status et le corps.
     */
    @ExceptionHandler(value = { HttpClientErrorException.class })
    public abstract ResponseEntity<GenericApiResponse<T>> handleHttpClientErrorException(final HttpClientErrorException ex);

    /**
     * Interception des exceptions internes propres au server (erreurs internes levées).
     * 
     * @param ex erreur interne lsurvenue.
     * @return entité de réponse HTTP avec le status et le corps.
     */
    @ExceptionHandler(value = { CustomAppException.class })
    public abstract ResponseEntity<GenericApiResponse<T>> handleHttpCustomAppException(final Exception ex);

    /**
     * Interception des exceptions ou erreurs survenues lorsque la requête JSON est mal formatée aussi bien en lecture qu'en
     * écriture.
     * 
     * @param ex erreur ou exception levée.
     * @return entité de réponse HTTP avec le status et le corps.
     */
    @ExceptionHandler(value = { HttpMessageNotReadableException.class, JsonParseException.class, HttpMessageNotWritableException.class })
    public abstract ResponseEntity<GenericApiResponse<T>> handleNotReadableException(final Exception ex);

    /**
     * Interception des erreurs d'URL non valide.
     * 
     * @param ex erreur ou exception levée.
     * @return entité de réponse HTTP avec le status et le corps.
     */
    @ExceptionHandler(value = { NoHandlerFoundException.class })
    public abstract ResponseEntity<GenericApiResponse<T>> handleNoHandlerFoundException(NoHandlerFoundException ex);

    /**
     * Intercèpter les erreurs de violations de contraintes lorsque @Validated échoue.
     * 
     * @param ex erreur ou exception levée.
     * @return entité de réponse HTTP avec le status et le corps.
     */
    @ExceptionHandler(value = { ConstraintViolationException.class })
    public abstract ResponseEntity<GenericApiResponse<T>> handleConstraintViolationException(ConstraintViolationException ex);

    /**
     * Intercepter les erreurs survenues pour les recherches infructueuses.
     * 
     * @param ex erreur ou exception levée.
     * @return entité de réponse HTTP avec le status et le corps.
     */
    @ExceptionHandler(value = { EntityNotFoundException.class, UsernameNotFoundException.class })
    public abstract ResponseEntity<GenericApiResponse<T>> handleNotFoundException(final Exception ex);

    /**
     * Intercepter les erreurs de violations d'intégrités des données.
     * 
     * @param ex erreur ou exception levée.
     * @return entité de réponse HTTP avec le status et le corps.
     */
    @ExceptionHandler(value = { DataIntegrityViolationException.class })
    public abstract ResponseEntity<GenericApiResponse<T>> handleDataIntegrityException(final DataIntegrityViolationException ex);

    /**
     * Intercepter les erreurs de non-concordance de type d'argument de méthode.
     * 
     * @param ex erreur ou exception levée.
     * @return entité de réponse HTTP avec le status et le corps.
     */
    @ExceptionHandler(value = { MethodArgumentTypeMismatchException.class })
    public abstract ResponseEntity<GenericApiResponse<T>> handleMethodArgumentTypException(final MethodArgumentTypeMismatchException ex);

    /**
     * Intercepter les erreurs d'accès non autorisés.
     * 
     * @param ex erreur ou exception levée.
     * @return entité de réponse HTTP avec le status et le corps.
     */
    @ExceptionHandler(value = { AccessDeniedException.class, OAuth2AccessDeniedException.class })
    public abstract ResponseEntity<GenericApiResponse<T>> handleAccessDeniedException(final Exception ex);
}
