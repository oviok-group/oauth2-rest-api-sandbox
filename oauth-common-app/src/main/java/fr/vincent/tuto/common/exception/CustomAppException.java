/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : CustomAppException.java
 * Date de création : 19 nov. 2020
 * Heure de création : 19:15:12
 * Package : fr.vincent.tuto.common.exception
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Classe d'interception des erreurs/exceptions metiers ou applicatifs. Elles ne sont pas vérifiees par le compilateur
 * (Unchecked exceptions).
 * 
 * @author Vincent Otchoun
 */
@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
public class CustomAppException extends RuntimeException
{
    /**
     * 
     */
    private static final long serialVersionUID = 3744259998932959889L;

    /**
     * Constructeur avec en paramètre le message d'erreurs.
     * 
     * @param message le message d'erreurs.
     */
    public CustomAppException(String message)
    {
        super(message);
    }

    /**
     * Constructeur avec la cause de l'exception.
     * 
     * @param cause
     */
    public CustomAppException(Throwable cause)
    {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public CustomAppException(String message, Throwable cause)
    {
        super(message, cause);
    }

}
