/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 11 déc. 2020
 * Heure de création : 05:28:53
 * Package : fr.vincent.tuto.common.utils.rest
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les utilitaires du design pattern REST.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.utils.rest;
