/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 25 nov. 2020
 * Heure de création : 01:34:06
 * Package : fr.vincent.tuto.common.mapper
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * @author Vincent Otchoun
 *
 */
package fr.vincent.tuto.common.mapper;
