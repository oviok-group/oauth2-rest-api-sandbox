/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : CryptoModel.java
 * Date de création : 22 nov. 2020
 * Heure de création : 06:49:00
 * Package : fr.vincent.tuto.common.model
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.model.mo;

import java.io.Serializable;
import java.util.Objects;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Transient;

/**
 * Le modèle de données pour la fourniture des paramètres des fonction de traitement AES.
 * 
 * @author Vincent Otchoun
 */
public class CryptoModel implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -2370244878693760189L;

    // Algorithme symétrique AES
    private String algorithm;
    private String instancePad;
    private String secureRandomInstance;
    private Integer bytesLength;
    private String keyValue;
    private String encoding;

    // Algorithme asymétrique avec clé publique RSA.
    private String privateKey;
    private String publicKey;
    private String keystorePassword; // -storepass
    private String keyPassword; // -keypass
    private String aliasKeystore;
    private String signatureInstance;
    @Transient
    private String keystoreFileLocation; // classpath:app-recette-keystore.jks

    @Transient
    private String publicKeyFileLocation; // classpath:app_recette_publicKey.pub.txt

    //
    private Boolean aesSwitchFlag;

    public CryptoModel algorithm(final String pAlgorithm)
    {
        this.algorithm = pAlgorithm;
        return this;
    }

    public CryptoModel instancePad(final String pInstancePad)
    {
        this.instancePad = pInstancePad;
        return this;
    }

    public CryptoModel secureRandomInstance(final String pSecureRandomInstance)
    {
        this.secureRandomInstance = pSecureRandomInstance;
        return this;
    }

    public CryptoModel bytesLength(final Integer pBytesLength)
    {
        this.bytesLength = pBytesLength;
        return this;
    }

    public CryptoModel keyValue(final String pKeyValue)
    {
        this.keyValue = pKeyValue;
        return this;
    }

    public CryptoModel encoding(final String pEncoding)
    {
        this.encoding = pEncoding;
        return this;
    }

    public CryptoModel privateKey(final String pPrivateKey)
    {
        this.privateKey = pPrivateKey;
        return this;
    }

    public CryptoModel publicKey(final String pPublicKey)
    {
        this.publicKey = pPublicKey;
        return this;
    }

    public CryptoModel aesSwitchFlag(final Boolean pAesSwitchFlag)
    {
        this.aesSwitchFlag = pAesSwitchFlag;
        return this;
    }

    public CryptoModel keystorePassword(final String pKeystorePassword)
    {
        this.keystorePassword = pKeystorePassword;
        return this;
    }

    public CryptoModel keyPassword(final String pKeyPassword)
    {
        this.keyPassword = pKeyPassword;
        return this;
    }

    public CryptoModel aliasKeystore(final String pAliasKeystore)
    {
        this.aliasKeystore = pAliasKeystore;
        return this;
    }

    public CryptoModel signatureInstance(final String pSignatureInstance)
    {
        this.signatureInstance = pSignatureInstance;
        return this;
    }

    public CryptoModel keystoreFileLocation(final String pKeystoreFileLocation)
    {
        this.keystoreFileLocation = pKeystoreFileLocation;
        return this;
    }

    public CryptoModel publicKeyFileLocation(final String pPublicKeyFileLocation)
    {
        this.publicKeyFileLocation = pPublicKeyFileLocation;
        return this;
    }

    public String getAlgorithm()
    {
        return this.algorithm;
    }

    public String getInstancePad()
    {
        return this.instancePad;
    }

    public String getSecureRandomInstance()
    {
        return this.secureRandomInstance;
    }

    public Integer getBytesLength()
    {
        return this.bytesLength;
    }

    public String getKeyValue()
    {
        return this.keyValue;
    }

    public String getEncoding()
    {
        return this.encoding;
    }

    public String getPrivateKey()
    {
        return this.privateKey;
    }

    public String getPublicKey()
    {
        return this.publicKey;
    }

    public Boolean getAesSwitchFlag()
    {
        return this.aesSwitchFlag;
    }

    public String getKeystorePassword()
    {
        return this.keystorePassword;
    }

    public String getKeyPassword()
    {
        return this.keyPassword;
    }

    public String getAliasKeystore()
    {
        return this.aliasKeystore;
    }

    public String getSignatureInstance()
    {
        return this.signatureInstance;
    }

    public String getKeystoreFileLocation()
    {
        return this.keystoreFileLocation;
    }

    public String getPublicKeyFileLocation()
    {
        return this.publicKeyFileLocation;
    }

    public void setAlgorithm(final String pAlgorithm)
    {
        this.algorithm = pAlgorithm;
    }

    public void setInstancePad(final String pInstancePad)
    {
        this.instancePad = pInstancePad;
    }

    public void setSecureRandomInstance(final String pSecureRandomInstance)
    {
        this.secureRandomInstance = pSecureRandomInstance;
    }

    public void setBytesLength(final Integer pBytesLength)
    {
        this.bytesLength = pBytesLength;
    }

    public void setKeyValue(final String pKeyValue)
    {
        this.keyValue = pKeyValue;
    }

    public void setEncoding(final String pEncoding)
    {
        this.encoding = pEncoding;
    }

    public void setPrivateKey(final String pPrivateKey)
    {
        this.privateKey = pPrivateKey;
    }

    public void setPublicKey(final String pPublicKey)
    {
        this.publicKey = pPublicKey;
    }

    public void setAesSwitchFlag(final Boolean pAesSwitchFlag)
    {
        this.aesSwitchFlag = pAesSwitchFlag;
    }

    public void setKeystorePassword(String keystorePassword)
    {
        this.keystorePassword = keystorePassword;
    }

    public void setKeyPassword(String keyPassword)
    {
        this.keyPassword = keyPassword;
    }

    public void setAliasKeystore(String aliasKeystore)
    {
        this.aliasKeystore = aliasKeystore;
    }

    public void setSignatureInstance(final String pSignatureInstance)
    {
        this.signatureInstance = pSignatureInstance;
    }

    public void setKeystoreFileLocation(final String pKeystoreFileLocation)
    {
        this.keystoreFileLocation = pKeystoreFileLocation;
    }

    public void setPublicKeyFileLocation(final String pPublicKeyFileLocation)
    {
        this.publicKeyFileLocation = pPublicKeyFileLocation;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(java.lang.Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        CryptoModel article = (CryptoModel) o;
        return Objects.equals(this.algorithm, article.algorithm)//
        && Objects.equals(this.instancePad, article.instancePad)//
        && Objects.equals(this.secureRandomInstance, article.secureRandomInstance)//
        && Objects.equals(this.bytesLength, article.bytesLength)//
        && Objects.equals(this.keyValue, article.keyValue)//
        && Objects.equals(this.encoding, article.encoding)//
        && Objects.equals(this.privateKey, article.privateKey)//
        && Objects.equals(this.publicKey, article.publicKey)//
        && Objects.equals(this.keystorePassword, article.keystorePassword)//
        && Objects.equals(this.keyPassword, article.keyPassword)//
        && Objects.equals(this.aliasKeystore, article.aliasKeystore)//
        && Objects.equals(this.signatureInstance, article.signatureInstance);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode()
    {
        return Objects.hash(this.algorithm, this.instancePad, this.secureRandomInstance, this.bytesLength,
        this.keyValue, this.encoding, this.privateKey, this.publicKey, this.keystorePassword, this.keyPassword,
        this.aliasKeystore, this.signatureInstance);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString()
    {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
