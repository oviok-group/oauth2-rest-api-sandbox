/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : AuthEntryPointCommon.java
 * Date de création : 5 déc. 2020
 * Heure de création : 13:11:10
 * Package : fr.vincent.tuto.common.exception
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.exception;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import fr.vincent.tuto.common.constants.AppConstants;
import fr.vincent.tuto.common.mapper.GenericJSONMapper;
import fr.vincent.tuto.common.model.error.ApiResponseError;

/**
 * Composant permettant de gérer les execeptions ou erreurs d'authnetification.
 * 
 * @author Vincent Otchoun
 */
@Component
public class AuthEntryPointCommon implements AuthenticationEntryPoint
{
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthEntryPointCommon.class);

    @Autowired
    private GenericJSONMapper genericJSONMapper;

    @Autowired
    private StringHttpMessageConverter httpMessageConverter;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException
    {
        // On trace l'erreur survenue.
        LOGGER.error("[commence] - Erreur accès non autorisé : {} ", authException.getMessage());

        request.setCharacterEncoding(AppConstants.UTF_8);

        // Construire l'erreur à retourner en cas d'accès non autorisé.
        final ApiResponseError error = new ApiResponseError()//
        .status(HttpStatus.UNAUTHORIZED)//
        .timestamp(LocalDateTime.now(ZoneId.systemDefault()))//
        .details(authException.getMessage())//
        .validationErrors(null);

        final ServerHttpResponse outputMessage = new ServletServerHttpResponse(response);
        outputMessage.setStatusCode(HttpStatus.UNAUTHORIZED);

        final String errorStr = this.genericJSONMapper.toStringJSON(error, true);

        this.httpMessageConverter.setDefaultCharset(AppConstants.CHARSET_UTF_8);
        this.httpMessageConverter.write(errorStr, MediaType.APPLICATION_JSON, outputMessage);
    }
}
