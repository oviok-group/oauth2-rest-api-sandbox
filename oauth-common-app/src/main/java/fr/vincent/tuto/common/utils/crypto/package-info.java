/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 22 nov. 2020
 * Heure de création : 06:25:57
 * Package : fr.vincent.tuto.common.utils.crypto
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Utilitaires pour les normes avec algorithmes chiffrement symétrique AES (Advanced Encryption Standard) et asymétrique
 * RSA (Rivest-Shamir-Adelman).
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.utils.crypto;
