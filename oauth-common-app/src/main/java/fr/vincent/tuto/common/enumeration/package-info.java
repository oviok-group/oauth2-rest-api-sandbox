/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 24 nov. 2020
 * Heure de création : 19:33:28
 * Package : fr.vincent.tuto.common.enumeration
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les enumerations communes de l'application.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.enumeration;
