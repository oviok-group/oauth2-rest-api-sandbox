/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : package-info.java
 * Date de création : 18 nov. 2020
 * Heure de création : 12:48:17
 * Package : fr.vincent.tuto.common.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
/**
 * Contient les configurations de base du module commun.
 * 
 * @author Vincent Otchoun
 */
package fr.vincent.tuto.common.config;
