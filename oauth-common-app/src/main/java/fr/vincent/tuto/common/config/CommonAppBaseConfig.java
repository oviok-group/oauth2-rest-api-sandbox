/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : CommonAppBaseConfig.java
 * Date de création : 18 nov. 2020
 * Heure de création : 12:43:52
 * Package : fr.vincent.tuto.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 * Configuration de base du module applicatif.
 * 
 * @author Vincent Otchoun
 */
@Configuration
@PropertySources(value = { @PropertySource(value = { "classpath:common-app-db.properties" }, ignoreResourceNotFound = true), @PropertySource(value = {
        "classpath:common-app-application.properties" }, ignoreResourceNotFound = true), @PropertySource(value =
        { "classpath:common-app-messages.properties" }, ignoreResourceNotFound = true) })
@ComponentScan(basePackages = { "fr.vincent.tuto.common" })
@ConfigurationProperties(prefix = "vot", ignoreUnknownFields = true)
public class CommonAppBaseConfig
{
    //

    // @Bean
    // public StaticApplicationContext staticApplicationContext()
    // {
    // return new StaticApplicationContext();
    // }
}
