/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : CommonBeansConfig.java
 * Date de création : 25 nov. 2020
 * Heure de création : 07:00:07
 * Package : fr.vincent.tuto.common.config
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */
package fr.vincent.tuto.common.config;

import java.util.Properties;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import fr.vincent.tuto.common.constants.AppConstants;
import fr.vincent.tuto.common.service.props.ApplicationPropsService;
import fr.vincent.tuto.common.service.props.ApplicationPropsService.MailProps;

/**
 * Configuration de base des beans communs à utiliser dnas les autres autres modules applicatifs.
 * 
 * @author Vincent Otchoun
 */
@Configuration
public class CommonBeansConfig
{
    //
    @Autowired
    private ApplicationPropsService applicationPropsService;

    /**
     * Construire le bean d'implémentation de {@link PasswordEncoder} utilisant la fonction de hachage forte
     * {@link BCrypt} pour encoder/crypter les mots de passe.
     * 
     * @return le mot de passe crypté.
     */
    @Bean(name = "passwordEncoder")
    @Primary
    public BCryptPasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder(12);
    }

    /**
     * Construire le bean de générateur de mappeur Jackson vers objet.
     * 
     * @return le générateur de mappeur.
     */
    @Bean(name = "jackson2ObjectMapperBuilder")
    @Primary
    public Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder()
    {
        //
        return new Jackson2ObjectMapperBuilder()
        {
            //
            @SuppressWarnings("deprecation")
            @Override
            public void configure(final ObjectMapper pObjectMapper)
            {
                //
                super.configure(pObjectMapper);

                pObjectMapper.setVisibility(PropertyAccessor.ALL, Visibility.NONE);
                pObjectMapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
                // pObjectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                pObjectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                pObjectMapper.configure(SerializationFeature.INDENT_OUTPUT, false);
                pObjectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true);
                pObjectMapper.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
                pObjectMapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true);
                pObjectMapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
                pObjectMapper.configure(SerializationFeature.WRITE_CHAR_ARRAYS_AS_JSON_ARRAYS, true);
                pObjectMapper.configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, true);
                pObjectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true);
                pObjectMapper.configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, true);
                pObjectMapper.configure(SerializationFeature.WRAP_EXCEPTIONS, true);

                pObjectMapper.configure(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS, true);
                pObjectMapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, true);
                // pObjectMapper.configure(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS, true);
                // pObjectMapper.configure(DeserializationFeature.USE_BIG_INTEGER_FOR_INTS, true);
                // pObjectMapper.configure(DeserializationFeature.USE_LONG_FOR_INTS, true);
                pObjectMapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
            }
        };
    }

    /**
     * Construire le bean de mappeurd'objets de construction au format JSON.
     * 
     * @return le mappeur d'objets au format JSON.
     */
    @Bean(name = "objectMapper")
    @Primary
    public ObjectMapper objectMapper()
    {
        return this.jackson2ObjectMapperBuilder().build();
    }

    /**
     * mappes de propriétés personnalisées et convertisseurs pour gérer le transfert de l'objet de transfert de données
     * à l'entité et vice versa.
     * 
     * @return
     */
    @Bean(name = "modelMapper")
    @Primary
    public ModelMapper modelMapper()
    {
        return new ModelMapper();
    }

    /**
     * @return
     */
    @Bean(name = "javaMailSender")
    @Primary
    public JavaMailSender javaMailSender()
    {
        final MailProps mailProps = this.applicationPropsService.getMailProps();
        final JavaMailSenderImpl mailSenderImpl = new JavaMailSenderImpl();
        final Properties properties = new Properties();
        //
        mailSenderImpl.setHost(mailProps.getHost());
        mailSenderImpl.setPort(mailProps.getPort());
        mailSenderImpl.setUsername(mailProps.getUsername());
        mailSenderImpl.setPassword(mailProps.getPassword());
        mailSenderImpl.setProtocol(mailProps.getProtocol());

        //
        properties.put(AppConstants.SMTP_AUTH_KEY, mailProps.getAuth().booleanValue());
        properties.put(AppConstants.TLS_ENABLE_KEY, mailProps.getTls());
        properties.put(AppConstants.QUIT_WAIT_KEY, mailProps.getQuitWait());

        //
        return mailSenderImpl;
    }

}
