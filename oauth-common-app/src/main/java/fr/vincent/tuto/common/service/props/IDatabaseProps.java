/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : IDatabaseProps.java
 * Date de création : 9 mars 2021
 * Heure de création : 11:54:51
 * Package : fr.vincent.tuto.common.service.props
 * Auteur : Vincent Otchoun
 * Copyright © 2021 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.service.props;

import fr.vincent.tuto.common.service.props.DatabasePropsService.DataSourceProps;
import fr.vincent.tuto.common.service.props.DatabasePropsService.FlywayProps;
import fr.vincent.tuto.common.service.props.DatabasePropsService.HikariProps;
import fr.vincent.tuto.common.service.props.DatabasePropsService.JpaHibernateProps;

/**
 * @author Vincent Otchoun
 *
 */
public interface IDatabaseProps
{
    DataSourceProps getDataSourceProps();

    JpaHibernateProps getJpaHibernateProps();

    HikariProps getHikariProps();

    FlywayProps getFlywayProps();
}
