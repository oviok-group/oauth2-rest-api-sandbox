/*
 * ----------------------------------------------
 * Projet ou Module : oauth-common-app
 * Nom de la classe : RestUtils.java
 * Date de création : 11 déc. 2020
 * Heure de création : 05:30:34
 * Package : fr.vincent.tuto.common.utils.rest
 * Auteur : Vincent Otchoun
 * Copyright © 2020 - All rights reserved.
 * ----------------------------------------------
 */	
package fr.vincent.tuto.common.utils.rest;

import org.springframework.http.ResponseEntity;

import fr.vincent.tuto.common.model.error.ApiResponseError;
import fr.vincent.tuto.common.model.payload.GenericApiResponse;

/**
 * Utilitaire pour le design pattern REST.
 * 
 * @author Vincent Otchoun
 */
public final class RestUtils
{
    /**
     * Constructeur privé de l'utilitaire.
     */
    private RestUtils()
    {
    }

    /**
     * Construire la réponse HTTP avec le status et le corps.
     * 
     * @param <T>            le type de l'objet embarquée.
     * @param pResponseError entité de gestion des erreurs pour la réponse.
     * @return entité générique de la réponse HTTP.
     */
    public static <T> ResponseEntity<GenericApiResponse<T>> buildResponseErrorEntity(final ApiResponseError pResponseError)
    {
        final GenericApiResponse<T> response = new GenericApiResponse<>();
        response.setErrors(pResponseError);
        return ResponseEntity//
        .status(pResponseError.getStatus())//
        .body(response);
    }
}
